# -*- coding: utf-8 -*-
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
import os, inspect, types
from google.appengine.api import users, memcache
from google.appengine.ext.webapp import template as tmpl
from django.utils import simplejson
import settings, context_processors

enable_cache = getattr(settings, 'DEBUG', False)

class TemplateNotFound(Exception):
    pass

def json(func):
    def wrapper(self, *args, **kwargs):
        data = func(self, *args, **kwargs)
        json = simplejson.dumps(data)
        self.response.content_type = 'application/json'
        self.response.out.write(json)
        return data
    return wrapper

def params(**kwconvs):
    to_template = False
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            newargs = {}
            args = list(args)
            iargs, varargs, varkw, defaults = inspect.getargspec(func)
            defaults = defaults or ()
            defaults_start = len(iargs) - len(defaults) - 1
            do_nothing = lambda x: x
            for i, argname in enumerate(iargs[1:]):
                if i < len(args):
                    if args[i] == None:
                        val = None
                    else:
                        val = args[i] = kwconvs.get(argname, do_nothing)(args[i])
                else:
                    val = self.request.get(argname, None)
                    if val and val != 'null':
                        val = kwconvs.get(argname, do_nothing)(val)
                    elif argname not in kwargs:
                        defaults_idx = i - defaults_start
                        if 0 <= defaults_idx:
                            val = defaults[defaults_idx]
                    if val: kwargs[argname] = val
                if val and to_template: newargs[argname] = val
            d = func(self, *args, **kwargs)
            if to_template: d.update(newargs)
            return d
        return wrapper
    return decorator

def login_required(users=users):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            user = users.get_current_user()
            if not user:
                self.redirect(users.create_login_url(self.request.uri))
            else:
                kwargs['user'] = user
                return func(self, *args, **kwargs)
        return wrapper
    return decorator

def login_option(users=users):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            user = users.get_current_user()
            if user:
                kwargs['user'] = user
            return func(self, *args, **kwargs)
        return wrapper
    return decorator

def template(template_path, site=None, extra_dict={},
        use_cache=False, cache_timeout=10, cache_key='', 
        cache_key_args=None, cache_namespace='',
        use_settings=True, use_params=True, use_user=True,
        user_openid=False, i18n=False, default_lang=None,
        store_lang=False, use_var=False, force_template_path=None):
    def decorator(func):
        if use_settings: func = context_processor(context_processors.settings)(func)
        if use_params: func = context_processor(context_processors.params)(func)
        if use_user:
            if user_openid:
                func = context_processor(context_processors.openid_user)(func)
            else:
                func = context_processor(context_processors.user)(func)
        def wrapper(self, *args, **kwargs):
            _cache_key = cache_key
            output = None
            if enable_cache and use_cache:
                if not _cache_key and cache_key_args:
                    values = []
                    for carg in cache_key_args:
                        if type(carg) is types.IntType:
                            val = args[carg]
                        else:
                            val = kwargs.get(carg, None) or \
                                self.request.get(carg, None)
                        if val: values.append(val)
                    _cache_key = ','.join(values)
                output = memcache.get(cache_key, cache_namespace)
            if output:
                d = {}
            else:
                lang_header = self.request.headers.get('Accept-Language')
                if i18n and lang_header:
                    langs = get_accept_languages(lang_header)
                    if store_lang and langs:
                        first_lang = langs[0].split('-')[0]
                        kwargs['language'] = first_lang
                else:
                    langs = []

                d = {}
                http_code = None

                if kwargs.get('_return'):
                    d.update(kwargs)
                    http_code = d.get('_http_code')
                else:
                    try:
                        d = func(self, *args, **kwargs)
                    except:
                        if getattr(settings, 'DEBUG', False): raise
                        else: http_code = 500
                    else:
                        http_code = d.get('_http_code')

                if '_template_path' in d:
                    _template_path = d['_template_path']
                elif http_code:
                    self.error(http_code)
                    _template_path = '%s.html' % http_code
                else:
                    _template_path = template_path
                    if use_var:
                        _template_path = _template_path % d.get('template')
                        
                path = get_path_exists(_template_path, langs, default_lang)
                if path:
                    output = render(path, d, use_cache, _cache_key, 
                            cache_timeout, cache_namespace)
            if output: self.response.out.write(output)
            return d
        return wrapper
    return decorator

def get_path_exists(template_path, langs, default_lang):
    if not template_path: return None
    name, ext = template_path.split('.')
    for lang in langs:
        path = '%s.%s.%s' % (name, lang, ext) if lang else default_lang
        abs_path = os.path.join(settings.TEMPLATE_PATH, path)
        if os.path.exists(abs_path): return abs_path

    if default_lang is not None:
        path = '%s.%s.%s' % (name, default_lang, ext)
        abs_path = os.path.join(settings.TEMPLATE_PATH, path)
        if os.path.exists(abs_path): return abs_path 

    abs_path = os.path.join(settings.TEMPLATE_PATH, template_path)
    if os.path.exists(abs_path): return abs_path 

    if (getattr(settings, 'DEBUG', False) or 
            template_path == '404.html'):
        raise TemplateNotFound()
    else:
        return get_path_exists('404.html', langs, default_lang)

def render(path, d, use_cache=False, cache_key=None, cache_timeout=0, cache_namespace=None):
    output = tmpl.render(path, d)
    if use_cache:
        memcache.set(cache_key, output, time=cache_timeout, namespace=cache_namespace)
    return output

def get_accept_languages(header):
    languages = []
    for lang in header.split(','):
        lang = lang.strip().lower()
        lang1 = lang.split(';')[0]
        if lang1 not in languages: languages.append(lang1)
        lang2 = lang1.split('-')[0]
        if lang2 not in languages: languages.append(lang2)
    languages.append('')
    return languages
    
def cache():
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            d = func(self, *args, **kwargs)
            return d
        return wrapper
    return decorator
    
def context_processor(extra_func):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            pd = extra_func(self)
            d = func(self, *args, **kwargs)
            if d is None: d = {}
            d.update(pd)
            return d
        return wrapper
    return decorator
    
