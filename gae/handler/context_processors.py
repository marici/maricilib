# -*- coding: utf-8 -*-
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
import hashlib
from uuid import uuid4
import settings as app_settings
from google.appengine.api import users
try:
    from aeoid import users as oidusers
except ImportError:
    oidusers = None

def params(handler):
    args = handler.request.arguments()
    d = {}
    for arg in args:
        d[arg] = handler.request.get(arg)
    return d

def settings(handler):
    d = {}
    for attr in dir(app_settings):
        d[attr] = getattr(app_settings, attr)
    return d

def user(handler):
    user = users.get_current_user()
    return {'user': user}

def openid_user(handler):
    if oidusers:
        user = oidusers.get_current_user()
        return {'user': user}
    else:
        return {}

def csrf_token(handler):
    session_id = str(uuid4())
    handler.response.headers['Set-Cookie'] = 'onetimetoken=%s; path=/' % session_id
    csrf_token = _make_csrf_token(session_id)
    return {'csrf_token':csrf_token}

def csrf_validate(handler):
    request_csrf_token = handler.request.get('csrf_token')
    session_id = handler.request.cookies.get('onetimetoken')
    csrf_token = _make_csrf_token(session_id)
    if not request_csrf_token or request_csrf_token != csrf_token:
        handler.error(403)
        raise Exception()
    return {}
    
def _make_csrf_token(session_id):
    if not session_id: return None
    return hashlib.md5(app_settings.SECRET_KEY + session_id).hexdigest()
