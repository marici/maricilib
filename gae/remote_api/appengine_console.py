#!/usr/bin/env python
# coding:utf-8
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
import code
import remote_api_base

def main():
    options, args = remote_api_base.start()
    code.interact('App Engine interactive console for %s' % options.app_id, 
        None, locals())

if __name__ == '__main__':
    main()
