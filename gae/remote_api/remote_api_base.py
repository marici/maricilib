#! /usr/bin/env python2.5
# coding: utf-8
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
import sys, os, getpass, logging

logger = logging.getLogger(__name__)

gae_path = '/usr/local/google_appengine'
extra_path = [
  gae_path,
  os.path.join(gae_path, 'lib', 'antlr3'),
  os.path.join(gae_path, 'lib', 'webob'),
  os.path.join(gae_path, 'lib', 'django'),
  os.path.join(gae_path, 'lib', 'yaml', 'lib')
]
sys.path = extra_path + sys.path
sys.path.append(os.getcwd())

from google.appengine.ext.remote_api import remote_api_stub
from google.appengine.ext import db

def start(option_list=[]):
    import optparse, codecs
    parser = optparse.OptionParser()
    parser.add_option('-v', '--verbose', dest='verbose', default=False,
        action='store_true')
    parser.add_option('-a', '--app_id', dest='app_id', default=None)
    parser.add_option('--host', dest='host', default=None)
    parser.add_option('-u', '--user', dest='user', default=None)
    parser.add_option('-p', '--password', dest='password', default=None)
    parser.add_option('-o', '--output-encoding', dest='outencoding', default='utf-8')
    parser.add_option('-i', '--input-encoding', dest='inencoding', default='utf-8')
    for option in option_list:
        parser.add_option(option)
    options, args = parser.parse_args()

    if not options.app_id:
        print('Please input app_id following to \'-a\'.')
        sys.exit(-1)

    if options.verbose:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.INFO)
    sys.stdout = codecs.getwriter(options.outencoding)(sys.stdout)

    if not options.host:
        options.host = '%s.appspot.com' % options.app_id
    logger.info('connecting %s %s', options.app_id, options.host)

    def auth_func():
        user = options.user or raw_input('Username: ')
        password = options.password or getpass.getpass('Password: ')
        return user, password

    remote_api_stub.ConfigureRemoteDatastore(options.app_id, '/remote_api', auth_func, options.host)

    return options, args

if __name__ == '__main__':
    start()
