# -*- coding: utf-8 -*-
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
import sys, os

def filterdict(d, *names):
    names = set(names)
    return dict([(k, d[k]) for k in d if k in names ])

def setup_path():
    gae_path = '/usr/local/google_appengine'
    extra_path = [
      gae_path,
      os.path.join(gae_path, 'lib', 'antlr3'),
      os.path.join(gae_path, 'lib', 'webob'),
      os.path.join(gae_path, 'lib', 'django_0_96'),
      os.path.join(gae_path, 'lib', 'fancy_urllib'),
      os.path.join(gae_path, 'lib', 'yaml', 'lib')
    ]
    sys.path = extra_path + sys.path
    sys.path.append(os.getcwd())
