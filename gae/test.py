# -*- coding: utf-8 -*-
import util
util.setup_path()

import os, unittest
from google.appengine.api import apiproxy_stub_map
from google.appengine.api import datastore_file_stub
from google.appengine.api import mail_stub
from google.appengine.api import urlfetch_stub
from google.appengine.api import user_service_stub
from google.appengine.api.memcache import memcache_stub
from google.appengine.api.images import images_stub
from google.appengine.api.taskqueue import taskqueue_stub
from google.appengine.ext import db, search
from google.appengine.api import users

# for setting Django version
#from google.appengine.dist import use_library
#use_library('django', '0.96')

APP_ID = u'test'
AUTH_DOMAIN = 'gmail.com'
LOGGED_IN_USER = 'test@example.com' 

class GAETestBase(unittest.TestCase):

    def setUp(self):
        apiproxy_stub_map.apiproxy = apiproxy_stub_map.APIProxyStubMap()
   
        stub = datastore_file_stub.DatastoreFileStub(APP_ID,'/dev/null',
            '/dev/null')
        apiproxy_stub_map.apiproxy.RegisterStub('datastore_v3', stub)
        os.environ['APPLICATION_ID'] = APP_ID
   
        apiproxy_stub_map.apiproxy.RegisterStub(
            'user', user_service_stub.UserServiceStub())
        os.environ['AUTH_DOMAIN'] = AUTH_DOMAIN
        os.environ['USER_EMAIL'] = LOGGED_IN_USER
   
        apiproxy_stub_map.apiproxy.RegisterStub(
            'urlfetch', urlfetch_stub.URLFetchServiceStub())
   
        apiproxy_stub_map.apiproxy.RegisterStub(
            'mail', mail_stub.MailServiceStub()) 

        apiproxy_stub_map.apiproxy.RegisterStub(
            'memcache', memcache_stub.MemcacheServiceStub()) 
        
        apiproxy_stub_map.apiproxy.RegisterStub(
            'images', images_stub.ImagesServiceStub()) 
        
        root_path = os.path.join(os.path.dirname(__file__), '..')
        stub = taskqueue_stub.TaskQueueServiceStub(root_path=root_path,
            auto_task_running=True)
        apiproxy_stub_map.apiproxy.RegisterStub('taskqueue', stub)

