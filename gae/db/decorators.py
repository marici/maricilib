# -*- coding: utf-8 -*-
'''
Copyright (c) 2010 Marici, Inc.
@author: michisu@marici.co.jp
'''
from google.appengine.ext import db

def in_transaction(func):
    def decorator(*args, **kwargs):
        return db.run_in_transaction(func, *args, **kwargs)
    return decorator

