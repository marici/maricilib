# -*- coding: utf-8 -*-
from google.appengine.ext import db
from django.utils import simplejson

"""
This code is derived from appenginepatch.ragendja.
"""

def getattr_by_path(obj, attr, *default):
    """Like getattr(), but can go down a hierarchy like 'attr.subattr'"""
    value = obj
    for part in attr.split('.'):
        if not hasattr(value, part) and len(default):
            return default[0]
        value = getattr(value, part)
        if callable(value):
            value = value()
    return value

def to_json_data(model_instance, property_list):
    """
    Converts a models into dicts for use with JSONResponse.

    You can either pass a single model instance and get a single dict
    or a list of models and get a list of dicts.

    For security reasons only the properties in the property_list will get
    added. If the value of the property has a json_data function its result
    will be added, instead.
    """
    if hasattr(model_instance, '__iter__'):
        return [to_json_data(item, property_list) for item in model_instance]
    json_data = {}
    for property in property_list:
        property_instance = None
        try:
            property_instance = getattr(model_instance.__class__,
                property.split('.', 1)[0])
        except:
            pass
        key_access = property[len(property.split('.', 1)[0]):]
        if isinstance(property_instance, db.ReferenceProperty) and \
                key_access in ('.key', '.key.name'):
            key = property_instance.get_value_for_datastore(model_instance)
            if key_access == '.key':
                json_data[property] = str(key)
            else:
                json_data[property] = key.name()
            continue
        value = getattr_by_path(model_instance, property, None)
        value = getattr_by_path(value, 'json_data', value)
        json_data[property] = value
    return json_data

