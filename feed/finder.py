# coding: utf-8
import re
import HTMLParser
import urllib

import logging
log = logging.getLogger("maricilib.feed.finder")

link_tag_pattern = re.compile("<link[^>]*>", re.I)
type_attr_pattern = re.compile('type="([^"]*)"', re.I)
rel_attr_pattern = re.compile('rel="([^"]*)"', re.I)
title_attr_pattern = re.compile('title="([^"]*)"')
href_attr_pattern = re.compile('href="([^"]*)"', re.I)
FEED_CONTENT_TYPES = ("application/x.atom+xml", "application/atom+xml", 
    "application/xml", "text/xml", "application/rss+xml", "application/rdf+xml")

class FeedFinderRe(object):

    def __init__(self):
        self.feeds = []

    def feed(self, html):
        for link_elm in link_tag_pattern.findall(html):
            type = type_attr_pattern.search(link_elm)
            type_feed = type and type.group(1) in FEED_CONTENT_TYPES
            rel = rel_attr_pattern.search(link_elm)
            rel_feed = rel and rel.group(1) == "alternate"
            title = title_attr_pattern.search(link_elm)
            title_str = title.group(1) if title else ""
            if type_feed and rel_feed:
                href = href_attr_pattern.search(link_elm)
                self.feeds.append(dict(
                    title=title_str,
                    type=type.group(1),
                    href=href.group(1)
                ))


def find_from_url(blog_url):
    page = urllib.urlopen(blog_url)
    html = page.read()
    page.close()
    return find_from_html(html)

def find_from_html(html):
    parser = FeedFinderRe()
    parser.feed(html)
    return parser.feeds
