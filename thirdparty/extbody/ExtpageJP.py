#!/usr/local/Python25/bin/python
# coding: euc_jp

##    Copyright 2007 Hiroshi Ayukawa (email: ayukawa.hiroshi [atmark] gmail.com)
##
##    Licensed under the Apache License, Version 2.0 (the "License");
##    you may not use this file except in compliance with the License.
##    You may obtain a copy of the License at
##
##        http://www.apache.org/licenses/LICENSE-2.0
##
##    Unless required by applicable law or agreed to in writing, software
##    distributed under the License is distributed on an "AS IS" BASIS,
##    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##    See the License for the specific language governing permissions and
##    limitations under the License.


import chardet, re, math, types, urllib
from chardet.universaldetector import UniversalDetector
import ppkf

import FoolParser, PageInfo
from util import fmul


GOMIPAT = re.compile(u"[\(（]\?[\)）]")        
KAIGYOU = re.compile(u"(<(p|br)(/?)>([^\n]))", re.MULTILINE|re.IGNORECASE)
NOT_NEWS = [
    re.compile(r"www\.future\-planning\.net/x/modules/news/")
    ]
def extpage(pageInfo):
    #ページデータに事前フィルタを掛ける。
    #ここでは、HTMLのいらない部分をあらかじめ消去します。

    pageInfo.newsFlag = False
    if re.search("news|headline", re.sub("/[^/]+$", "", pageInfo.url).lower()):
        pageInfo.newsFlag = True
        if "livedoor" in pageInfo.url or "ameba" in pageInfo.url:
            #print "livedoor"
            if re.search(u"<title>[^<]*【", pageInfo.uPageSrc, re.MULTILINE|re.IGNORECASE):
                #print "doc"
                pageInfo.newsFlag = False
    if pageInfo.newsFlag:
        for p in NOT_NEWS:
            if p.search(pageInfo.url):
                pageInfo.newsFlag = False
                break

    pageInfo.body = KAIGYOU.sub(u"<\\2\\3>\n\\4", GOMIPAT.sub(u"", FoolParser.getBody(pageInfo)))
    return [pageInfo.body, pageInfo.newsFlag]
    
def test(url, nobr=False):
    import feedparser
    d = feedparser.parse(url)
    num = 1
    for x in d.entries:
        print "========  %d  ==================" % num
        print "Entry: " + x.link
        print "============================"
        pageInfo = PageInfo.PageInfo(x.link, urllib.urlopen(x.link).read())
        line = extpage(pageInfo)[0]
        line = re.sub(u"<[^>]+>", "", line).strip()
        if line == "":
            continue
        if nobr:
            print line.replace(u"\n", u"")
        else:
            print line
        num += 1
        
if __name__ == "__main__":
    import sys, optparse
    
    parser = optparse.OptionParser(usage=u"""
    ブログのエントリーページから本文記事を抽出します。
    """)
    parser.add_option("-u", "--url", dest="url",
                      help=u"エントリーページURL")
    parser.add_option("-t", "--test", dest="test", action="store_true",
                      help=u"RSSに載っているURLをつかってバッチtestします。-u引数にはRSSを指定してください。", default=False)
    parser.add_option("-n", "--no-br", dest="nobr", action="store_true",
                      help=u"記事から改行を除外します。", default=False)
    (options, args) = parser.parse_args()

    
    if options.test:
        test(options.url, options.nobr)
    else:
        pageInfo = PageInfo.PageInfo(options.url, urllib.urlopen(options.url).read())
        if options.nobr:
            print pageInfo[0].replace("\n", "")
        else:
            print extpage(pageInfo)[0]

