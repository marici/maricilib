# coding: euc_jp

##    Copyright 2007 Hiroshi Ayukawa (email: ayukawa.hiroshi [atmark] gmail.com)
##
##    Licensed under the Apache License, Version 2.0 (the "License");
##    you may not use this file except in compliance with the License.
##    You may obtain a copy of the License at
##
##        http://www.apache.org/licenses/LICENSE-2.0
##
##    Unless required by applicable law or agreed to in writing, software
##    distributed under the License is distributed on an "AS IS" BASIS,
##    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##    See the License for the specific language governing permissions and
##    limitations under the License.

import re
from chardet.universaldetector import UniversalDetector
import ppkf
import FoolParser

nobrPat = re.compile(r"[\r\n]")
METAEXT = re.compile("<meta[^>]+charset=([^\\\">]+)")
def detectEncoding(s):
    pp = ppkf.ppkf(fallback=None)
    g = pp.guess(s)[1]
    if g:
        #print "ppkf-encoding", g
        return g

    detector = UniversalDetector()
    tabun = None
    for line in s.split("\n"):
        m = METAEXT.search(line)
        if m:
            tabun = m.group(1)
            
        detector.feed(line)
        if detector.done: break
    detect = detector.result
    #print "UD-encoding:", detect["encoding"]
    if detect["encoding"] == "EUC-JP":
        return "euc_jp"
    elif detect["encoding"] == "SHIFT_JIS":
        return "shift_jis"
    elif detect["encoding"] == "utf-8":
        return "utf-8"
    elif tabun != None:
        tabun = tabun.lower().replace("-", "").replace("_", "")
        if tabun == "eucjp" or tabun == "xeucjp":
            return "euc_jp"
        elif tabun == "sjis" or tabun == "shiftjis":
            return "shift_jis"
        elif tabun == "utf8":
            return "utf-8"
        else:
            return None
    else:
        #print "文字コード認識できず。"
        #print tabun
        return None

TAGPAT = re.compile(u"<[^>]+>")
NUMPAT = re.compile(u"[0-9]")
JPPAT = re.compile(u"[ぁ-ん一-龠ァ-ヴ]")
def detectLang(src):
    src = FoolParser.removeTagRegion(src, u"script")
    src = FoolParser.removeTagRegion(src, u"style")
    src = NUMPAT.sub("", TAGPAT.sub(u"", FoolParser.removeTagRegion(src, u"pre")))
    jplen = len(JPPAT.findall(src))
    if jplen / float(len(src)) < 0.05:
        return "en"
    return "ja"
