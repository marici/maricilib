# coding: euc_jp

##    Copyright 2007 Hiroshi Ayukawa (email: ayukawa.hiroshi [atmark] gmail.com)
##
##    Licensed under the Apache License, Version 2.0 (the "License");
##    you may not use this file except in compliance with the License.
##    You may obtain a copy of the License at
##
##        http://www.apache.org/licenses/LICENSE-2.0
##
##    Unless required by applicable law or agreed to in writing, software
##    distributed under the License is distributed on an "AS IS" BASIS,
##    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##    See the License for the specific language governing permissions and
##    limitations under the License.

def fmul(*funcs):
    funcs = list(funcs)
    funcs.reverse()
    
    def f(a):
        return reduce(lambda x, g: g(x), funcs, a)
    return f


def removeCommentRegion(src):
    prelen = len(src)
    while "<!--" in src and "-->" in src:
        s = src.index("<!--")
        e = src.index("-->") + 3
        if s >= e: break
        src = src[:s] + src[e:]
        
    return src

        
