#!/usr/local/bin/python
# coding: euc_jp

##    Copyright 2007 Hiroshi Ayukawa (email: ayukawa.hiroshi [atmark] gmail.com)
##
##    Licensed under the Apache License, Version 2.0 (the "License");
##    you may not use this file except in compliance with the License.
##    You may obtain a copy of the License at
##
##        http://www.apache.org/licenses/LICENSE-2.0
##
##    Unless required by applicable law or agreed to in writing, software
##    distributed under the License is distributed on an "AS IS" BASIS,
##    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##    See the License for the specific language governing permissions and
##    limitations under the License.

import re
from operator import itemgetter
from itertools import imap
TAGPAT = re.compile(u"<([^> ]+)[^>]*>")


kugiriMoji = re.compile(u"[\\.\\?!]")
def getBody(pageInfo):
    """
    本文を決定するアルゴリズム。
    @param src  元のHTMLデータ
    @param encoding そのHTMLの文字コード

    本文をunicode文字列として返します。
    """
    body0 = None
    maxMaru = 0
    body = None
    maruSum = 1
    maruMax = 0
    lenMax = 0
    secLen = 0
    body2 = None
    body3 = None
    def sortFunc(a, b):
        if a[0][0] > b[0][0]: return 1
        return -1
    
    # 丸の数の増加率で判定
    for x in sorted(parse(pageInfo)):
        #print "***********************************"
        #print x[1]
        line = removeGomi(x[1])

        #if isCommentStart(line):
        #    break

        #print line,"<br/>"
        #print "---------------------------------------<br/>"
        if isCommentArea(line):
            #print "THIS IS COMMENT!<br/>"
            #print line.encode("euc_jp", "ignore"), "<br/>"
            continue
        
        #if isFootNote(line):
        #    #print "foot"
        #    continue
        if isStopWords(line):
            #print "stop"
            continue
        sentence = TAGPAT.sub(u"", line)
        #print sentence.encode("euc_jp", "ignore"),"<br/>"
        #print "-----------------------<br/>"
        score = len(kugiriMoji.split(sentence))
        #まるの数
        maru = sentence.count(u".")
        
        #print "maru:",maru
        if maru > maxMaru:
            maxMaru = maru
            body0 = line
        #まるの数の増加率
        if score > maruSum * 2:
            body = line
        #それでもだめなら長さで判定するのでそれも記録しておく。
        if lenMax < len(sentence):
            secLen = lenMax
            lenMax = len(sentence)
            body3 = line
            

        maruSum = maruSum + score

    if body0 != None:
        #print "maru_no_kazu=", maxMaru
        return body0
    if body != None:
        #print "maru_no_zokaritsu"
        return body
    if body3 != None:
        #print "nagasa"
        return body3
    return ""

#EN
def removeGomi(line):
    flag = re.IGNORECASE|re.MULTILINE|re.DOTALL
    line = re.sub(u"<a [^>]+my\\.trackword\\.net[^>]*>[^<]*</a>", "", line, flag)
    line = re.split(u"<h[1-4][^>]*>[^<]*(comment)[^<]*</[^>]+>", line, flag)[0]
    return line

def isCommentStart(src):
    return  re.search(u"<[^>]+ (id|class)=[\"']?comment[^>]*>[^<]*(コメント|comment)[^<]*</[^>]+>", src, re.IGNORECASE|re.MULTILINE) != None

#EN
def isCommentArea(src):
    if not u"comment" in src.lower():
        return False
    APAT = re.compile(u"<a [^>]+>[^<]+</a>", re.IGNORECASE)
    DAYPAT = re.compile(u"[0-9]+\\s*/\\s*[0-9]+\\s*/\\s*[0-9]+\\s*")
    MONTHPAT = re.compile(u"\\s(J(an|AN)|F(eb|EB)|M(ar|AR)|A(pr|PR)|M(ay|AY)|J(un|UN)|J(ul|UL)|A(ug|UG)|S(ep|EP)|O(ct|CT)|N(ov|OV)|D(ec|EC))\\.")
    MONTHPAT2 = re.compile(u"january|feburuary|march|april|june|july|august|september|october|november|december", re.IGNORECASE)
    TIMEPAT = re.compile(u"[0-9]\\s*:\\s*[0-9]\\s*")
    NAMEPAT = re.compile(u"name", re.IGNORECASE)
    rawLen = float(len(TAGPAT.sub("",src)))
    sump = 1.0
    for x in src.split(u"\n"):
        p = 1.0
        x = APAT.sub("", x)
        if DAYPAT.search(x):
            p *= 1.1
        if TIMEPAT.search(x):
            p *= 1.1
        if MONTHPAT.search(x) or MONTHPAT2.search(x):
            p *= 1.1
        if NAMEPAT.search(x):
            p *= 1.1
        sump += (p - 1.0)
        sump = sump / rawLen
    if sump > 1.5:
        #print src.encode("euc_jp", "ignore"), "<br/>"
        #Bprint "-----------------------------", sump, "<br/>"

        return True
    
    #print src.encode("euc_jp", "ignore"), "<br/>"
    #print "NO-----------------------------", sump, "<br/>"
    
    return False

def isFootNote(line):
    if re.search(u"class=[\"']foot", line, re.IGNORECASE) and len(line) < 100:
        return True
    return False

def isStopWords(line):
    #if re.search(u"<a href[^>]+>[^<]*音声ブラウザ[^<]*</a>", line, re.IGNORECASE|re.MULTILINE):
    #    return True
    return False



"""
HTMLを解析し、tdとdivの中身をある程度きれいにして返します。
この結果を使う側は、結果からタグを消去すれば純粋な本文が得られます。
タグの情報が必要なら、タグをそのままつかうこともできます。
入力、出力ともにunicodeです。
"""
def parse(pageInfo):
    for data in parseHTML(pageInfo):
        yield [data[0], data[1]]

"""
区切り「。」を適切に入れます。
"""
def kugiri(src):
    return cleanMaru(addMaru(src))

"""
区切るべき部分に「。」を追加する関数。
"""
KUGIRIPAT = re.compile(u"(</?(h[1-7]|li|ul|ol)[^>]*>)", re.IGNORECASE)
def addMaru(src):
    if src.count(u"。") + src.count(u"．")> 5:
        return KUGIRIPAT.sub(u"。\\1", src)
    else:
        return src
    
"""
様々な処理で重複して生まれた「。」を省きます。
"""
CLEANMARU = re.compile(u"。(\\s*|\\s*<[^>]+>\\s*)。")
def cleanMaru(src):
    l = len(src)
    pl = l + 1
    while pl != l:
        pl = len(src)
        src = CLEANMARU.sub(u"。", src)
        l = len(src)
    return src

"""
HTMLを解析して、tdタグの中身とdivタグの中身だけを返すジェネレーター。
その際、
 * blockquoteタグの中身は消去。
 * その他のタグはそのまま残す。
 
"""
DIVALL = re.compile(u"</?div[^>]*>", re.IGNORECASE)
def parseHTML(pageInfo):
    # blockquote, preは不要なので消去。
    src = removeTagRegion(pageInfo.uPageSrc, "blockquote", 0.3)  #たまに全体をblockquoteでくくる人がいるので。
    src = removeTagRegion(src, "pre")
    src = removeTagRegion(src, "script")
    # tableタグ又はdivタグの中身だけを、入れ子を解除しながら取得。
    for data in DivTableParser(src):
        if data[1].lower().startswith(u"<table"):
            # tableタグであればさらにtdごとにわけて返す。
            for x in TableAnalyzer(data[1]):
                yield [data[0] + x[0], x[1]]
        else:
            # divタグはdivの中身をそのまま返す。
            yield [data[0], DIVALL.sub("", data[1])]
    

def tagWalker(src):
    scanner = TAGPAT.scanner(src)
    m = scanner.search()
    while m != None:
        yield (m.start(), m.end(), m.group(1).lower())
        m = scanner.search()
        
def removeTagRegion(src, tag, limit=None):
    if limit:
        limit = int(float(len(src)) * limit)
    tagInfos = filter(lambda x: x[2] == tag or x[2] == u"/" + tag, tagWalker(src))
    cpos = 0
    suruu = False
    buf = []
    rec = 0
    for x in tagInfos:
        if x[2][0] == tag[0]:
            if  rec == 0:
                buf.append(src[cpos:x[0]])
            rec += 1
        if x[2][0] == u"/":
            rec -= 1
            if rec == 0:
                if limit and limit < x[1]- cpos:
                    buf.append(src[cpos:x[1]])
                cpos = x[1]
    buf.append(src[cpos:])
    return u"\n".join(buf)

"""
tableタグと、divタグは少なくとも対応が取れているだろうという仮定
でのパーサー。

"""
DIVTABLE_TAG = re.compile(u"</?(div|table|body)[^>]*>", re.IGNORECASE)
def DivTableParser(src):
    stack = []
    lastErased = []

    scanner = DIVTABLE_TAG.scanner(src)
    res = scanner.search()
    while res != None:
        if not res.group(0).startswith(u"</"):
            stack.append(res)
        else:
            if len(stack) == 0:
                #raise ParseError("Unmatch table or div tag.")
                res = scanner.search()
                continue
            lastTag = stack.pop()

            def processTag(tag):
                if lastTag.group(0).lower().startswith(u"<" + tag):
                    bufBegin = [lastTag.start()]
                    bufEnd = []
                    for span in filter(lambda x: lastTag.start() <= x[0] and x[1] <= res.end(), lastErased):
                        bufEnd.append(span[0])
                        bufBegin.append(span[1])
                    bufEnd.append(res.end())

                    return [lastTag.start(), u"".join(map(lambda x: src[x[0]:x[1]], zip(bufBegin, bufEnd)))]
                else:
                    raise ParseError("Unmatch table or div tag.")
            try:
                if res.group(0).lower().startswith(u"</table"):
                    yield processTag(u"table")
                elif res.group(0).lower().startswith(u"</div"):  # divタグが終わった場合
                    yield processTag(u"div")
                else:
                    yield processTag(u"body")
                lastErased = filter(lambda x: x[0] < lastTag.start() or res.end() < x[1] , lastErased)
                lastErased.append([lastTag.start(), res.end()])
                
            except ParseError:
                #無駄な終了table,divタグは無視する。
                #TODO: 遡ってかろうじて対応しそうなtable,divタグまでスタックを消してしまうべきでは？
                #      ↑tableタグも結構間違えて閉じ忘れるからね。。。。
                pass
            
        res = scanner.search()

"""
単一tableタグの中を解析するパーサー。
tdやtrは閉じ忘れが多いため、特別に処理を行う。
ここで解析するtableタグ内には他のtableやdivタグは含まれないものとする。
"""
TD_TAG = re.compile(u"<td[^>]*>", re.IGNORECASE)
TABLEOTHER_TAG = re.compile(u"</?(table|tr|td)[^>]*>", re.IGNORECASE)
def TableAnalyzer(src):
    scanner = TD_TAG.scanner(src)
    res = scanner.search()
    pos = []
    while res:
        pos.append(res.start())
        res = scanner.search()
    pos.append(-1)
    for i in range(len(pos) - 1):
        yield [pos[i], TABLEOTHER_TAG.sub(u"", src[pos[i]:pos[i + 1]])]
        
class ParseError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)


if __name__ == "__main__":
    #print removeBQ("<html>qqqq<blockquote>aaaaaa1</blockquote>wwww<blockquote>cccc<blockquote>aaaaaa2</blockquote>xxxx</blockquote>cccc2</html>")
    src = unicode(file("sample_divtable.txt", "r").read(), "euc_jp")
    parser = DivTableParser(src)
    i = 1
    #print src
    print "--------------------"
    for data in parser:
        print i, ">", data[0], data[1].encode("euc_jp")
        i += 1
        print "------------------------------------"
