# coding: euc_jp

import re
import Detector, util


class PageInfo:
    def __init__(self, url, pageSrc):
        self.url = url
        self.pageSrc = util.removeCommentRegion(pageSrc)
        self.lang = None
        self.encoding = None
        self.uPageSrc = None

        self.detect()
        if self.uPageSrc == None:
            self.uPageSrc = cleanUnicode(unicode(self.pageSrc, self.encoding, "ignore"))
        m = re.search(u"<title[^>]*>([^<]*)</title>", self.uPageSrc)
        if m:
            self.title = m.group(1)
        else:
            self.title = ""
        
    def detect(self):
        src = util.fmul(
            util.removeCommentRegion,
            lambda x: x.replace("</SCR'+'IPT>", ""),
            )(self.pageSrc)
        
        self.encoding = None
        self.lang = None
        m = re.search(r"<html[^>]+lang=[\"']?(en|ja)", src, re.IGNORECASE)
        if m:
            self.lang = m.group(1).lower()
        m = re.search(r"<meta[^>]+charset=([a-z0-9\-_]+)", src, re.IGNORECASE)
        if m:
            code = m.group(1).lower().replace("_", "").replace("-", "")
            if code in ["eucjp", "xeucjp"]:
                self.encoding = "euc_jp"
                self.lang = "ja"
            elif code in ["shiftjis", "sjis"]:
                #print "meta-encoding", code
                self.encoding = "shift_jis"
                self.lang = "ja"
            elif code == "utf8":
                #print "code:", code
                self.encoding = "utf-8"
            elif code in ["iso88591",]:
                self.lang = "en"

        if self.lang == "en":
            self.encoding = "latin1"
            
        if self.encoding == None:
            self.encoding = Detector.detectEncoding(src)
        #print "self.encoding:", self.encoding
        if self.encoding == None:
            self.encoding = "utf-8"

        if self.lang == None:
            self.uPageSrc = cleanUnicode(unicode(src, self.encoding, "ignore"))
            self.lang = Detector.detectLang(self.uPageSrc)
        #print "lang:", self.lang
        #print "code:", self.encoding
 


"""
Unicode���ݥ��꡼��
"""
# unicode clean1
def replaceUnicode(a, code1, code2):
    return a.replace(unicode(code1, "Unicode-Escape"), unicode(code2, "Unicode-Escape"))

#unicode clean2
def replaceUnicode2(a, code1, char2):
    return a.replace(unicode(code1, "Unicode-Escape"), char2)

#unicode clean 3
def cleanUnicode(s):
    s = replaceUnicode(s, "\\uff5e", "\\u301c")
    s = replaceUnicode(s, "\\u2225", "\\u2016")
    s = replaceUnicode(s, "\\uff0d", "\\u2212")
    s = replaceUnicode(s, "\\uffe0", "\\u00a2")
    s = replaceUnicode(s, "\\uffe1", "\\u00a3")
    s = replaceUnicode(s, "\\uffe2", "\\u00ac")
    s = replaceUnicode(s, "\\u2014", "\\u2015")

    #IBM��ĥʸ��
    s = replaceUnicode2(s, "\\u2160", "I")
    s = replaceUnicode2(s, "\\u2161", "II")
    s = replaceUnicode2(s, "\\u2162", "III")
    s = replaceUnicode2(s, "\\u2163", "IV")
    s = replaceUnicode2(s, "\\u2164", "V")
    s = replaceUnicode2(s, "\\u2165", "VI")
    s = replaceUnicode2(s, "\\u2166", "VII")
    s = replaceUnicode2(s, "\\u2167", "VIII")
    s = replaceUnicode2(s, "\\u2168", "IX")
    s = replaceUnicode2(s, "\\u2169", "X")
    
    s = replaceUnicode2(s, "\\uffe2", "!")
    s = replaceUnicode2(s, "\\u3231", u"��")
    s = replaceUnicode2(s, "\\u2116", "No.")
    s = replaceUnicode2(s, "\\u2121", "Tel")
    s = replaceUnicode2(s, "\\u2235", "...")
    
    s = replaceUnicode(s, "\\u00a0", " ")
    return s

