#!/usr/bin/python
# -*- coding: utf-8 -*-

###########################################################################
#
# Pure Python Kanji Format recognizer - ppkf
#
###########################################################################
#
# Depends on: Python 2.4
#
############################################################################
#
# Copyright (C) 2007 Masaharu Max Goto. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
############################################################################
#
#
# Appendix: Useful informations for CJK codec recognization:
#  G-PROJECT
#   http://www.gprj.net/dev/tips/net/txtenc.shtml - sample C# class
#   http://www.gprj.net/dev/tips/other/kanji.shtml - recognization summary(obsolete but useful)
# $Id$

from array import array
import locale
import codecs

# Scoring constants
WEAK = 1
STRONG = 2

# UTF8/16/32 BOM signatures
BOM_UTF8 = array('B',codecs.BOM_UTF8)
BOM_UTF16BE = array('B',codecs.BOM_UTF16_BE)
BOM_UTF16LE = array('B',codecs.BOM_UTF16_LE)
BOM_UTF32BE = array('B',codecs.BOM_UTF32_BE)
BOM_UTF32LE = array('B',codecs.BOM_UTF32_LE)
BOM_NULL =array('B',(0,0))

#ISO-2022 signatures
JISX0208_1978 = array('B',[0x1b,0x24,0x40])
JISX0208_1983 = array('B',[0x1b,0x24,0x42])
JISX0208_1990_1 = array('B',[0x1b,0x24,0x28,0x44])
JISX0208_1990_2 = array('B',[0x1b,0x26,0x40,0x1b,0x24,0x42])

dbg=False
exotest=False

def debug( text ):
  if dbg:
    print text

class ppkf:
        """Usage: ppkf(self, fallback=None): create recognizer. 'fallback' is a weighted encoding used if ppkf confused.
                methods guess( bytestream ): guess encoding for given bytestream and return encoding name and confidence ratio.
        """
	def __init__( self,fallback=None ):
                if fallback:
                    self.fallback = fallback
                else:
                    self.fallback = locale.getpreferredencoding()
                debug("Instance creation: fallback encoding as %s" % self.fallback )

        def setFallback( self, fallback ):
            """ Set fallback value for codec """
            debug("Set fallback encoding as %s" % fallback )
            self.fallback = fallback

        def getFallback(self):
            """ Get fallback value for codec """
            return self.fallback
            
        def guess( self, data=None ):
            """ Guess encoding from given rawdata. 
                Given: data := Raw data stream to examine.
                Return: [0] := Confidence of recognization from 0 to 1. 0 if not recognized.
                        [1] := Recognized encoding.
            """

            # Flags if any of encodings is estimated.
            is_bom = False
            is_be = False
            is_utf32 = False
            is_utf16 = False
            is_utf8 = False
            is_eucjp = False
            is_sjis = False
            is_sjis_hkana = False
            is_jis = False
            is_broken_eucjp = False
            langdef = "jp"    # most possible for japanese
           
            # Possiblities for each encodings.
            # possible_utf32 =0    # Not used 
            # possible_utf16 = 0   # Not used
            possible_utf8 = 0
            possible_jis = 0
            possible_sjis_hkana = 0
            possible_eucjp = 0
            possible_sjis = 0
                                          
            rawdata = array('B',data)

            delta=1
            is_ascii = True

            for i in range(len(rawdata)):

                c = rawdata[i]

                # STEP[0] : Check if ascii 0x7f is delete, 0x20 is space : assume ascii 
                debug("STEP[0] : Check if ascii.")
                if c > 0x7e or c < 0x20:
                    is_ascii = False
                else:
                    debug("Character %x" % c )

                if delta > 1:
                    debug ("Skipping extra bytes")
                    delta = delta -1
                    continue
 
                                
                # STEP[1] : Check if "ISO-2022-JP"
                debug("STEP[1] : Check if ISO-2022-JP")              
                if rawdata[i:i+len(JISX0208_1978)] == JISX0208_1978 or \
                    rawdata[i:i+len(JISX0208_1983)] == JISX0208_1983 or \
                    rawdata[i:i+len(JISX0208_1990_1)] == JISX0208_1990_1 or\
                    rawdata[i:i+len(JISX0208_1990_2)] == JISX0208_1990_2 :
                    # iso-2022-jp :  JIS X 0208-1978, JIS X 0208-1983, JIS X 0208-1990, JIS X 2013
                        debug("Estimated: ISO-2022-JP")
                        is_jis = True
                        langdef = "jp"
                        break

                # STEP[2] : Check if "UTF-16/32"
                # ToDo: python generates strings NOT including BOM for UTF16-BE and UTF16-LE
                debug("STEP[2] : Check if UTF-16/32 BE/LE")
                #if c == 0 and rawdata[i+1] == 0 and \
                #    (i+2)<len(rawdata) and \
                #    rawdata[i+2]==0xfe and rawdata[i+3]==0xff :
                if rawdata[i:i+4] == BOM_UTF32BE:
                    # Estimated UTF-32 BE
                    debug("Estimated: UTF-32 BE")
                    is_utf32 = True
                    is_be = True
                    delta = 4
                    break
                    
                #elif c == 0xfe and rawdata[i+1] == 0xff:
                elif rawdata[i:i+2] == BOM_UTF16BE:
                    # if  i>2 and rawdata[i-2]==0 and rawdata[i-1]==0:
                    if rawdata[i+2:i+4] == BOM_NULL:
                        debug("Estimated: UTF-32 BE")
                        is_utf32 = True
                        is_be = True
                        delta = 4
                        break
                    else:
                        debug("Estimated: UTF-16 BE")
                        is_utf16 = True
                        is_be = True
                        delta = 2
                        break
                # elif c == 0xff and rawdata[i+1] == 0xfe:
                elif rawdata[i:i+2] == BOM_UTF16LE:
                    #if 2>len(rawdata) and \
                    #    rawdata[i+2]==0 and rawdata[i+3]==0 :
                    if rawdata[i+2:i+4] == BOM_NULL:
                        # Estimated UTF-32 LE
                        debug("Estimated: UTF-32 LE")
                        is_utf32 = True
                        is_be = False
                        delta = 4
                        break
                    else:
                        debug("Estimated: UTF-16 LE")
                        is_utf16 = True
                        is_le = False
                        delta = 2
                        break
                else:
                     pass
                                
                # STEP[3] : Check if "UTF-8/UTF-8N"
                debug("STEP[3] : Check if UTF-8(with BOM)/UTF-8N(without BOM")
                if c >= 0xc2 and c <= 0xfd:
                    
                    # Guess length of codepoint
                    # Though 6,5 bytes codes are obsolete, here i define for backward compatibilities
                    delta=0
                    while ( c & 0xfc ) & ( 0x80 >> delta ):
                        delta = delta +1

                    if delta == 0:
                        delta =1
                        debug("Estimated character is 0x%x" % c )

                    # Code start with 0xfc or 0xfe is obsolete.
                    if delta >= 5:
                        debug("Possible obsolete UTF-8 for RFC2279.")
                        # delta = 1
                        # continue

                    debug("Expected bytes for this codepoint = %d" % delta)

                    if (i+delta) > len(rawdata) :
                        debug("Corrupted codepoint - not enough bytes to go; Not UTF-8/UTF-8N")
                        #if possible_utf8:
                        #    possible_utf8 = 0
                        # delta = 1
                        # continue
                    
                    # Check BOM
                    if rawdata[i:i+len(BOM_UTF8)] == BOM_UTF8:
                        # BOM found
                        debug("Estimated: UTF-8(with BOM)")
                        is_bom = True
                        delta = 3
                        break
                    else:
                        is_bom = False

                    # Strongly possible: 'utf-8'
                    if i+delta <= len(rawdata):
                        for j in range(i+1,i+delta):
                            if rawdata[j] < 0x80 or rawdata[j] > 0xbf:
                                debug("Corrupted codepoint - not in 0x81 - 0xbf ; Not UTF-8/UTF-8N")
                                # Doubt about utf-8: skip and try another
                                # pass
                                delta=1
                                break
                        else:
                            debug ("Perehaps utf-8")
                            possible_utf8 = possible_utf8 + STRONG
                            continue                          
                                
                    else:
                        debug("Corrupted codepoint - not enough bytes to go ; doubt in UTF-8/UTF-8N")
                        delta=1
                    
                    # if is_utf8:
                    #    possible_utf8 = possible_utf8 + STRONG
                        
                debug("STEP[4] : check if eucjp half width")
                # STEP[4] : Check if euc half-width kana
                if c == 0x8e:
                    debug("Check if eucjp half-width kana")
                    # half-width kana : follow byte ?
                    if rawdata[i+1] >= 0xa1 and rawdata[i+1] <= 0xdf:
                        if possible_sjis <STRONG and possible_utf8 <STRONG and\
                            possible_sjis_hkana <STRONG :
                            possible_eucjp = possible_eucjp + STRONG
                            debug("Perhaps eucjp")
                            delta=2
                            continue
                    
                    elif rawdata[i+1] >= 0x80 and rawdata[i+1]<=0xa0:
                        debug("Estimated: Shift JIS")
                        is_sjis = True
                        break
                    else:
                        continue

                debug("STEP[5] : Check if eucjp extra code")
                # STEP [5] : Check eucjp extra code
                if c == 0x8f:
                    if rawdata[i+1] >= 0xa1 and rawdata[i+1] <= 0xfe and \
                        rawdata[i+2] >= 0xa1 and rawdata[i+2] <= 0xfe:
                        # Possible eucjp eucjp extra codearea.
                        if possible_sjis <STRONG and possible_utf8 <STRONG:
                            debug("Perhaps eucjp")
                            possible_eucjp = possible_eucjp + STRONG
                            delta = 3
                            continue
                    elif rawdata[i+1] == 0xfd or rawdata[i+1] == 0xfe or \
                        rawdata[i+2] == 0xfd or rawdata[i+2] == 0xfe:
                        # No doubt, eucjp extra codearea.
                        debug("Estimated: eucjp")
                        # possible_eucjp = possible_eucjp + STRONG
                        is_eucjp = True
                        #result ="eucjp"
                        break
                    elif rawdata[i+1] >= 0x80 and rawdata[i+1] <= 0xa0 and \
                        rawdata[i+2] >=  0x80 and rawdata[i+2] <= 0xa0:
                        debug("Estimated: sjis")
                        is_sjis = True
                        break

                debug("STEP[6] : Check if sjis")
                # STEP[6] : Check if Shift-JIS  
                if c >= 0x80 and c <= 0xa0:
                    debug("Estimated: sjis")
                    is_sjis = True
                    break

                # STEP[7] : Check if Shift-JIS half-width kana
                debug("STEP[7] : Check if sjis half-width kana")
                if c>= 0xa1 and c <=0xdf and possible_utf8<STRONG:
                    # possible_sjis = possible_sjis + STRONG
                    # possible_eucjp = possible_eucjp + STRONG

                    if  c == 0xa4 and rawdata[i+1]>=0xa1 and rawdata[i+1]<=0xf3:
                        debug("Possible eucjp Zenkaku Hira Kana")
                        possible_eucjp = possible_eucjp + WEAK
                        delta = 2
                        continue

                    elif  c == 0xa5 and rawdata[i+1]>=0xa1 and rawdata[i+1]<=0xf6:
                        debug("Possible eucjp Zenkaku Kata Kana")
                        possible_eucjp = possible_eucjp + WEAK
                        delta = 2
                        continue

                    elif len(rawdata) > (i+1):
                        if rawdata[i+1] >= 0xe0 and rawdata[i+1] <= 0xFE:
                            debug("Perhaps eucjp")
                            possible_eucjp = possible_eucjp +STRONG
                            if rawdata[i+1] == 0xfd or rawdata[i+1] == 0xfe:
                                debug("Estimated: eucjp")
                                is_eucjp = True
                                break
                            delta = 2
                            continue
                        else:
                            debug("Possible sjis Hankaku-kana")
                            possible_sjis_hkana = possible_sjis_hkana + STRONG
                            # continue
                                                        
                    else:
                        debug("Possible sjis hankaku kana")
                        possible_sjis_hkana = possible_sjis_hkana + STRONG
                        # debug ("Corrupted codepoint.")
                        # delta = 1

                # possible_sjis = possible_sjis + STRONG
                # continue

                # STEP[8] : Check if eucjp
                debug("STEP[8] : Check if eucjp")
                if c>= 0xa1 and c <= 0xfe:
                    if len(rawdata[i:])>=2: 
                        if rawdata[i+1] >=0xa1 and rawdata[i+1]<=0xfe :
                            debug("Perhaps eucjp")
                            possible_eucjp = possible_eucjp + STRONG
                            delta=2
                            if c == 0xfd or c == 0xfe:
                                is_eucjp = True
                                break
                    else:
                        debug("Possible broken eucjp.")
                        is_broken_eucjp = True
                        possible_eucjp = 0
                        is_sjis_hkana = True
                        continue
                
            # check scoreboards
            debug("****Checking scoreboards****")
            debug("UTF-8 ; score = %d, bom = %s estimated = %s" % (possible_utf8,is_bom, is_utf8) )
            debug("UTF-16 ; estimated = %s bom=%s be=%s" % (is_utf16,is_bom,is_be))
            debug("UTF-32 ; estimated = %s bom=%s be=%s" % (is_utf32,is_bom,is_be))
            debug("SJIS  ; score = %d, estimated = %s" % (possible_sjis,is_sjis) )
            debug("(SJIS-HKANA  ; score = %d)" % (possible_sjis_hkana) )
            debug("EUCJP ; score = %d, estimated = %s" % (possible_eucjp,is_eucjp) )
            debug("JIS   ; score = %d, estimated = %s" % (possible_jis,is_jis) )

            # If explicitly set return as detected.
            if is_ascii:
                return 1,"ascii"
            elif is_utf8:
                if is_bom:
                    return 1,"utf_8"
                else:
                    return 1,"utf_8n"
                    # return 1,"utf_8"
            elif is_eucjp:
                return 1,"euc_jp"
            elif is_jis:
                return 1,"iso2022_%s" % langdef
            elif is_sjis:
                return 1,"shift_jis"
            elif is_sjis_hkana:
                return 1,"shift_jis"
            elif is_utf16:
                if is_be:
                    return 1,"utf_16_be"
                    # return 1,"utf_16"
                else:
                    return 1,"utf_16_le"
                    # return 1,"utf_16"
            elif is_utf32:
                if is_be:
                    return 1,"utf_32_be"
                else:
                    return 1,"utf_32_le"
            else:
              
                possible_sum = float(possible_eucjp + possible_sjis + possible_jis + possible_utf8 + possible_sjis_hkana)
                if possible_eucjp > possible_sjis and \
                    possible_eucjp > possible_utf8 and \
                    possible_eucjp > possible_jis and\
                    not is_broken_eucjp:
                        return possible_eucjp/possible_sum, "euc_jp"
                elif possible_sjis > possible_jis and \
                    possible_sjis > possible_utf8 and \
                    possible_sjis > possible_eucjp:
                        return possible_sjis/possible_sum, "shift_jis"
                elif possible_sjis_hkana > possible_jis and \
                    possible_sjis_hkana > possible_utf8 and \
                    ( possible_eucjp < WEAK or is_broken_eucjp ):
                        return possible_sjis_hkana/possible_sum, "shift_jis"
                elif possible_jis > possible_sjis and \
                    possible_jis > possible_utf8 and \
                    possible_jis > possible_eucjp:
                        return possible_jis/possible_sum, "iso2022_%s" % langdef
                elif possible_utf8 > possible_sjis and \
                    possible_utf8 > possible_jis and \
                    possible_utf8 > possible_eucjp:
                        if is_bom:
                            return possible_utf8/possible_sum, "utf_8"
                        else:
                            return possible_utf8/possible_sum, "utf_8"
                else:
                    pass
                    # return 0,self.fallback
                        
            return 0,self.fallback
                
                
if __name__ == "__main__":

    # Builtin unit test code for ppkf.py
    # dbg=True
    dbg=False
    exotest=False
    p = ppkf()
    # Test cases
    examples = ( 
                # Japanese Texts
                u"〜",
                u"ﾊﾝｶｸｶﾅｷﾀｰ",
                u"ﾊ",
                u"か",
                u"ｦ",
                u"ヴ",
                u"残るSJIS EUC UTF-8の判別",
                u"椎名林檎",
                u"ダイナマイト",
                u"ﾊﾝｶｸｶﾅ日本語alphabet美乳",
                u"い",
                # SBCS en
                u"Hotel Carifornia",
                u"#1$%",
                # Exotic locales : Maybe fails
                u"這份文件也有pdf版本。(簡体中文)",
                u"Анна Каренина(Russian)",
                u"是生成以XML(繁体中文)",
                u"Wenn Sie schrittweise nach dem Tutorial lernen möchten.(German)",
                u"제목 그대로.. mp3 태그 편집할 수 있는 프로그램 좀 소개시켜주세요.(Korean)"
                )
    encnames = ("utf_8","euc_jp","iso2022_jp","shift_jis","utf_16","utf_16_be","utf_16_le")

    # Begin UT:Guess cases
    passcount = 0
    failcount = 0
    checkcount = 0
    print "=============================\nUnit test for ppkf starts.\n============================="
    print "-----------------------------"
    print "case 1 - methods."
    print "-----------------------------"
    checkcount = checkcount + 1
    try:
        print "Default getFallback() == %s" % p.getFallback()
    except:
        failcount = failcount + 1
    passcount = passcount + 1
    for fb in encnames:
            try:
                p.setFallback(fb)
            except:
                print "FAIL: setFallback('%s')." % fb
                failcount = failcount + 1
                
            print "PASS: setFallback('%s')." % fb
            passcount = passcount + 1
            
            try:
                rfb = p.getFallback()
                if  rfb == fb:
                    print "PASS: getFallback() == %s." % rfb
                    passcount = passcount +1
                else:
                    print "FAIL: getFallback() != %s but %s." % (fb,rfb)
                    failcount = failcount +1
            except:
                print "FAIL: ppkf internal error..."
                failcount = failcount +1
    
    print "-----------------------------"
    print "case 2 - guess various texts."
    print "-----------------------------"
    for srcstr in examples:
        print "Source String = %s" % srcstr.encode("utf-8")
        for enc in encnames:
            try: 
                if enc == "utf_16_be":
                    src_enc = codecs.BOM_UTF16_BE + srcstr.encode("utf_16_be")
                elif enc =="utf_16_le":
                    src_enc = codecs.BOM_UTF16_LE + srcstr.encode("utf_16_le")
                else:
                    src_enc =  srcstr.encode(enc)
            except:
                print "CHECK: Not supported encode :%s" % enc
                checkcount = checkcount + 1
            else:
                debug ("Guess %s encoded string." % enc)
                try:
                    result = p.guess(src_enc)
                except:
                    print "FAIL: ppkf internal error..."
                    failcount = failcount +1
                else:
                    if result[1] != enc:
                        if enc == "utf_16" and ( result[1] == "utf_16_le" or result[1] == "utf_16_be" ):
                            print "PASS: %s with confidence %4.2f." % (result[1],result[0] )
                            passcount = passcount +1
                        else:
                            print "CHECK: %s taken as %s is correct result ?" % (enc, result[1])
                            checkcount = checkcount +1
                    else:
                        print "PASS: %s with confidence %4.2f." % (result[1],result[0] )
                        passcount = passcount +1
        print "---"
    print "=============================\nUnit test for ppkf ends.\n============================="
    print "Test cases result TOTAL %d [ PASS = %d / FAIL = %d / CHECK = %d ]" %(passcount+failcount+checkcount,passcount,failcount,checkcount)
