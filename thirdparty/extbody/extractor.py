#!/usr/local/Python25/bin/python
# _*_ coding: euc_jp _*_

##    Copyright 2007 Hiroshi Ayukawa (email: ayukawa.hiroshi [atmark] gmail.com)
##
##    Licensed under the Apache License, Version 2.0 (the "License");
##    you may not use this file except in compliance with the License.
##    You may obtain a copy of the License at
##
##        http://www.apache.org/licenses/LICENSE-2.0
##
##    Unless required by applicable law or agreed to in writing, software
##    distributed under the License is distributed on an "AS IS" BASIS,
##    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##    See the License for the specific language governing permissions and
##    limitations under the License.

import urllib
import re

import PageInfo, ExtpageJP, ExtpageEN

def ext(info):
    if info.lang == "ja":
        return ExtpageJP.extpage(info)
    elif info.lang == "en":
        return ExtpageEN.extpage(info)
    else:
        raise Exception("can not detect lang!")
    
def extbodyByUrl(url):
   pageInfo = PageInfo.PageInfo(url, urllib.urlopen(url).read())
   #print pageInfo.lang
   #print pageInfo.encoding
   return ext(pageInfo)
   
def test(url, nobr=False):
    import feedparser
    d = feedparser.parse(url)
    num = 1
    for x in d.entries:
        print "========  %d  ==================" % num
        print "Entry: <a href='%s'>%s</a>" % (x.link, x.link)
        print "============================"
        num += 1
        try:
            res = extbodyByUrl(x.link)
        except Exception, e:
            print "Failed!!"
            print e
            continue
        line = re.sub(u"<[^>]+>", "", res[0]).strip()
        if line == "":
            continue
        try:
            if nobr:
                print line.replace(u"\n", u"")
            else:
                print line
        except Exception, e:
            print "Erroe:", e
        
if __name__ == "__main__":
    import sys, optparse
    
    parser = optparse.OptionParser(usage=u"""
    ブログのエントリーページから本文記事を抽出します。
    """)
    parser.add_option("-u", "--url", dest="url",
                      help=u"エントリーページURL")
    parser.add_option("-t", "--test", dest="test", action="store_true",
                      help=u"RSSに載っているURLをつかってバッチtestします。-u引数にはRSSを指定してください。", default=False)
    parser.add_option("-n", "--no-br", dest="nobr", action="store_true",
                      help=u"記事から改行を除外します。", default=False)
    (options, args) = parser.parse_args()

    
    if options.test:
        test(options.url, options.nobr)
    else:
        if options.nobr:
            
            print extbodyByUrl(options.url)[0].replace("\n", "")
        else:
            print extbodyByUrl(options.url)[0]

