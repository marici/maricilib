# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import logging
#logging.basicConfig(level=logging.INFO)

import re, urllib, socket
from Queue import Queue
from threading import Thread
_num_worker_threads = 20
socket.setdefaulttimeout(10)

bbs_list_url = 'http://menu.2ch.net/bbstable.html'
a_tag_pat = re.compile(r'<a[^>]*href="([^"]*)"[^>]*>(.*?)</a>', re.I | re.S)
dirty_a_tag_pat = re.compile(r'<a[^>]*href="?([^ >]*)"?[^>]*>(.*?)</a>', re.I | re.S)
bbs_url_pat = re.compile(r'http://.+\.2ch\.net/[^/]/')
thread_title_pat = re.compile(r'\d+: (.*) \((\d+)\)')
pink_bbs_url_pat = re.compile(r'http://.+\.bbspink\.com/')
exclude_bbs_url_set = set(('http://www.2ch.net/',))

def open_bbs_info(include_pink=False):
    return get_bbs_info(urllib.urlopen(bbs_list_url), include_pink)

def get_bbs_info(page, include_pink=False):
    bbs_list = []
    html = page.read().decode('sjis', 'ignore')
    for match in dirty_a_tag_pat.findall(html):
        url = match[0]
        title = match[1]
        if not include_pink and is_pink(url): continue
        if url.endswith('/') and url not in exclude_bbs_url_set:
            bbs_list.append(dict(url=url, title=title))
    page.close()
    return bbs_list

def open_subback(base_url=None, bbs_info=None):
    if base_url is None: base_url = bbs_info.get('url')
    url = '%ssubback.html' % base_url
    try:
        page = urllib.urlopen(url)
    except:
        return []
    return get_subback(base_url, page, bbs_info)

def get_subback(base_url, page, bbs_info=None):
    thread_list = []
    html = page.read().decode('sjis', 'ignore')
    for match in a_tag_pat.findall(html):
        url = match[0]
        title = match[1]
        m = thread_title_pat.match(title)
        if m:
            thread_list.append(dict(
                url=base_url + url, 
                title=m.group(1), 
                responses=m.group(2),
                is_pink=is_pink(base_url),
                bbs_info=bbs_info))
    return thread_list
        
def is_pink(url):
    return pink_bbs_url_pat.match(url) is not None

def worker(queue, results, callback=None):
    while True:
        bbs_info = queue.get()
        logging.info('Opening %s', bbs_info.get('url'))
        try:
            for thread_info in open_subback(bbs_info=bbs_info):
                logging.debug('Thread %s', thread_info.get('url'))
                if callback:
                    callback(thread_info)
                else:
                    results.append(thread_info)
        except Exception, e:
            logging.warn(e)
        finally:
            queue.task_done()

def crawl(include_pink=True, callback=None):
    queue = Queue(20)
    results = []
    logging.info('start 2ch crawl.')
    for i in range(_num_worker_threads):
        t = Thread(target=worker, args=(queue, results, callback))
        t.setDaemon(True)
        t.start()
    for bbs_info in open_bbs_info(include_pink):
        queue.put(bbs_info)
    queue.join()
    logging.info('finished 2ch crawl.')
    return results

def main():
    import sys
    crawl()
    #for bbs in get_bbs_info(file(sys.argv[1])):
    #    get_subback(bbs.get('url'))
    #get_subback(file(sys.argv[1]))

if __name__ == '__main__':
    main()
