# -*- coding: utf-8 -*-
import sys, urllib, re, types, urlparse, time
from datetime import datetime
from maricilib.thirdparty import extbody

br_pat = re.compile(r'[\r\n]+')
tag_pat = re.compile(r'<[^>]*>')
link_pat = re.compile(r'<a[^>]*href="([^"]*)"[^>]*>', re.I)
title_pat = re.compile(r'<title>([^<]*)</title>', re.I)
date_pat = re.compile(u'(\d{4})[年/\.]\s*(\d{1,2})[月/\.]\s*(\d{1,2})日?', re.U)

def crawl(start_url, pattern, urlvalidator=None):
    if not pattern:
        pattern = start_url
    page = urllib.urlopen(start_url)
    url_set = set()
    for m in link_pat.finditer(page.read()):
        url = m.group(1)
        if not url.startswith('http'):
            url = urlparse.urljoin(start_url, url)
        if url == start_url: continue
        if re.match(pattern, url):
            url_set.add(url)
    page.close()
    for url in url_set:
        if urlvalidator and not urlvalidator(url): continue
        time.sleep(3)
        real_url, title, body, published_at = get_page_info(url)
        yield url, real_url, title, body, published_at

def get_page_info(url):
    page = urllib.urlopen(url)
    pageInfo = extbody.PageInfo.PageInfo(url, page.read())
    body, newsFlag = extbody.extractor.ext(pageInfo)
    real_url = page.geturl()
    page.close()

    body = tag_pat.sub('', body)
    body = br_pat.sub('\n', body)

    published_at = None
    date_set = set()
    for m in date_pat.finditer(body):
        date_set.add(m.group(0))
    if len(date_set) == 0:
        for m in date_pat.finditer(tag_pat.sub('', pageInfo.uPageSrc)):
            date_set.add(m.group(0))
    if len(date_set) == 1:
        year = m.group(1)
        month = m.group(2)
        day = m.group(3)
        if year.isdigit() and month.isdigit() and day.isdigit():
            published_at = datetime(int(year), int(month), int(day))
        
    m = title_pat.search(pageInfo.uPageSrc)
    title = m.group(1) if m else ''
    return real_url, title, body, published_at

if __name__ == '__main__':
    for line in sys.stdin:
        site_name, url, pattern = line.strip().split(',')
        crawl(url, pattern)

