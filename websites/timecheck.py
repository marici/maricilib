# -*- coding: utf-8 -*-
import re
import sys
import time
import urllib
import urlparse
from datetime import datetime

a_tag_pat = re.compile(r'<a[^>]*href="([^"]*)"[^>]*>(.*?)</a>', re.I | re.S)
index_paths = (
    '/index.html',
    '/index.php',
)
interval = 10


def main():
    url = sys.argv[1]
    max_depth = int(sys.argv[2])
    times = {}
    crawl(url, url, max_depth, times=times)
    ranking = sorted(times.items(), key=lambda x:x[1], reverse=True)
    for index, (url, seconds) in enumerate(ranking):
        print u'%s. %s (%s秒)' % (index + 1, url, seconds)


def crawl(start_url, root_url, max_depth, fetched=None, depth=0, times={}):
    urls = []
    if start_url == root_url + '/' or root_url == start_url + '/':
        return urls
    fetched = fetched or set()
    html, seconds = get(start_url)
    fetched.add(start_url)
    times[start_url] = seconds
    print start_url, seconds
    for match in a_tag_pat.findall(html):
        url = match[0]
        url = urlparse.urljoin(start_url, url)
        for path in index_paths:
            if url.endswith(path):
                url = url.replace(path, '/')
        if url.startswith(root_url) and url not in fetched:
            urls.append(url)
            fetched.add(url)
    if depth < max_depth:
        for url in urls:
            time.sleep(interval)
            _urls = crawl(url, root_url, max_depth, fetched, depth + 1, times)
            urls.extend(_urls)
    return urls

def get(url):
    start = datetime.now()
    try:
        html = urllib.urlopen(url).read()
    except:
        print 'error', url
    end = datetime.now()
    delta = end - start
    return html, delta.total_seconds()


if __name__ == '__main__':
    main()
