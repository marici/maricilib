# coding: utf-8
import re, urlparse

tag_pat = re.compile(r'<[^>]*>')
br_pat = re.compile(r'[\r\n]+')

mark_lines = {
    'www.nikkansports.com' : (u'<!-- +++++ ニュース本文 +++++ -->', u'<!-- /+++++ ニュース本文 +++++ -->'),
    'www2.asahi.com' : ('<!--googleon:index-->', '<!--googleoff:index-->'),
    'mainichi.jp' : (u'<!-- ||ここから自動 $Kiji.Page || -->', '<div class="RelatedArticle">'),
    'www.yomiuri.co.jp' : ('<!--// headline_start //-->', '<!--// article_end //-->'),
    'sankei.jp.msn.com' : ('<div id="articleTextnews1" class="parent chrome1">', '<div id="RelatedImg" class="parent">')
}
def ext_body(url, html):
    body_lines = []
    site = urlparse.urlparse(url).netloc
    start_line = mark_lines.get(site)[0]
    end_line = mark_lines.get(site)[1]
    in_body = False
    for line in html.split('\n'):
        if start_line in line:
            in_body = True
        elif in_body and end_line in line:
            break
        elif in_body:
            body_lines.append(line)
    text = '\n'.join(body_lines)
    text = tag_pat.sub('', text)
    text = br_pat.sub('\n', text).strip()
    print text
    return text

