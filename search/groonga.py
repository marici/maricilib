# -*- coding: utf-8 -*-
import json
import time
import urllib
import logging
from datetime import datetime

GRN_SUCCESS = 0

class NotAvailable(Exception):
    pass


class Drilldown(object):

    def __init__(self, column, sortby='-_nsubrecs',
           offset=0, limit=10):
        params = self.params = {}
        if type(column) in (str, unicode):
            params['drilldown'] = column
        else:
            params['drilldown'] = ','.join(column)
        if type(sortby) in (str, unicode):
            params['drilldown_sortby'] = sortby
        else:
            params['drilldown_sortby'] = ','.join(sortby)
        params['drilldown_offset'] = str(offset)
        params['drilldown_limit'] = str(limit)


class MatchColumns(object):

    def __init__(self, columns):
        params = self.params = {}
        if type(column) in (str, unicode):
            params['match_columns'] = match_columns
        else:
            match_columns = [ '%s * %s' % (c[0], c[1])
                if isinstance(c, tuple) else c
                for c in columns ]
            params['match_columns'] = ' || '.join(match_columns)


class Result(object):

    def __init__(self, jsonobj):
        info = jsonobj[0]
        self.return_code = info[0]
        self.start_time = info[1]
        self.process_time = info[2]
        
        self.search_result = None
        self.drilldown = []

        if self.return_code == GRN_SUCCESS:
            self.search_result = SearchResult(jsonobj[1][0])
            if 1 < len(jsonobj[1]):
                for obj in jsonobj[1][1:]:
                    self.drilldown.append(SearchResult(obj))
        else:
            self.error_msg = info[3]

    @property
    def hits(self):
        return self.search_result.hits if self.search_result else 0

    @property
    def objects(self):
        return self.search_result.objects if self.search_result else []


class SearchResult(object):

    def __init__(self, jsonobj):
        self.hits = jsonobj[0][0]
        self.columns = jsonobj[1]
        self.results = jsonobj[2:]
        self._objects = None

    @property
    def objects(self):
        if self._objects is None:
            self._objects = []
            for result in self.results:
                obj = {}
                for (colname, coltype), value in zip(self.columns, result):
                    obj[colname] = value
                self._objects.append(obj)
        return self._objects


class Groonga(object):

    def __init__(self, url=None, output=None):
        if url and url.endswith('/'): url = url[:-1]
        self.url = url
        self.output = output
        
    def datetime(self, value):
        try:
            return time.mktime(value.timetuple())
        except:
            return ''
    
    def ensure_key(self, docs):
        for doc in docs:
            if '_key' not in doc: raise ValueError()
            
    def load(self, table, docs):
        self.ensure_key(docs)
        for doc in docs:
            delete_keys = []
            for key, value in doc.iteritems():
                if value is None:
                    delete_keys.append(key)
                elif value == 'null':
                    doc[key] = None
                elif isinstance(value, datetime):
                    doc[key] = self.datetime(value)
            for dkey in delete_keys:
                del doc[dkey]
       
        values = json.dumps(docs, ensure_ascii=False)
        if self.output is not None:
            self.output.write('load --table %s --input_type json\n' % table)
            self.output.write(values.encode('utf-8'))
            self.output.write('\n')
        if self.url is not None:
            params = {}
            params['table'] = table
            params['input_type'] = 'json'
            params['values'] = values.encode('utf-8')
            url = self.url + '/d/load?' + urllib.urlencode(params)
            resp = urllib.urlopen(url)
            jsonstr = resp.read()
            resp.close()
            ret_values = json.loads(jsonstr)
            if len(ret_values) != 2: return 0
            if len(ret_values) != 2:
                logging.warn('groonga load error: %s', json)
                return 0
            return ret_values[1]
        return len(docs)

    def select(self, table, match_columns=None, query=None, filter=None,
            sortby=None, drilldown=None, offset=0, limit=10):
        if self.url is None: raise NotAvailable('Groonga URL is not specified.')
        params = {}
        params['table'] = table

        if isinstance(match_columns, MatchColumns):
            params.update(match_columns.params)
        elif type(match_columns) in (str, unicode):
            params['match_columns'] = match_columns
        elif match_columns:
            params['match_columns'] = ','.join(match_columns)

        if query: params['query'] = query.encode('utf-8')
        if filter: params['filter'] = filter.encode('utf-8')

        if type(sortby) in (str, unicode):
            params['sortby'] = sortby
        elif sortby:
            params['sortby'] = sortby

        if drilldown: params.update(drilldown.params)
        params['offset'] = str(offset)
        params['limit'] = str(limit)

        url = self.url + '/d/select?' + urllib.urlencode(params)
        resp = urllib.urlopen(url)
        jsonstr = resp.read()
        resp.close()
        ret_values = json.loads(jsonstr)
        return Result(ret_values)
                
    def delete(self, table, key):
        if self.output is not None:
            self.output.write('delete %s %s\n' % (table, key))
        if self.url is not None:
            params = {'table':table, 'key':key}
            url = self.url + '/d/delete?' + urllib.urlencode(params)
            resp = urllib.urlopen(url)
            jsonstr = resp.read()
            resp.close()
            ret_values = json.loads(jsonstr)
            return ret_values[1]
        return 1
        

if __name__ == '__main__':
    groonga = Groonga('http://localhost:10041')

    drilldown = Drilldown(['title', '_key'])
    result = groonga.select('Site', 'title', u'マップ', sortby='_key',
        drilldown=drilldown)
    print result.hits
    for obj in result.objects:
        print obj['_key'], obj['title']
    for obj in result.drilldown[0].objects:
        print obj['_key'], obj['_nsubrecs']
    for obj in result.drilldown[1].objects:
        print obj['_key'], obj['_nsubrecs']

    result = groonga.select('Site', filter='_key == "http://example4.jp/"')
    print result.hits
    for obj in result.objects:
        print obj['_key'], obj['title']


