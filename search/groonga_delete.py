from groonga import Groonga

groonga = Groonga('http://localhost:8080')

while True:
    result = groonga.select('ex_friend')
    if result.hits == 0: break
    for obj in result.objects:
        groonga.delete('ex_friend', obj['_key'])

