import logging
import urllib
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from xml.etree import ElementTree

logging.getLogger().setLevel(logging.INFO)

url = "http://boss.yahooapis.com/ysearch/web/v1/%s?%s"
default_params = dict(
    appid = "Gr3ACffV34FMq..CvWk6wscvR2mFHSxr4R_FN0dQkpcE8wiIa8sRdCgbxC.3gz7_",
    format = "xml",
    lang = "jp",
    region = "jp"
)
namespace = "http://www.inktomi.com/"
Path = lambda name: ElementTree.QName(namespace, name).text

def search(query, **params):
    apiurl = url % (urllib.quote(query.encode("utf-8")), getparams(**params))
    logging.debug(apiurl)
    return parse(urllib.urlopen(apiurl))
    
def getparams(**params):
    p = dict(default_params)
    if params:
        p.update(params)
    logging.debug(urllib.urlencode(p))
    return urllib.urlencode(p)

def parse(file):
    results = []
    et = ElementTree.parse(file)
    rset = et.find(Path("resultset_web"))
    elements = rset.findall(Path("result"))
    logging.debug("results: %s", len(elements))
    for el in elements:
        logging.debug(el.findtext(Path("url")))
        logging.debug(el.findtext(Path("dispurl")))
        logging.debug(el.findtext(Path("title")))
        logging.debug(el.findtext(Path("abstract")))
        results.append(dict(
            url=el.findtext(Path("url")),
            dispurl=el.findtext(Path("dispurl")),
            title=el.findtext(Path("title")),
            abstract=el.findtext(Path("abstract"))
        ))
    return results

def main():
    import sys
    search(sys.argv[1].decode("utf-8"))
    #parse("bosssearch.xml")

if __name__ == "__main__":
    main()

