# -*- coding: utf-8 -*-

DATABASE_NAME = 'ffmini'
DATABASE_USER = 'root'
DATABASE_PASSWORD = ''
DATABASE_HOST = 'localhost'
DATABASE_PORT = 3306
DATABASE_CHARSET = 'utf8'

LIBPATH = '../lib'

import os
import sys
if not LIBPATH:
    LIBPATH = os.path.join(os.path.dirname(__file__), 'lib')
if LIBPATH not in sys.path:
    sys.path.append(LIBPATH)
