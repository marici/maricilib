# -*- coding:utf-8 -*-
import sys
import codecs
import logging
import optparse

import settings
from models import *
from bayesianfilter import BayesianFilter

MENS_CLASS = 'mens'
LADIES_CLASS = 'ladies'


def run(options, args):
    setup('mysql://%s:%s@%s:%s/%s?charset=%s' % (
        settings.DATABASE_USER,
        settings.DATABASE_PASSWORD,
        settings.DATABASE_HOST,
        settings.DATABASE_PORT,
        settings.DATABASE_NAME,
        settings.DATABASE_CHARSET))
    bayesianfilter = BayesianFilter()
    if args:
        for pk in args:
            item = Item.get_by(id=pk)
            test(bayesianfilter, item)
    else:
        for item in Item.query.all():
            test(bayesianfilter, item)
    bayesianfilter.close()


def test(bayesianfilter, item):
    cls = None
    attrs = ('original_category', 'name')
    for attr in attrs:
        cls = get_class(getattr(item, attr, None))
        if cls:
            break
    if cls is None:
        lp = bayesianfilter.test(LADIES_CLASS, item.description)
        mp = bayesianfilter.test(MENS_CLASS, item.description)
        print item.id, item.name, item.original_category, 'F:', lp, 'M', mp
        #best = bayesianfilter.check((LADIES_CLASS, MENS_CLASS), item.description)
        #print item.id, item.name, best


def get_class(text):
    if not text: return None
    if u'レディースライク' in text:
        return MENS_CLASS
    if u'メンズライク' in text:
        return LADIES_CLASS
    if u'レディース' in text:
        return LADIES_CLASS
    if u'メンズ' in text:
        return MENS_CLASS
    if u'女性用' in text:
        return LADIES_CLASS
    if u'男性用' in text:
        return MENS_CLASS
    return None


def main():
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-o', '--output-encoding', dest='outencoding',
        default='utf-8')
    parser.add_option('-v', '--verbose', action='store_true',
        dest='verbose', default=False)
    parser.add_option('--groonga-url', dest='groonga_url',
        default=None)
    parser.add_option('--mysql-user', dest='mysql_user',
        default=settings.DATABASE_USER)
    parser.add_option('--mysql-password', dest='mysql_password',
        default=settings.DATABASE_PASSWORD)
    parser.add_option('--mysql-database', dest='mysql_database',
        default=settings.DATABASE_NAME)
    parser.add_option('--mysql-host', dest='mysql_host',
        default=settings.DATABASE_HOST)
    parser.add_option('--mysql-charset', dest='mysql_charset',
        default=settings.DATABASE_CHARSET)
    options, args = parser.parse_args()

    log_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    log_handler.setFormatter(formatter)
    logging.getLogger().addHandler(log_handler)
    if options.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)

    sys.stdout = codecs.getwriter(options.outencoding)(sys.stdout)

    run(options, args)

if __name__ == '__main__':
    main()
