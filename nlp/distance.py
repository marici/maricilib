# coding: utf-8
import math, logging
from tfidf import TFVector

class Distance(object):

    def levenshtein(self, a, b, edit_cost=1):
        m = [ [0] * (len(b) + 1) for i in xrange(len(a)+1) ]
        for i in xrange(len(a)+1):
            m[i][0] = i
        for j in xrange(len(b)+1):
            m[0][j] = j
        for i in xrange(1, len(a)+1):
            for j in xrange(1, len(b)+1):
                x = edit_cost
                if a[i-1] == b[j-1]:
                    x = 0
                m[i][j] = min(m[i-1][j] + 1, m[i][j-1] + 1, m[i-1][j-1] + x)
        return m[-1][-1]

    def normalized_levenshtein(self, a, b, edit_cost=2, boost=0.0, length_bias=1.1):
        max_cost = max(len(a), len(b)) if edit_cost == 1 else len(a) + len(b)
        d = self.levenshtein(a, b, edit_cost)
        if d == 0: return 1.0
        if length_bias: max_cost = int(max_cost * length_bias)
        logging.debug('%s / %s', d, max_cost)
        if boost:
            max_cost *= math.pow(1.5 - float(d) / max_cost, boost)
        return 1.0 - min(1.0, (float(d) / max_cost))

    def cosine(self, fd1, fd2):
        ip = 0
        for t in fd1:
            if t in fd2: ip += fd1[t] * fd2[t]
        norm1 = 0
        for t in fd1:
            norm1 += math.pow(fd1[t], 2)
        norm2 = 0
        for t in fd2:
            norm2 += math.pow(fd2[t], 2)
        if norm1 == 0 or norm2 == 0:
            return 0.0
        else:
            return ip / (math.sqrt(norm1) * math.sqrt(norm2))


class TokensDistance(Distance):

    filter = (u'名詞', u'動詞', u'形容詞', u'副詞')

    def cosine(self, tokens1, tokens2, filter=None):
        if filter is None: filter = self.filter
        tf1 = TFVector(tokens1.filter(filter))
        tf2 = TFVector(tokens2.filter(filter))
        return Distance.cosine(self, tf1, tf2)
        
    def normalized_levenshtein(self, tokens1, tokens2, filter=None):
        if filter is None: filter = self.filter
        basics1 = [ t.basic for t in tokens1.filter(filter) ]
        basics2 = [ t.basic for t in tokens2.filter(filter) ]
        return Distance.normalized_levenshtein(self, basics1, basics2)

def main():
    import morpheme
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-f', '--file', dest='filename', metavar='FILE',
        default=None, help='Specify file')
    (options, args) = parser.parse_args()

    encoding = 'utf-8'

    distance = TokensDistance()
    if options.filename:
        for line in file(options.filename):
            line = line.strip().decode(encoding)
            if not line: continue
            a, b = line.split(',')
            print a[:10], b[:10], \
                distance.cosine(morpheme.analyze(a), 
                    morpheme.analyze(b))
    else:
        a = args[0].decode(encoding)
        b = args[1].decode(encoding)
        print a, b, \
            distance.normalized_levenshtein(morpheme.analyze(a), 
                morpheme.analyze(b))

if __name__ == '__main__':
    main()
