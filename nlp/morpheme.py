# coding: utf-8
import logging
import types, re, unicodedata

try:
    import MeCab
except ImportError:
    MeCab = None

logger = logging.getLogger(__name__)


class TokenList(list):

    poslist1 = (u'名詞 一般', u'名詞 固有名詞', u'名詞 サ変接続', u'名詞 接尾 人名')
    poslist2 = (u'記号 一般', u'記号 アルファベット')
    NON_SAY_BASICS = (u'おろか', u'まだしも', u'ともかく')

    def all_subtokens(self, poslist, skip_parenthesis=True):
        l = []
        skip = False
        skip_level = 0
        s_tokens = TokenList()
        for t in self:
            if t.is_open_parenthesis():
                skip_level += 1
                skip = True
            elif t.is_close_parenthesis():
                skip_level -= 1
                if skip_level == 0: skip = False
            elif t.is_parenthesis():
                if skip_level == 0: skip = not skip
            elif not (skip_parenthesis and skip):
                if t.pos_in(poslist):
                    s_tokens.append(t)
                elif s_tokens:
                    l.append(s_tokens)
                    s_tokens = TokenList()
        if s_tokens: l.append(s_tokens)
        return l

    def subtokens(self, poslist, ostart=0, reverse=False, skip_parenthesis=False):
        s_tokens = TokenList()
        if reverse:
            target = TokenList(reversed(self.oslice(0, ostart)))
        else:
            target = self.oslice(ostart)
        skip = False
        skip_level = 0
        for t in target:
            if (not reverse and t.is_open_parenthesis()) or \
                    (reverse and t.is_close_parenthesis()):
                skip_level += 1
                skip = True
            elif (not reverse and t.is_close_parenthesis()) or \
                    (reverse and t.is_open_parenthesis()):
                skip_level -= 1
                if skip_level == 0: skip = False
            elif t.is_parenthesis():
                if skip_level == 0: skip = not skip
            elif not (skip_parenthesis and skip):
                if t.pos_in(poslist):
                    s_tokens.append(t)
                else:
                    break
        if reverse: s_tokens.reverse()
        return s_tokens

    def filter(self, poslist):
        return TokenList([ t for t in self if t.pos_in(poslist) ])
            
    def exclude(self, poslist):
        return TokenList([ t for t in self if not t.pos_in(poslist) ])
            
    def truncate(self, target):
        new_tokens = TokenList()
        text = ''.join([ t.surface for t in self ])
        for i in range(len(self)):
            if target and text.startswith(target):
                target = target.replace(self[i].surface, '', 1)
            else:
                new_tokens.append(self[i])
            text = text.replace(self[i].surface, '', 1)
        return new_tokens
                
    def truncate_after(self, target):
        new_tokens = TokenList()
        for token in self:
            if target in token.surface:
                break
            new_tokens.append(token)
        return new_tokens

    def truncate_between(self, start, end):
        new_tokens = TokenList()
        skip = False
        for token in self:
            if start in token.surface: skip = True
            elif end in token.surface: skip = False
            elif not skip: new_tokens.append(token)
        return new_tokens

    def subject(self, poslist=None, reverse=True, stop=False):
        if poslist is None: poslist = (u'名詞 一般', u'名詞 固有名詞')
        candidates = []
        s_tokens = TokenList()
        skip = False
        skip_level = 0

        for i, t in enumerate(self):
            if t.pos_in(u'記号 句点'):
                break
            elif stop and t.surface in (u'に', u'へ'):
                break
            elif t.pos_in(u'助詞 係助詞'):
                if i == len(self) -1 or self[i+1].surface not in self.NON_SAY_BASICS:
                    if s_tokens.pos_in(poslist):
                        candidates.append((s_tokens, True))
                s_tokens = TokenList()
            elif t.surface == u'が' and t.pos_in(u'助詞 格助詞'):
                if s_tokens.pos_in(poslist): candidates.append((s_tokens, False))
                s_tokens = TokenList()
            elif t.surface == u'から':
                if s_tokens.pos_in(poslist): candidates.append((s_tokens, False))
                s_tokens = TokenList()
            else:
                if t.is_open_parenthesis():
                    skip_level += 1
                    skip = True
                elif t.is_close_parenthesis():
                    skip_level -= 1
                    if skip_level == 0: skip = False
                elif t.is_parenthesis():
                    if skip_level == 0: skip = not skip
                else:
                    if skip:
                        continue
                    if t.pos_in(u'名詞') and not t.pos_in(u'名詞 非自立'):
                        s_tokens.append(t)
                    elif s_tokens:
                        s_tokens = TokenList()

        if candidates:
            if reverse: candidates = list(reversed(candidates))
            for c, strong in candidates:
                if strong: return c, strong
            for c, strong in candidates:
                if c.pos_in(u'名詞 固有名詞'): return c, False
            else:
                return candidates[-1][0], False
        else:
            if s_tokens.pos_in(poslist): return s_tokens, False
            return TokenList(), False

    def contains(self, *basics):
        for t in self:
            if t.basic in basics: return True
        return False

    def pos_in(self, poslist):
        for t in self:
            if t.pos_in(poslist):
                return True
        return False

    def pos_another(self, poslist):
        for t in self:
            if not t.pos_in(poslist):
                return True
        return False

    def sentence_bound(self, indices, default):
        skip = False
        for i in indices:
            if self[i].is_parenthesis():
                skip = not skip
            else:
                if skip: continue
                elif self[i].pos_in(u'記号 句点'):
                    return i + 1
        else:
            return default

    def sentence(self, ostart):
        for si, t in enumerate(self):
            if ostart <= t.index: break
        start = self.sentence_bound(reversed(range(si)), 0)
        end = self.sentence_bound(range(si, len(self)), len(self))
        return TokenList(self[start:end])

    def sentences(self):
        l = []
        sentence = TokenList()
        skip_level = 0
        for t in self:
            sentence.append(t)
            if skip_level <= 0 and t.pos_in(u'記号 句点'):
                l.append(sentence)
                sentence = TokenList()
                skip_level = 0
            if t.is_open_parenthesis():
                skip_level += 1
            elif t.is_close_parenthesis():
                skip_level -= 1
        if sentence: l.append(sentence)
        return l

    def reset_indices(self):
        text = unicode(self)
        pos = 0
        for t in self:
            t.index = pos
            pos += len(t.surface)
            
    def __str__(self):
        return ''.join([ t.encoded_surface for t in self ])

    def __unicode__(self):
        return ''.join([ t.surface for t in self ])

    def string(self):
        return unicode(self)

    def oget(self, start):
        for i, t in enumerate(self):
            if start <= t.index: return t
        else:
            return None

    def oindex(self, substring):
        text = unicode(self)
        return text.index(substring)

    def oslice(self, start, end=None):
        si = -1
        for i, t in enumerate(self):
            if si == -1 and start <= t.index:
                if end is None: return TokenList(self[i:])
                si = i
            if end is not None and end <= t.index:
                return TokenList(self[si:i])
        else:
            return TokenList(self[si:])

    def resemble(self, tl):
        for i in range(min(len(self), len(tl))):
            if self[i].surface == tl[i].surface:
                return True
        else:
            return False

    def debug(self):
        for t in self:
            print t.surface, t.pos


class Token(object):

    def __init__(self, surface, basic, pos, encoding, encoded, index):
        self.index = index
        if encoded:
            self.surface = surface.decode(encoding)
            self.basic = basic.decode(encoding)
            self.pos = pos.decode(encoding)
            self.encoded_surface = surface
            self.encoded_basic = basic
            self.encoded_pos = pos
        else:
            self.surface = surface
            self.basic = basic
            self.pos = pos
            self.encoded_surface = surface.encode(encoding)
            self.encoded_basic = basic.encode(encoding)
            self.encoded_pos = pos.encode(encoding)

    def pos_in(self, *poslist, **kwargs):
        if 'exact' in kwargs:
            return self.pos in poslist
        for pos in poslist:
            if self.pos.startswith(pos):
                return True
        return False

    def is_parenthesis(self):
        try:
            name = unicodedata.name(self.surface) if len(self.surface) == 1 else ''
        except:
            return False
        return self.pos_in(u'記号 括弧') or \
            (name.endswith('PARENTHESIS') or 'BRACKET' in name or \
             name == 'FULLWIDTH EQUALS SIGN')
        
    def is_open_parenthesis(self):
        try:
            name = unicodedata.name(self.surface) if len(self.surface) == 1 else ''
        except:
            return False
        return (name.endswith('LEFT PARENTHESIS') or \
             re.match('LEFT .* BRACKET', name))

    def is_close_parenthesis(self):
        try:
            name = unicodedata.name(self.surface) if len(self.surface) == 1 else ''
        except:
            return False
        return (name.endswith('RIGHT PARENTHESIS') or \
             re.match('RIGHT .* BRACKET', name))

    def __str__(self):
        return '%s %s' % (self.encoded_surface, self.encoded_pos)

    def __unicode__(self):
        return '%s %s' % (self.surface, self.pos)

    def __hash__(self):
        return hash(self.surface) ^ hash(self.pos)

    def __eq__(self, other):
        return self.surface == other.surface and self.pos == other.pos


def analyze(s=None, **kwargs):
    if kwargs:
        tokenlists = dict()
        for key in kwargs:
            tokenlists[key] = analyze_by_mecab(kwargs[key])
        return tokenlists
    else:
        return analyze_by_mecab(s)

def analyze_by_mecab(text, option=''):
    tokens = TokenList()
    index = 0
    tagger = MeCab.Tagger(option)
    encode_text = text.encode('utf-8')
    node = tagger.parseToNode(encode_text)
    while node:
        if node.surface != '':
            try:
                surface = node.surface.decode('utf-8')
                ft = node.feature.decode('utf-8').split(',')
                basic = ft[6] if 6 < len(ft) and ft[6] != '*' else surface
                pos = u' '.join(ft[:3])
                token = Token(surface, basic, pos, 'utf-8', False, index)
                tokens.append(token)
                index += len(token.surface)
            except Exception, e:
                logger.warn(e)
        node = node.next
    return tokens


def main():
    import sys, pickle
    options = ' '.join(sys.argv[1:])
    for line in iter(sys.stdin.readline, ''):
        if not line: break
        tokens = analyze_by_mecab(line.decode('utf-8').strip(), options)
        for token in tokens:
            print token.surface, token.basic, token.pos, token.index

if __name__ == '__main__':
    main()
