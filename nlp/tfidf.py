# coding: utf-8
import os, math, shelve, logging

class TFVector(dict):

    @classmethod
    def clone(cls, other):
        vector = cls()
        vector.update(other)
        vector.max_tf = other.max_tf
        return vector

    def __init__(self, tokens=None, keep_tokens=False):
        dict(self)
        self.keep_tokens = keep_tokens
        self.max_tf = 0
        if tokens:
            for token in tokens:
                tf =  self.tget(token, 0) + 1
                self.set(token, tf)

    def tget(self, token, default=None):
        key = token if self.keep_tokens else str(hash(token))
        return self.get(key, default)

    def set(self, token, freq):
        key = token if self.keep_tokens else str(hash(token))
        self[key] = freq
        self.max_tf = max(self.max_tf, freq)

    def get_log_tf(self, token):
        tf = math.log10(1.0 + self.tget(token))
        return tf
    
    def get_an_tf(self, token):
        tf = 0.5 + 0.5 * self.tget(token) / self.max_tf
        logging.info("AN_TF:%s:%s" % (token, tf))
        return tf

    def __add__(self, other):
        vector = self.__class__.clone(self)
        for key, freq in other.iteritems():
            vector[key] = vector.get(key, 0) + freq
        return vector

    def __div__(self, number):
        vector = self.__class__.clone(self)
        for key, freq in self.iteritems():
            vector[key] = vector.get(key, 0) / number
        return vector
        
    def __str__(self):
        return '{ %s }' % ', '.join(
            [ '\'%s\':%s' % (t, q) for t, q in self.iteritems() ])

    def __unicode__(self):
        return '{ %s }' % ', '.join(
            [ '\'%s\':%s' % (unicode(t), q) for t, q in self.iteritems() ])


class DF(object):
    
    LEN_KEY = '[Number of documents]'

    def __init__(self, basedir='.', prefix='', extension='db'):
        prefix = '%s-' % prefix if prefix else ''
        filename  = os.path.join(basedir, '%sdf.%s' % (prefix, extension))
        self.db = shelve.open(filename)
        self.dn = self.db.get(self.LEN_KEY, 0)
        logging.info('Open document frequency database: %s', filename)

    def close(self):
        self.db.close()

    def add(self, tokens):
        for token in set(tokens):
            self.db[str(token)] = self.db.get(str(token), 0) + 1
            logging.debug(token, self.db[str(token)])
        self.dn += 1
        self.db[self.LEN_KEY] = self.db.get(self.LEN_KEY, 0) + 1

    def get(self, token):
        return self.db.get(str(token), 0)

    def get_idf(self, token):
        if not self.dn: self.dn = self.db.get(self.LEN_KEY, 0)
        df = self.get(token)
        if df:
            return math.log(float(self.dn) / df, self.dn)
        else:
            return 1.0
    

def main():
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-p', '--prefix', dest='prefix', metavar='PREFIX',
        default='', help='add prefix to filename')
    parser.add_option('-t', '--test', dest='test', action='store_true',
        default=False, help='test')
    (options, args) = parser.parse_args()

    encoding = 'utf-8'

    import sys, morpheme
    df = DF(prefix=options.prefix)
    for filename in args:
        text = file(filename).read().decode(encoding)
        tokens = morpheme.analyze(text)
        if options.test:
            tf = TFVector(tokens)
            for token in tokens:
                print token, tf.get_an_tf(token) * df.get_idf(token)
        else:
            df.add(tokens)
    if not options.test: print len(args), 'documents added.'
    df.close()

if __name__ == '__main__':
    main()
