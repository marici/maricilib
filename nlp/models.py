# -*- coding: utf-8 -*-
from elixir import *

def setup(bind, echo=True):
    metadata.bind = bind
    metadata.echo = echo
    setup_all()
    create_all()


class Item(Entity):
    using_options(tablename='items', autoload=True)
