#!/usr/bin/env python
# coding: utf-8
import re, logging, unicodedata
import morpheme
import bayesianfilter

logger = logging.getLogger(__name__)

REMARK_CLASS = '1'
SUBJECT_FILTER = (u'名詞 固有名詞 人名',)
NOUN_FILTER = (u'名詞 一般', u'名詞 サ変接続', u'名詞 固有名詞', u'記号 一般')
SAYS_BASICS = (u'いう', u'言う', u'話す', u'語る', u'明かす')

def overwrap(str1, str2):
    return (str1 in str2 or str2 in str1)

def is_katakana(tokens):
    for t in tokens:
        for c in t.surface:
            if not t.pos_in(u'記号') and not unicodedata.name(c).startswith('KATAKANA'):
                return False
    return True
    
class RemarkExtractor(object):
    
    pat1 = re.compile(u'「([^」]*)」(（([^）]*)）)?', re.U)
    pat2 = re.compile(r'\s+', re.U)
    pat3 = re.compile(r'(</p>|<br ?/?>|[\r\n]+)', re.I)
    #pat4 = re.compile(u'[\(（]\d+[\)）]?(＝[^＝]*＝[\)）]?)?', re.U)
    pat4 = re.compile(u'[\(（]\d+', re.U)

    def __init__(self, text=None, bsfilter=None):
        if bsfilter is None:
            bsfilter = bayesianfilter.BayesianFilter()
        self.bsfilter = bsfilter

    def analyze_text(self, text):
        for line in self.pat3.split(text):
            line = line.strip()
            if not line: continue
            tokens = morpheme.analyze(line)
            self.tokenlines.append(tokens)
            self.tokens.extend(tokens)
        self.tokens.reset_indices()

    def extract(self, text, basic_subject=None, basic_names=[]):
        self.tokenlines = []
        self.tokens = morpheme.TokenList()
        self.analyze_text(text)

        ret_remarks = []
        self.basic_names = basic_names
        self.basic_subject = basic_subject
        self.basic_subject_distance = 0
        self.subject = None
        self.subject_distance = 0
        count = 0
        for tokens in self.tokenlines:
            sentences = tokens.sentences()
            for i, sentence in enumerate(sentences):
                sentence.reset_indices()

                if not self.basic_subject and count == 0:
                    self.basic_names = self.get_names(sentence)
                    logger.debug('Basic Names %s', ','.join([ unicode(n) for n in self.basic_names ]))
                    if len(self.basic_names) == 1:
                        self.basic_subject = self.basic_names[0]

                next = sentences[i+1] if i + 1 != len(sentences) else None
                remarks = []
                pos = 0
                s = sentence
                subject, remark_tokens, startpos, endpos = self.get_remark(sentence, pos)
                if remark_tokens:
                    while True:
                        if remark_tokens:
                            if remark_tokens.pos_another((u'名詞', u'助詞 連体化', u'記号')):
                                remark = unicode(remark_tokens)
                                if subject:
                                    ret_remarks.append((subject, remark))
                                else:
                                    target = sentence.oslice(pos).truncate(remark)
                                    subject = self.get_subject(target, has_remark=True, remark_start=startpos, 
                                        remark_end=endpos, next=next)
                                    if subject:
                                        subject_pos = subject[-1].index + len(subject[-1].surface)
                                        endpos = max(endpos, subject_pos)
                                    subject = subject or self.basic_subject
                                    if subject and not self.overwrap_basic_name(subject) and \
                                            not subject.pos_in(u'名詞 固有名詞') and not is_katakana(subject):
                                        subject = None
                                    ret_remarks.append((subject, remark))
                            s = s.oslice(endpos+1)
                            pos = endpos
                            subject, remark_tokens, startpos, endpos = self.get_remark(sentence, pos)
                        else:
                            break
                else:
                    # 発言とは無関係に主語を取得
                    first = count == 0
                    subject = self.get_subject(sentence, next=next, has_remark=False, is_first=first)

                count += 1
        return ret_remarks

    def get_names(self, sentence):
        names = []
        pos = 0
        while True:
            m = self.pat4.search(unicode(sentence), pos)
            if m is None: break
            name = sentence.subtokens(NOUN_FILTER + SUBJECT_FILTER, m.start(0), True, True)
            names.append(name)
            pos = m.end(0)
        if not names:
            for tokens in sentence.all_subtokens(NOUN_FILTER):
                if tokens.pos_in(SUBJECT_FILTER): names.append(tokens)
        return names

    def overwrap_basic_name(self, target):
        if not self.basic_names: return False
        target = unicode(target)
        for name in self.basic_names:
            if overwrap(target, unicode(name)): return True
        else:
            return False

    def get_subject(self, sentence, remark_start=-1, remark_end=-1, next=False, 
            has_remark=False, is_first=False):
        if remark_start != -1 and remark_end != -1:
            prefix = sentence.oslice(0, remark_start)
            sbj, strong = prefix.subject(NOUN_FILTER)
            if sbj: logger.debug('Subject for prefix %s %s', unicode(sbj), unicode(prefix))
            if not sbj:
                suffix = sentence.oslice(remark_end).truncate_between(u'「', u'」')
                sbj, strong = suffix.subject(NOUN_FILTER, reverse=False, stop=True)
                if sbj: logger.debug('Subject for suffix %s %s', unicode(sbj), unicode(suffix))
        else:
            sbj, strong = sentence.subject(NOUN_FILTER)
            if sbj: logger.debug('Subject %s %s', unicode(sbj), unicode(sentence))


        if sbj and self.basic_subject:
            self.basic_subject_distance += 1
            sbj_str = unicode(sbj)
            basic_str = unicode(self.basic_subject)
            if sbj_str and overwrap(sbj_str, basic_str):
                self.basic_subject_distance -= 3
            if 5 < self.basic_subject_distance:
                self.basic_subject = None

        if sbj:
            if sbj.pos_in(SUBJECT_FILTER) or is_katakana(sbj):
                if strong or has_remark:
                    self.subject = sbj
                    self.subject_distance = 0
                if is_first and not self.basic_subject:
                    self.basic_subject = sbj
            elif strong:
                clear = len(sbj) != 1
                if clear and self.subject is not None:
                    for name in self.basic_names:
                        if self.subject.resemble(name):
                            clear = False
                            break
                if clear:
                    self.subject = None
                    self.subject_distance = 0
            else:
                sbj = False
        else: 
            if next and next.contains(*SAYS_BASICS):
                sbj, strong = next.subject()

        if sbj:
            passive = False
            for token in sbj:
                if token.surface == u'に' and token.pos == u'助詞,格助詞':
                    passive = True
                if passive and token.surface == u'れ' and token.pos == u'動詞 接尾':
                    sbj = None
                    break

        if not sbj and self.subject:
            if self.subject_distance < 3:
                sbj = self.subject
                self.subject_distance += 1
        return sbj

    def get_remark(self, tokens, pos):
        match = self.pat1.search(unicode(tokens), pos)
        if match:
            remark_tokens = tokens.oslice(match.start(1), match.end(1))
            p = self.bsfilter.test(REMARK_CLASS, tokens=remark_tokens)
            logger.debug('Remark proberbility %s %s', match.group(1), p)
            if 0.9 < p:
                return match.group(3), remark_tokens, match.start(0), match.end(0)
        return None, None, 0, 0


def main():
    import sys, os, codecs
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true',
        default=False, help='verbose')
    parser.add_option('-t', '--test', dest='test', action='store_true',
        default=False, help='test')
    parser.add_option('--write-test', dest='write_test', action='store_true',
        default=False, help='write testcase')
    (options, args) = parser.parse_args()

    if options.verbose:
        logger.setLevel(logging.DEBUG)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    bsfilter = bayesianfilter.BayesianFilter()
    extractor = RemarkExtractor(bsfilter)
    test_count = 0
    success_count = 0
    for arg in args:
        text = file(arg).read().decode('utf-8')
        remarks = extractor.extract(text)

        testname = '%s.test' % arg
        if options.test:
            if os.path.exists(testname):
                for i, line in enumerate(file('%s.test' % arg)):
                    line = line.strip().decode('utf-8')
                    if not line: continue
                    s, r = line.split(',')
                    subject, remark = remarks[i]
                    if s and s != unicode(subject):
                        print 'expected %s: %s' % (s, subject)
                    elif r and r != unicode(remark):
                        print 'expected %s: %s' % (r, remark)
                    else:
                        success_count += 1
                    test_count += 1
        else:
            if options.write_test:
                f = codecs.getwriter('utf-8')(file(testname, 'w'))
            for subject, remark in remarks:
                if subject: out = '%s,%s' % (unicode(subject), unicode(remark))
                else: out = ',%s' % remark
                print out
                if options.write_test: f.write(out + '\n')
            if options.write_test:
                f.close()
    if options.test:
        print 'success', success_count, 'failure', test_count - success_count
    bsfilter.close()

if __name__ == '__main__':
    main()
