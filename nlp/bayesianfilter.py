# coding: utf-8
import os
import sys
import math
import glob
import shelve
import logging
import morpheme

LEN_KEY = '[Number of documents]'


class BayesianFilter(object):

    def __init__(self, basedir='.', prefix='bayes', extension='db'):
        self.dbdict = {}
        self.basedir = basedir
        self.prefix = prefix
        self.extension = extension
        for filename in glob.glob(os.path.join(basedir, '%s-*.%s' % (prefix, extension))):
            cls = os.path.basename(filename)[len(prefix)+1:-len(extension)-1]
            self.dbdict[cls] = shelve.open(filename)
            logging.debug('Open bayesiain filter database: %s', filename)

    def open_db(self, cls):
        db = self.dbdict.get(cls)
        if not db:
            db = self.dbdict[cls] = shelve.open(
                os.path.join(self.basedir, '%s-%s.%s' % (self.prefix, cls, self.extension)))
        return db

    def close(self):
        for db in self.dbdict.values():
            db.close()

    def get_tokens(self, text, tokens, poslist=None):
        if tokens is not None: return tokens
        if text is None: return None
        tokens = morpheme.analyze(text=text)['text']
        if poslist:
            tokens = [token for token in tokens if token.pos_in(poslist)]
        return tokens

    def learn(self, cls, text=None, tokens=None):
        db = self.open_db(cls)
        tokens = self.get_tokens(text, tokens)
        tokenset = set()
        for token in tokens:
            tokenset.add(str(token))
        if not tokenset:
            return tokens
        for tokenstr in tokenset:
            freq = db.get(tokenstr, 0) + 1
            db[tokenstr] = freq
            logging.debug('Learned %s %s', tokenstr, freq)
        db[LEN_KEY] = db.get(LEN_KEY, 0) + 1
        return tokens

    def score(self, cls, text=None, tokens=None):
        tokens = self.get_tokens(text, tokens)
        score = self.priorprob(cls)
        for token in set(tokens):
            score *= self.weightedprob(token, cls)
        return score

    def priorprob(self, cls):
        db = self.open_db(cls)
        return float(db.get(LEN_KEY, 0)) / self.totalcount()

    def tokenprob(self, token, cls):
        db = self.open_db(cls)
        count = db.get(str(token), 0)
        if count:
            return float(count) / db.get(LEN_KEY, 0)
        else:
            return 0

    def weightedprob(self, token, cls, weight=1.0, ap=0.5):
        p = self.tokenprob(token, cls)
        total = self.totalfreq(token)
        bp = ((weight * ap) + (total * p)) / (weight + total)
        return bp

    def tokenprob_(self, token, cls):
        db = self.open_db(cls)
        prob = \
            (db.get(str(token), 0) + 1.0) / \
            (self.totalcount() + \
            len(db) * 1.0)
        logging.debug('%s %s %s', cls, unicode(token), prob)
        return prob

    def totalfreq(self, token):
        num = 0
        for dbcls in self.dbdict:
            db = self.dbdict[dbcls]
            num += db.get(str(token), 0)
        return num

    def totalcount(self):
        num = 0
        for dbcls in self.dbdict:
            db = self.dbdict[dbcls]
            num += db.get(LEN_KEY, 0)
        return num

    def check_(self, classes, text=None, tokens=None):
        if not tokens:
            tokens = self.get_tokens(text, tokens)
        scores = {}
        maxscore = 0.0
        best = None
        for cls in classes:
            score = self.score(cls, None, tokens)
            scores[cls] = score
            if maxscore < score:
                maxscore = score
                best = cls
        for cls in scores:
            if cls == best: continue
            if scores[best] < scores[cls] * 5.0:
                return None
        return best

    def check(self, classes, text=None, tokens=None):
        tokens = self.get_tokens(text, tokens)
        #tokens = [token for token in tokens
        #        if token.pos_in((u'名詞', u'形容詞'))]
        scores = {}
        for cls in classes:
            scores[cls] = self.priorprob(cls)
        checked = False
        for token in set(tokens):
            maxp = -sys.maxint
            minp = sys.maxint
            maxcls = None
            tp_dict = {}
            for cls in classes:
                tp = self.weightedprob(token, cls)
                maxp = max(maxp, tp)
                minp = min(minp, tp)
                tp_dict[cls] = tp
            rate = maxp / minp
            if 2.0 < rate:
                checked = True
                for cls in classes:
                    scores[cls] *= tp_dict[cls] * rate
        if not checked: return None
        maxscore = -sys.maxint
        minscore = sys.maxint
        maxcls = None
        for cls, score in scores.iteritems():
            if maxscore < score:
                maxscore = score
                maxcls = cls
            if score < minscore:
                minscore = score
                mincls = cls
        if 2.0 < maxscore / minscore:
            return maxcls
        else:
            return None

    def test(self, cls, text=None, tokens=None):
        return self.robinson(cls, text, tokens)

    def graham(self, cls, text=None, tokens=None):
        token_dict = {}
        pw_list = []
        tokens = self.get_tokens(text, tokens)
        if not tokens: return 0
        for token in set(tokens):
            p = self.calc_p(cls, token)
            logging.debug('Test score %s %s', token, p)
            pw_list.append(p)
        # 0.5から離れている順にソート
        pw_list.sort(lambda x, y: -cmp(abs(x-0.5), abs(y-0.5)))
        pw_list = pw_list[:15]
        #sorted_list = sorted(pw_list)
        #head = sorted_list[:10]
        #tail = sorted_list[10:]
        #pw_list = head[:7] + tail[-7:]
        product = reduce(lambda x, y: x * y, pw_list)
        denominator = product + reduce(lambda x, y: x * y, [ 1.0 - x for x in pw_list ])
        p = product / denominator
        logging.debug('%s / %s = %s | %s', product, denominator, p,
                ','.join([str(pw) for pw in pw_list]))
        return p

    def calc_p(self, cls, token):
        numerator = 0.0
        denominator = 0.0
        total = 0
        for dbcls in self.dbdict:
            db = self.dbdict[dbcls]
            number = db.get(str(token), 0)
            total += number
            if token.surface == u'貝':
                print u'貝', number, db.get(LEN_KEY)
            rate = min(1.0, number / float(db.get(LEN_KEY)))
            denominator += rate
            if dbcls == cls:
                numerator = rate
        if total < 5 or not numerator:
            return 0.4
        else:
            return max(0.1, min(0.99, numerator / denominator))

    def robinson(self, cls, text=None, tokens=None):
        token_dict = {}
        fw_list = []
        _fw_list = []
        fw_sum = _fw_sum = 0
        tokens = self.get_tokens(text, tokens)
        if not tokens: return 0
        for token in set(tokens):
            f = self.calc_f(cls, token)
            fw_list.append(f)
            _fw_list.append(1-f)
            logging.debug('Test score %s %s', token, f)
            fw_sum += f
            _fw_sum += 1 - f
        p = 1 - pow(reduce(lambda x, y: x * y, _fw_list), 1.0 / len(_fw_list))
        q = 1 - pow(reduce(lambda x, y: x * y, fw_list), 1.0 / len(fw_list))
        s = (p - q) / (p + q)
        s2 = (1 + s) / 2
        return s2

    def calc_f(self, cls, token):
        x = 0.5
        s = 1.0
        numerator = 0.0
        denominator = 0.0
        total = 0
        n = 0
        for dbcls in self.dbdict:
            db = self.dbdict[dbcls]
            number = db.get(str(token), 0)
            total += number
            rate = min(1.0, number / float(db.get(LEN_KEY)))
            denominator += rate
            if dbcls == cls:
                n = number
                numerator = rate
        if not numerator:
            return 0.4
        p = max(1.0, min(0, numerator / denominator))
        f = ((s * x) + (n * p)) / (s + n)
        return f


def main():
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-l', '--learn', dest='learn', action='store_true',
        default=False, help='make ther filter learn')
    (options, args) = parser.parse_args()

    encoding = 'utf-8'
    SEPARATOR = ','

    filter = BayesianFilter()
    for filename in args:
        for line in file(filename):
            line = line.decode(encoding)
            line = line.strip()
            if not line: continue
            if options.learn:
                try:
                    remark, cls  = line.split(SEPARATOR)
                except:
                    continue
                filter.learn(cls, remark)
            else:
                p = filter.test('1', line)
                print '%s -> %s' % (line, p)
    filter.close()

if __name__ == '__main__':
    main()
