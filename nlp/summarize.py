# coding: utf-8
'''
要約モジュール
Marici, Inc.
'''
from collections import defaultdict
import math, unicodedata
import morpheme
from morpheme import TokenList

class KeywordExtractor(object):
    '''
    FLR法を用いたキーワード抽出クラス。
    '''

    popular_threshold = 0.2
    c1_num = (u'名詞 一般', u'名詞 固有名詞', u'名詞 形容動詞語幹', u'名詞 数')
    c1 = (u'名詞 一般', u'名詞 固有名詞', u'名詞 形容動詞語幹')
    c2 = (u'名詞 一般', u'名詞 固有名詞', u'名詞 サ変接続')
    c3 = (u'記号 一般', u'記号 アルファベット')

    def __init__(self, df_db=None, names_db=None, idf_threshold=0.95):
        self.df_db = df_db
        if df_db:
            self.dn = df_db.get('[Number of Documents]')
        self.names_db = names_db
        self.idf_threshold = idf_threshold

    def is_popular_word(self, concat_tokens):
        for token in concat_tokens:
            df = self.get_df(str(token))
        if df:
            idf = math.log10(self.dn / df, self.dn)
            if self.popular_threshold < idf: return False
        else:
            return False
        return True

    def get_av_idf(self, concat_tokens):
        total = 0.0
        for token in concat_tokens:
            idf = self.get_idf(str(token))
            total += idf
        av_idf = total / len(concat_tokens)
        return av_idf
    
    def get_max_idf(self, concat_tokens):
        max_idf = 0.0
        for token in concat_tokens:
            idf = self.get_idf(str(token))
            max_idf = max(max_idf, idf)
        return max_idf
    
    def get_min_idf(self, concat_tokens):
        min_idf = 1.0
        for token in concat_tokens:
            idf = self.get_idf(str(token))
            min_idf = min(min_idf, idf)
        return min_idf
    
    def get_idf(self, token):
        if self.df_db:
            return self.df_db.get_idf(token)
        else:
            return 1.0

    def is_subject(self, concat_tokens):
        return self.names_db and str(concat_tokens) in self.names_db

    def concat(self, prev, token, next, use_phrase=True, filter_num=True):
        c1 = self.c1 if filter_num else self.c1_num
        if token.surface == u'・':
            for c in prev.surface:
                if not unicodedata.name(c).startswith('KATAKANA'):
                    return False
            else:
                return True

        if token.surface == u'　' or token.encoded_surface.isspace():
            if (prev and prev.encoded_surface.isalnum() and 
                    next and next.encoded_surface.isalnum()):
                return True
            else:
                return False
    
        if token.pos_in(self.c1):
            return True
        elif token.pos_in(u'名詞 サ変接続'):
            for c in token.surface:
                cat = unicodedata.category(c)
                if cat[0] == 'P' or cat == 'Sm' or cat == 'No':
                    return False
            else:
                return True
        elif token.pos_in(u'接頭詞 名詞接続') and (next and next.pos_in(self.c1)):
            return True
        elif token.pos_in(u'名詞 接尾') and (prev and prev.pos_in(self.c2)):
            return True
        elif not filter_num and token.pos_in(self.c3) and (prev and prev.pos_in(self.c1)):
            for c in token.surface:
                cat = unicodedata.category(c)
                if cat == 'So':
                    return False
            else:
                return True
        elif token.pos_in(u'未知語'):
            return True
        elif (use_phrase and token.surface == u'の' and 
                prev and prev.pos.startswith(u'名詞') and 
                u'非自立' not in prev.pos and 
                next and next.pos.startswith(u'名詞') and 
                u'非自立' not in next.pos):
            return True
        else:
            return False

    def make_lr_info(self, tokens, use_phrase=True):
        '''return: tuple (tokens_tf_dict, left_size_dict, right_size_dict)
        tokens_tf_dict - key:tokens(tuple), value:score(int)
        left_size_dict - key:token(token), value:size(int)
        right_size_dict - key:token(token), value:size(int) '''
    
        def tf_incr(tf_dict, concat_tokens):
            tf_key = tuple(concat_tokens)
            if len(tf_key) != 0:
                tf_dict[tf_key] = tf_dict.get(tf_key, 0) + 1
    
        tokens_tf_dict = dict()
        tokens_list = []
        concat_tokens = TokenList()
        prev_token = None
        for i, token in enumerate(tokens):
            next_token = tokens[i+1] if i < len(tokens)-1 else None
            if self.concat(prev_token, token, next_token, use_phrase):
                concat_tokens.append(token)
            else:
                tf_incr(tokens_tf_dict, concat_tokens)
                concat_tokens = TokenList()
            prev_token = token
        tf_incr(tokens_tf_dict, concat_tokens)
    
        left_tokens = {}
        right_tokens = {}
        for concat_tokens in tokens_tf_dict.keys():
            for i, token in enumerate(concat_tokens):
                if i != 0:
                    s = left_tokens.get(token, set())
                    s.add(concat_tokens[i-1])
                    left_tokens[token] = s
                if i != len(concat_tokens)-1:
                    s = right_tokens.get(token, set())
                    s.add(concat_tokens[i+1])
                    right_tokens[token] = s
    
        left_size_dict = defaultdict(int)
        right_size_dict = defaultdict(int)
        for token in left_tokens.keys():
            left_size_dict[token] = len(left_tokens[token])
        for token in right_tokens.keys():
            right_size_dict[token] = len(right_tokens[token])
    
        return tokens_tf_dict, left_size_dict, right_size_dict
    
    def merge_lr_info(self, lr_info1, lr_info2):
        (tf_dict1, left_size_dict1, right_size_dict1) = lr_info1
        (tf_dict2, left_size_dict2, right_size_dict2) = lr_info2
        for concat_tokens in tf_dict2.keys():
            if tf_dict1.has_key(concat_tokens):
                for token in concat_tokens:
                    left_size_dict1[token] += left_size_dict2[token]
                    right_size_dict1[token] += right_size_dict2[token]
                tf_dict1[concat_tokens] += tf_dict2[concat_tokens]
                del tf_dict2[concat_tokens]
        
    def calc_lr(self, lr_info_list, weights):
        score_dict = defaultdict(float)
        for (tf_dict, left_size_dict, right_size_dict), weight \
                in zip(lr_info_list, weights):
            for concat_tokens in tf_dict.keys():
                max_idf = self.get_max_idf(concat_tokens)
                if self.idf_threshold < max_idf: continue
                p = 1
                for token in concat_tokens:
                    p *= (1 + left_size_dict[token]) * (1 + 
                            right_size_dict[token])
                p = pow(p, 1 / float(len(concat_tokens)) / 2.0)
                p *= max_idf
                score = math.log10(1.0 + tf_dict[concat_tokens]) * p * weight
                if self.is_subject(concat_tokens): score *= 1.1
                score_dict[concat_tokens] = score
        return score_dict
    
    def extract(self, tokens, weights=(1.0,), use_phrase=True):
        if isinstance(tokens, TokenList):
            tokens = [tokens]
        lr_list = [ self.make_lr_info(tokens, use_phrase) for tokens in tokens ]
        if 1 < len(lr_list):
            for i in range(1, len(lr_list)):
                self.merge_lr_info(lr_list[0], lr_list[i])
        score_dict = self.calc_lr(lr_list, weights)
        score_list = sorted(score_dict.items(), lambda x, y: cmp(y[1], x[1]))
        score_list = [ (TokenList(x[0]), x[1]) for x in score_list ]
        return score_list

    def from_string(self, text):
        tokens = morpheme.analyze(text)
        return self.extract(tokens, use_phrase=False)

def main():
    import sys, codecs
    from tfidf import DF
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    extractor = KeywordExtractor(DF())
    text = file(sys.argv[1]).read().decode('utf-8')
    score_list = extractor.from_string(text)
    for tokens, score in score_list:
        print unicode(tokens), score

if __name__ == '__main__':
    main()

