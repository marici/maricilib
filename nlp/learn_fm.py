# -*- coding:utf-8 -*-
import os
import sys
import codecs
import logging
import optparse

import settings
from models import *
from bayesianfilter import BayesianFilter

MENS_CLASS = 'mens'
LADIES_CLASS = 'ladies'

bayesianfilter = BayesianFilter()


def run(options, args):
    setup('mysql://%s:%s@%s:%s/%s?charset=%s' % (
        settings.DATABASE_USER,
        settings.DATABASE_PASSWORD,
        settings.DATABASE_HOST,
        settings.DATABASE_PORT,
        settings.DATABASE_NAME,
        settings.DATABASE_CHARSET))
    attrs = ('original_category', 'name')
    start_id = 2408722
    count = 0
    stoppath = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'stop')
    while True:
        print start_id
        items = Item.query.filter(Item.id > start_id).limit(1000)
        if items.count == 0: break
        for item in items:
            if os.path.exists(stoppath): return
            cls = None
            for attr in attrs:
                cls = get_class(getattr(item, attr, None))
                if cls:
                    break
            if cls:
                logging.info('registered %s:%s(%s) to %s.',
                        item.id, item.name, item.original_category, cls)
                bayesianfilter.learn(cls, item.description)
            count += 1
            start_id = item.id
        if 100000 < count: break


def get_class(text):
    if not text: return None
    if u'レディースライク' in text:
        return MENS_CLASS
    if u'メンズライク' in text:
        return LADIES_CLASS
    ladies_words = (
        u'レディース',
        u'ウィメンズ',
        u'女性用',
        u'婦人用',
    )
    mens_words = (
        u'メンズ',
        u'男性用',
        u'紳士用',
    )
    for cls, words in ((LADIES_CLASS, ladies_words),
            (MENS_CLASS, mens_words)):
        for word in words:
            if word in text:
                return cls
    return None


def main():
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-o', '--output-encoding', dest='outencoding',
        default='utf-8')
    parser.add_option('-v', '--verbose', action='store_true',
        dest='verbose', default=False)
    parser.add_option('--groonga-url', dest='groonga_url',
        default=None)
    parser.add_option('--mysql-user', dest='mysql_user',
        default=settings.DATABASE_USER)
    parser.add_option('--mysql-password', dest='mysql_password',
        default=settings.DATABASE_PASSWORD)
    parser.add_option('--mysql-database', dest='mysql_database',
        default=settings.DATABASE_NAME)
    parser.add_option('--mysql-host', dest='mysql_host',
        default=settings.DATABASE_HOST)
    parser.add_option('--mysql-charset', dest='mysql_charset',
        default=settings.DATABASE_CHARSET)
    options, args = parser.parse_args()

    log_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    log_handler.setFormatter(formatter)
    logging.getLogger().addHandler(log_handler)
    if options.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)

    sys.stdout = codecs.getwriter(options.outencoding)(sys.stdout)

    run(options, args)

if __name__ == '__main__':
    try:
        main()
    finally:
        print 'END'
        bayesianfilter.close()
        
