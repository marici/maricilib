# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os, glob, time, uuid, fcntl, threading
try:
    import simplejson
except ImportError:
    try:
        from django.utils import simplejson
    except ImportError:
        import json as simplejson
from maricilib.taskqueue.backends.base import BaseQueueOperation

class QueueOperation(BaseQueueOperation):

    ext = 'task'

    def __init__(self, basedir=''):
        super(QueueOperation, self).__init__()
        self.basedir = basedir
        self.threadlocal = threading.local()

    def send_task(self, task, queue_name):
        queue_dir = os.path.join(self.basedir, queue_name)
        self._ensure_queue(queue_dir)
        while True:
            try:
                self._acquire(queue_dir)
            except:
                time.sleep(1)
            else:
                break
        try:
            filename = '%s.%s' % (uuid.uuid4(), self.ext)
            f = file(os.path.join(queue_dir, filename), 'w')
            json = simplejson.dumps(task.get_task_info())
            f.write(json.encode('utf-8'))
            f.close()
        finally:
            self._release()

    def _acquire(self, dirpath):
        self.threadlocal.lockfile = file(os.path.join(dirpath, '.lock'), 'w')
        fcntl.flock(self.threadlocal.lockfile.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        
    def _release(self):
        lockfile = self.threadlocal.lockfile
        fcntl.flock(lockfile.fileno(), fcntl.LOCK_UN)
        lockfile.close()
        self.threadlocal.lockfile = None

    def _ensure_queue(self, path):
        if not os.path.isdir(path):
            os.makedirs(path, 0755)

    def receive_tasks(self, queue_name):
        while True:
            if self.end_event.isSet(): break
            json = None
            queue_dir = os.path.join(self.basedir, queue_name)
            self._ensure_queue(queue_dir)
            try:
                self._acquire(queue_dir)
            except:
                time.sleep(1)
                continue
            try:
                filenames = glob.glob(os.path.join(queue_dir, '*.%s' % self.ext))
                if filenames:
                    filenames.sort(key=lambda x: os.path.getmtime(x))
                    filepath = os.path.join(queue_dir, filenames[0])
                    f = file(filepath)
                    json = f.read().decode('utf-8')
                    f.close()
                    os.remove(filepath)
            finally:
                self._release()
            if json:
                yield simplejson.loads(json)
            else:
                if self.join_event.isSet(): break
                time.sleep(3)

