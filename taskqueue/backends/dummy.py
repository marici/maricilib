# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from collections import defaultdict
from Queue import Queue, Empty
from maricilib.taskqueue.backends.base import BaseQueueOperation

class QueueOperation(BaseQueueOperation):

    get_timeout = 5

    def __init__(self, *args, **kwargs):
        super(QueueOperation, self).__init__()
        self.tasks = defaultdict(Queue)
    
    def send_task(self, task, queue_name):
        self.tasks[queue_name].put(task)
        
    def receive_tasks(self, queue_name):
        queue = self.tasks[queue_name]
        while True:
            if self.end_event.isSet(): break
            try:
                task = queue.get(True, self.get_timeout)
                yield task.get_task_info()
                queue.task_done()
            except Empty:
                if self.join_event.isSet(): break
