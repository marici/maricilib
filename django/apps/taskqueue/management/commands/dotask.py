# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from datetime import datetime, timedelta
from optparse import make_option
from maricilib.django.core.management.base import MultiThreadCommand
from maricilib.taskqueue import default_queue_name
from maricilib.django.apps.taskqueue.queue import get_taskqueue
from maricilib.django.apps.taskqueue.models import DoTaskLog, ErrorLog

class Command(MultiThreadCommand):

    help = 'Execute tasks.'
    args = 'queuename'

    task_init_threshold = 30

    def initialize(self, *args, **opts):
        self.debug = opts.get('debug', False)
        self.tasklogs = {}
        self.task_initialized_at = datetime.now()
        self.task_set = set()
        self.queue_name = default_queue_name
        if 0 < len(args):
            self.queue_name = args[0]

    def initialize_task(self, task):
        if self.task_initialized_at + timedelta(
                minutes=self.task_init_threshold) < datetime.now():
            self.task_set = set()
        if not hasattr(task, 'initialize'): return
        name, module, kwargs = task.get_task_info()
        if (name, module) not in self.task_set:
            task.initialize()
            self.task_set.add((name, module))

    def mainloop(self, *args, **opts):
        taskqueue = get_taskqueue()
        for task in taskqueue.receive_tasks(self.queue_name):
            self.initialize_task(task)
            self.enqueue(task)

    def handle_signal(self):
        super(Command, self).handle_signal()
        taskqueue = get_taskqueue()
        taskqueue.close()

    def main(self, *args, **opts):
        self.save_logs(force=True)

    def do(self, task):
        name, module, kwargs = task.get_task_info()
        log = self.tasklogs.get(name)
        if not log:
            log = DoTaskLog()
            log.start = datetime.now()
            log.task_name = name
            log.module_name = module
            log.success_count = 0
            log.error_count = 0
            self.tasklogs[name] = log
        try:
            task.do()
        except Exception, e:
            if self.debug: raise
            log.error_count += 1
            elog = ErrorLog()
            elog.task_name = name
            elog.module_name = module
            elog.args = str(task.kwargs)
            elog.text = str(e)
            elog.reported_at = datetime.now()
            elog.save()
        else:
            log.success_count += 1
        self.save_logs()

    def save_logs(self, intervalmin=60, force=False):
        now = datetime.now()
        threshold = now - timedelta(minutes=intervalmin)
        for name in self.tasklogs:
            log = self.tasklogs.get(name)
            if log.start < threshold:
                log.end = now
                log.total_count = log.success_count + log.error_count
                log.save()
                self.tasklogs[name] = None

