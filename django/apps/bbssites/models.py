# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from django.db import models
from maricilib.django.apps.search import register_index
import managers

class Thread(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u"スレッド"
        
    objects = managers.ThreadManager()

    url = models.URLField(u"URL", max_length=255, unique=True, db_index=True)
    title = models.CharField(u"タイトル", max_length=255)
    bbs_title = models.CharField(u"BBSタイトル", max_length=255)
    responses = models.IntegerField(u"返信数", null=True)
    adult = models.BooleanField(u"アダルト", default=False)
    created_at = models.DateTimeField(u"取得日", auto_now_add=True)
    updated_at = models.DateTimeField(u"更新日", null=True)
    
    def __unicode__(self):
        return self.title

    def __need_add_index__(self):
        return True
    
    def __need_delete_index__(self):
        return False
    
    def __indexdoc__(self):
        d = {"url_s":self.url,
             "title_tj":self.title,
             "bbs_title_tj":self.bbs_title,
             "responses_i":str(self.responses),
             "adult_b":str(self.adult)}
        return d

register_index(Thread)
