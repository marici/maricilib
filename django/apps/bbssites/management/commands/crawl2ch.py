# coding: utf-8
import logging
logging.basicConfig(level=logging.INFO)

from datetime import datetime
from django.core.management.base import BaseCommand
from maricilib.django.apps.bbssites.models import Thread
from maricilib.websites import tsubocrawler

def proc(thread_info):
    try:
        thread = Thread.objects.get(url=thread_info.get('url'))
    except Thread.DoesNotExist:
        thread = Thread.objects.create(
            url=thread_info.get('url'),
            title=thread_info.get('title'), 
            bbs_title=thread_info.get('bbs_info', {}).get('title'),
            responses=thread_info.get('responses'),
            adult=thread_info.get('is_pink'))
        logging.info('Thread was created: %s', thread_info.get('url'))
    else:
        if 50 < abs(int(thread_info.get('responses')) - thread.responses):
            thread.responses = thread_info.get('responses')
            thread.title = thread_info.get('title')
            thread.updated_at = datetime.now()
            thread.save()

class Command(BaseCommand):

    def handle(self, *args, **opts):
        tsubocrawler.crawl(callback=proc)
