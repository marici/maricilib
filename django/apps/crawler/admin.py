# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.contrib import admin
from maricilib.django.apps.crawler import models

class PreferenceAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Preference, PreferenceAdmin)

class SiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'is_active')
admin.site.register(models.Site, SiteAdmin)

class PageAdmin(admin.ModelAdmin):
    list_display = (
        'disp_url', 'title', 'is_article', 'updated_at', 'checked_at')
    list_filter = ('site', 'is_article',)
    search_fields = ['=url']
admin.site.register(models.Page, PageAdmin)

class SearchExecutionAdmin(admin.ModelAdmin):
    list_display = ('query', 'executed_at')
admin.site.register(models.SearchExecution, SearchExecutionAdmin)

class FeedAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'is_active')
admin.site.register(models.Feed, FeedAdmin)

class FeedEntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'url')
admin.site.register(models.FeedEntry, FeedEntryAdmin)

class NewsItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'content_object', 'is_valid', 'created_at')
admin.site.register(models.NewsItem, NewsItemAdmin)
