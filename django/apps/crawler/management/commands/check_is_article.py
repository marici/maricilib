# -*- coding: utf-8 -*-
import logging
import re
from django.core.management.base import BaseCommand
from maricilib.django.apps.crawler.models import Page

logging.basicConfig(filename='check_is_article.log',level=logging.DEBUG)

class Command(BaseCommand):

    def handle(self, *args, **opts):
        pages = Page.objects.all().only("url")
        page_count = pages.count()
        query_count = 10000
        if page_count > 10000:
            i = 0
            while (page_count > i):
                next_count = i + query_count
                if (page_count - i) < query_count:
                    pages = Page.objects.all()[i:]
                else:
                    pages = Page.objects.all()[i:next_count]
                pages = pages.only("url")
                self.check_is_article(pages)
                i = next_count
        else:
            self.check_is_article(pages)

    def check_is_article(self, pages):
        for page in pages:
            site = page.site
            page_url = page.url
            article_pattern = site.article_pattern
            if article_pattern:
                try:
                    match = re.match(article_pattern, page_url)
                    if match:
                        page.is_article = True
                    else:
                        page.is_article = False
                    page.save()
                except Exception, e:
                    logging.error(article_pattern)
                    logging.error(e)
