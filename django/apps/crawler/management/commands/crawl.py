# -*- coding: utf-8 -*-
import os
import time
import socket
import signal
import logging
from threading import Thread
from optparse import make_option
from datetime import datetime
from logging.handlers import RotatingFileHandler
from django.conf import settings
from django.core.management.base import BaseCommand
from maricilib.django.apps.crawler import Crawl
from maricilib.django.apps.crawler.models import Site, Preference, Page
from maricilib.django.apps.crawler.tasks import PageCheckTask
from maricilib.django.apps.crawler.queue import get_taskqueue
from maricilib import load_class
from Queue import Queue

SOCKET_TIMEOUT = 10.0

socket.setdefaulttimeout(SOCKET_TIMEOUT)

def get_class(class_name):
    names = class_name.split('.')
    return load_class(names[-1], '.'.join(names[:-1]))

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--check-old', dest='check_old', action='store_true',
            help='crawl old unfetched pages.'),
        make_option('--one-time', dest='one_time', action='store_true',
            help='crawl old unfetched pages.'),
        make_option('--threads', action='store', type='int', 
            dest='num_main_threads', default=1,
            help='Specifies the number of main threads.'),
        make_option('--task-threads', action='store', type='int', 
            dest='num_task_threads', default=1,
            help='Specifies the number of task threads.'),
        make_option('--tasks', dest='tasks', action='store',
            default='', help='following tasks for a page.'),
        make_option('--debug', dest='debug', action='store_true',
            help='raise exception and exit.'),
    )
    
    crawl_interval = 60 * 10
        
    def initialize_threads(self):
        self.queue = Queue(self.num_main_threads)
        self.main_threads = []
        self.task_threads = []
        for t in range(self.num_main_threads):
            t = Thread(target=self.main_worker)
            t.setDaemon(True)
            t.start()
            self.main_threads.append(t)
        for t in range(self.num_task_threads):
            t = Thread(target=self.task_worker)
            t.setDaemon(True)
            t.start()
            self.task_threads.append(t)

    def main_worker(self):
        while True:
            task = self.queue.get()
            if task.site.pk in self.prev_time_dict:
                prev_time = self.prev_time_dict.get(task.site.pk)
                delta = datetime.now() - prev_time
                sleep_time = self.crawl_interval - delta.seconds
                if 0 < sleep_time:
                    time.sleep(sleep_time)
            try:
                logging.info('crawl start %s', task.site.url)
                task.do()
            except Exception, e:
                if self.debug: raise
                logging.warn(e)
            finally:
                self.prev_time_dict[task.site.pk] = datetime.now()
                self.queue.task_done()
        
    def task_worker(self):
        queue = get_taskqueue()
        for task in queue.receive_tasks(PageCheckTask.queue_name):
            try:
                task.do()
            except Exception, e:
                if self.debug: raise
                logging.warn(e)
                
    def initialize_tasks(self, classes):
        for task_cls in classes:
            if task_cls and hasattr(task_cls, 'initialize'):
                task_cls.initialize()
        
    def handle(self, *args, **opts):
        self.is_closing = False
        signal.signal(signal.SIGINT, self.close)

        self.debug = opts.get('debug')
        if self.debug:
            logging.getLogger().setLevel(logging.DEBUG)
        else:
            max_bytes = 1024 * 1024 * 10
            logdir = os.path.join(settings.BASE_PATH, 'logs')
            if not os.path.exists(logdir):
                os.makedirs(logdir)
            logfile = os.path.join(logdir, 'crawl.log')
            log_handler = RotatingFileHandler(logfile, 'a', max_bytes, 9)
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            log_handler.setFormatter(formatter)
            logging.getLogger().addHandler(log_handler)
            logging.getLogger().setLevel(logging.INFO)
        self.one_time = opts.get('one_time')
        self.prev_time_dict = {}
        
        self.num_main_threads = opts.get('num_main_threads', 1)
        self.num_task_threads = opts.get('num_task_threads', 1)
        self.initialize_threads()
        
        task_class_names = opts.get('tasks', '').split(',')
        Crawl.imediate_tasks = []
        for class_name in task_class_names:
            if class_name:
                Crawl.imediate_tasks.append(get_class(class_name))
        
        pref = Preference.objects.get(unique=True)
        Crawl.new_page_task = (get_class(pref.new_page_task) if
            pref.new_page_task else None)
        Crawl.update_page_task = (get_class(pref.update_page_task) if
            pref.update_page_task else None)
        
        all_task_classes = list(Crawl.imediate_tasks)
        all_task_classes.append(Crawl.new_page_task)
        all_task_classes.append(Crawl.update_page_task)
        
        # クロール漏れを収集
        if opts.get('check_old'):
            self.initialize_tasks(all_task_classes)
            for page in Page.objects.filter(checked_at=None):
                task = PageCheckTask(dict(site=page.site, page=page, depth=10,
                        site_url=page.site.url, url=page.url))
                get_taskqueue().send_task(task, queue_name=task.queue_name)
        
        while True:
            self.initialize_tasks(all_task_classes)
            for site in Site.objects.all():
                if self.is_closing: break
                if not site.is_active: continue
                task = PageCheckTask(dict(url=site.url, site_url=site.url,
                    site=site, depth=0))
                self.queue.put(task)
            self.queue.join()
            if self.one_time:
                get_taskqueue().join()
                for t in self.task_threads:
                    t.join()
                break

    def close(self, num, frame):
        logging.info('crawler is closing...')
        get_taskqueue().close()
        self.is_closing = True
