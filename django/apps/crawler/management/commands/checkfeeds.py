# -*- coding: utf-8 -*-
import logging
from optparse import make_option
from threading import Thread
from Queue import Queue
from django.core.management.base import BaseCommand
from maricilib import load_class
from crawler.models import Feed, Preference
from crawler.tasks import FeedCheckTask

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--threads', action='store', type='int', 
            dest='num_threads', default=1,
            help='Specifies the number of threads.'),
        make_option('--tasks', dest='tasks', action='store',
            default='', help='following tasks for a page.'),
        make_option('--debug', dest='debug', action='store_true',
            help='raise exception and exit.'),
    )

    def worker(self):
        while True:
            task = self.queue.get()
            try:
                task.do()
            except Exception, e:
                if self.debug: raise
                logging.warn(e)
            finally:
                self.queue.task_done()

    def initialize_threads(self):
        self.queue = Queue(self.num_threads)
        for t in range(self.num_threads):
            t = Thread(target=self.worker)
            t.setDaemon(True)
            t.start()

    def handle(self, *args, **opts):
        self.debug = opts.get('debug', False)
        self.num_threads = opts.get('num_threads', 1)
        self.initialize_threads()

        task_class_names = opts.get('tasks', '').split(',')
        task_classes = []
        for class_name in task_class_names:
            if class_name:
                task_cls = load_class(class_name)
                if hasattr(task_cls, 'initialize'): task_cls.initialize()
                task_classes.append(task_cls)
        
        pref = Preference.objects.get(unique=True)
        entry_task = load_class(pref.entry_task) if pref.entry_task else None
        if hasattr(entry_task, 'initialize'): entry_task.initialize()

        for feed in Feed.objects.all():
            if not feed.is_active: continue
            logging.info('checking feed: %s' % feed.url)
            task = FeedCheckTask(dict(feed=feed, imediate_tasks=task_classes,
                entry_task=entry_task))
            self.queue.put(task)
        self.queue.join()
