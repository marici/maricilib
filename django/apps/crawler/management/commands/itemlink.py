# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
import sys
from optparse import make_option
from django.core.management.base import BaseCommand
from maricilib import load_class
from crawler.tasks import ItemLinkTask

class Command(BaseCommand):

    option_list = BaseCommand.option_list + ( 
        make_option('--key-name', dest='key_name', action='store',
            help='model key name.'),
        make_option('--task', dest='task', action='store',
            help='filtering task.'),
        make_option('--debug', dest='debug', action='store_true',
            help='raise exception and exit.'),
    )   

    def handle(self, *args, **opts):
        self.debug = opts.get('debug', False)
        model_name = args[0]
        if not model_name: return
        model_class = load_class(model_name)
        key_name = opts.get('key_name', 'name')
        task_name = opts.get('task', None)
        task_class = load_class(task_name) if task_name else None

        for line in sys.stdin:
            names = line.decode('utf-8').strip()
            if not names: continue
            names = names.split(',')
            task = ItemLinkTask(dict(names=names, key_name=key_name,
                model_class=model_class, task_class=task_class))
            task.do()
