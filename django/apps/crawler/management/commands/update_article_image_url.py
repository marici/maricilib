# -*- coding: utf-8 -*-
import logging
import urllib2
import urlparse
from BeautifulSoup import BeautifulSoup
from django.core.management.base import BaseCommand
from maricilib.django.apps.crawler.models import Page

logging.basicConfig(
    filename='update_article_image_url.log', level=logging.INFO)
logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **opts):
        start_id = 0
        while True:
            pages = Page.objects\
                .filter(is_article=True, pk__gt=start_id)\
                .only('pk', 'url', 'article_image_url').order_by('pk')
            if pages.count() == 0:
                break
            for page in pages[:100]:
                self.update_article_image_url(page)
                start_id = page.pk
        logger.info('Finish updating article_image_url')

    def update_article_image_url(self, page):
        if page.article_image_url:
            return
        image_link = self.find_facebook_image(page)
        if not image_link:
            image_link = self.find_image_in_article(page)
        page.article_image_url = urlparse.urljoin(page.url, image_link)
        page.save()

    def find_facebook_image(self, page):
        soup = None
        try:
            content = page.content
            soup = BeautifulSoup(content)
            image_meta = soup.find("meta", property="og:image")
            if image_meta:
                return image_meta.get("content")
        except Exception, e:
            logging.error(e)
        finally:
            if soup:
                soup.decompose()
        return None

    def find_image_in_article(self, page):
        urllib_page = None
        soup = None
        try:
            if page.body:
                content = page.body
            else:
                urllib_page = urllib2.urlopen(page.url)
                content = urllib_page.read()
                soup = BeautifulSoup(content)
                img = soup.find("img")
                if img:
                    src = img.get("src")
                    return src
        except Exception, e:
            logging.error(e)
        finally:
            if urllib_page:
                urllib_page.close()
            if soup:
                soup.decompose()
        return None
