# -*- coding:utf-8 -*-
import csv
from django.core.management.base import BaseCommand
from maricilib.django.apps.crawler.models import Site


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_files', nargs='+', type=str)

    def handle(self, *args, **options):

        csv_file = options['csv_files'][0]

        csv_fields = [u'媒体名', u'URL', u'記事URL', u'追跡URL', u'無視URL',
                      u'本文開始', u'本文終了', u'JavaScriptリンク',
                      u'記事内リンク',  u'備考']

        with open(csv_file) as sites:

            sites = csv.DictReader(sites, csv_fields)

            for i, row in enumerate(sites):
                if i == 0:
                    continue
                try:
                    site = Site.objects.get(url=row[u'URL'])
                    site.name = row[u'媒体名']
                    site.article_pattern = row[u'記事URL']
                    site.follow_pattern = row[u'追跡URL']
                    site.nofollow_pattern = row[u'無視URL']
                    site.body_start = row[u'本文開始']
                    site.body_end = row[u'本文終了']
                    site.body_link_pattern = row[u'記事内リンク']
                    site.save()
                except Site.DoesNotExist:
                    site = Site(
                        name=row[u'媒体名'],
                        url=row[u'URL'],
                        article_pattern=row[u'記事URL'],
                        follow_pattern=row[u'追跡URL'],
                        nofollow_pattern=row[u'無視URL'],
                        body_start=row[u'本文開始'],
                        body_end=row[u'本文終了'],
                        #row[u'JavaScriptリンク'],
                        body_link_pattern=row[u'記事内リンク'],
                        #row[u'備考']
                        )
                    site.save()
