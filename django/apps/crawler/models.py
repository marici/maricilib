# -*- coding:utf-8 -*-
import json
import time
import re
import difflib
import urlparse
from datetime import datetime, timedelta
from BeautifulSoup import BeautifulSoup
from django.conf import settings
from django.db.models.base import Model
from django.db.models.fields import DateTimeField, CharField, TextField,\
    IntegerField, URLField, BooleanField, PositiveIntegerField,\
    CommaSeparatedIntegerField
from django.db.models.fields.related import ForeignKey, ManyToManyField
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from maricilib.search import bosssearch
from maricilib.django.apps.search import register_index

def unix_time(value):
    if not value or not isinstance(value, datetime): return None
    return time.mktime(value.timetuple())
    
class Preference(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'全体設定'
    unique = BooleanField(default=True, unique=True, editable=False)
    new_page_task = CharField(u'ページ追加時実行タスク', max_length=255, 
        null=True, blank=True)
    update_page_task = CharField(u'ページ更新時実行タスク', max_length=255, 
        null=True, blank=True)
    entry_task = CharField(u'フィードエントリ追加時実行タスク', max_length=255, 
        null=True, blank=True)
    
    def __unicode__(self):
        return u'全体設定'
    
    
class SearchExecution(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'検索ワード'
    query = CharField(u'検索クエリ', max_length=255)
    executed_at = DateTimeField(u'実行日時', null=True, blank=True, editable=False)
    content = TextField(u'コンテンツ', null=True, editable=False)

    def __unicode__(self):
        return self.query

    @property
    def results(self):
        return json.loads(self.content) if self.content else []
    
    def needs_refresh(self):
        if not self.executed_at: return True
        return self.executed_at < datetime.now() - timedelta(days=1)

    def do(self):
        results = bosssearch.search(self.query)
        for r in results:
            url = r.get('url')
            title = r.get('title')
            try:
                Site.objects.get(url=url)
            except Site.DoesNotExist:
                Site.objects.create(url=url, name=title)
        self.executed_at = datetime.now()
        self.content = json.dumps(results)


class Site(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'クロール対象サイト'
    name = CharField(u'サイト名', max_length=255)
    url = URLField(u'起点URL', db_index=True, unique=True, max_length=255)
    depth = IntegerField(u'リンク追跡の深さ', default=2)
    update_depth = IntegerField(u'更新チェックの深さ', default=1)
    ignore_params = BooleanField(u'URLパラメータを無視する', default=False)
    follow_pattern = TextField(u'リンク追跡パターン', null=True, blank=True)
    nofollow_pattern = TextField(u'リンク無視パターン', null=True, blank=True)
    body_link_pattern = TextField(u'本文リンクパターン', null=True, blank=True)
    article_pattern = TextField(u'記事URLパターン', null=True, blank=True)
    article_keywords = TextField(u'記事キーワード', null=True, blank=True)
    nofollow_from_article = BooleanField(
        u'記事からのリンク無視', default=True)
    body_start = TextField(u'本文開始行の正規表現', null=True, blank=True)
    body_end = TextField(u'本文終了行の正規表現', null=True, blank=True)
    new_page_task = CharField(u'ページ追加時実行タスク', max_length=255,
        null=True, blank=True)
    update_page_task = CharField(u'ページ更新時実行タスク', max_length=255,
        null=True, blank=True)
    is_active = BooleanField(u'取得対象', default=True)
    
    def __unicode__(self):
        return self.name
    
    @property
    def data(self):
        return {
            'name':self.name,
            'url':self.url,
            'depth':self.depth,
            'update_depth':self.update_depth,
            'ignore_params':self.ignore_params,
            'follow_pattern':self.follow_pattern,
            'nofollow_pattern':self.nofollow_pattern,
            'body_start':self.body_start,
            'body_end':self.body_end
        }
        
    def follow(self, url):
        if urlparse.urlparse(url).netloc != urlparse.urlparse(self.url).netloc:
            return False
        if self.nofollow_pattern:
            if re.search(self.nofollow_pattern, url):
                return False
        if self.follow_pattern:
            if re.search(self.follow_pattern, url) is None:
                return False
        return True

    def check_next_page(self, url):
        if self.body_link_pattern:
            if re.search(self.body_link_pattern, url):
                return True
        return False
        
    def extract_body(self, url, html):
        if self.body_start and self.body_end:
            lines = []
            in_body = False
            for line in html.split(r'[\r\n]'):
                if re.search(self.body_start, line):
                    in_body = True
                elif re.search(self.body_end, line):
                    in_body = False
                if in_body:
                    lines.append(line)
            return '\n'.join(lines)
        else:
            from maricilib.thirdparty import extbody
            pageInfo = extbody.PageInfo.PageInfo(url, html)
            body, newsFlag = extbody.extractor.ext(pageInfo)
            return body

    def check_article(self, url, content):
        ret = False
        if self.article_pattern:
            match = re.match(self.article_pattern, url)
            ret = bool(match)
            if ret is False:
                return ret
        if self.article_keywords:
            for keyword in self.article_keywords.split('\n'):
                keyword = keyword.strip()
                if not keyword:
                    continue
                if keyword in content:
                    ret = True
                    break
            else:
                ret = False
        return ret


class Page(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'クロール済ページ'

    site = ForeignKey(Site, null=True, editable=False)
    url = URLField(u'URL', db_index=True, unique=True, max_length=255)
    title = CharField(u'タイトル', max_length=255, null=True)
    content = TextField(u'コンテンツ', null=True)
    tagged_body = TextField(u'タグ付き本文', null=True)
    body = TextField(u'本文', null=True)
    description = TextField(u'デスクリプション', null=True)
    updated_content = TextField(u'最後の更新内容', null=True)
    frame_urls = TextField(u'フレームURLのリスト', null=True, editable=False)
    has_feed = BooleanField(u'フィード保持', default=False, editable=False)
    crawled_at = DateTimeField(u'取得日時', null=True, editable=False,
        db_index=True)
    updated_at = DateTimeField(u'更新日時', null=True, editable=False,
        db_index=True)
    checked_at = DateTimeField(u'チェック日時', null=True, editable=False,
        db_index=True)
    intervals = CommaSeparatedIntegerField(u'更新間隔履歴（分）', max_length=255,
        default='', editable=False)
    is_article = BooleanField(u'記事フラグ', default=False, db_index=True)
    article_image_url = CharField(
        u"記事画像URL", max_length=255, null=True, blank=True, )
    meta = TextField(u'メタデータ', null=True)
    display_priority = IntegerField(u'表示優先度', default=0, db_index=True)
    # Site と紐づけない場合のため
    site_name = CharField(u'サイト名', max_length=255, null=True, editable=False)
    site_url = URLField(u'サイトURL', max_length=255, null=True, editable=False)

    @property
    def data(self):
        intervals = [ int(i) for i in self.intervals.split(',') if i ]
        return {
            'pk':self.pk,
            'site_pk':self.site.pk,
            'site_url':self.site.url,
            'url':self.url,
            'title':self.title,
            'content':self.content,
            'tagged_body':self.tagged_body,
            'body':self.updated_content,
            'frame_urls':self.frame_urls,
            'has_feed':self.has_feed,
            'crawled_at':unix_time(self.crawled_at),
            'updated_at':unix_time(self.updated_at),
            'checked_at':unix_time(self.checked_at),
            'intervals':intervals,
            'meta':self.meta,
            'content_text':self.content_text
        }
        
    def disp_url(self):
        return self.url[:40]

    def __unicode__(self):
        return self.disp_url()
    
    def add_interval(self, interval):
        intervals = [ int(i) for i in self.intervals.split(',') if i ]
        if 10 <= len(intervals): intervals = intervals[1:]
        intervals.append(interval)
        self.intervals = ','.join([ str(i) for i in intervals ])
        
    @property
    def average_interval(self):
        intervals = [ int(i) for i in self.intervals.split(',') if i ]
        if not intervals: return None
        return sum(intervals) / len(intervals)

    def needs_refresh(self):
        if not self.checked_at: return False
        if not self.average_interval: return True
        return (self.checked_at < 
            datetime.now() - timedelta(
            seconds=min(60 * 60, self.average_interval)))

    def get_updated_content(self, new_content):
        def unified_diff_ctrl(buf):
            marks = ('--- ', '+++ ', '@@ ')
            for m in marks:
                if buf.startswith(m): return True
            return False
        if self.content == new_content: return None
        if self.content:
            difflist = []
            update_lines = []
            for buf in difflib.unified_diff(self.content.splitlines(),
                    new_content.splitlines()):
                if unified_diff_ctrl(buf):
                    if update_lines:
                        difflist.append('\n'.join(update_lines))
                        update_lines = []
                elif not buf.startswith('-'):
                    update_lines.append(buf[1:])
            updated_content = '\n\n'.join(difflist)
            self.updated_content = updated_content
            return updated_content
        else:
            return new_content
        
    a_pat = re.compile(r'<a[^>]*>.*?</a>', re.I | re.S)
    @property
    def content_text(self):
        if not self.tagged_body: return None
        text = self.a_pat.sub('', self.tagged_body)
        if self.title: text = self.title + '\n' + text
        return text

    def get_description(self, soup=None):
        if self.description:
            return self.description
        soup_init = False
        if soup is None:
            soup_init = True
            soup = BeautifulSoup(self.content)
        tag = soup.find('meta', property='og:description')
        description = tag.get('content') if tag else ''
        if soup_init:
            soup.decompose()
        return description

    def get_article_image_url(self, soup=None):
        if self.article_image_url:
            return self.article_image_url
        soup_init = False
        if soup is None:
            soup_init = True
            soup = BeautifulSoup(self.content)
        image_meta = soup.find('meta', property='og:image')
        image_link = image_meta.get('content') if image_meta else None
        if not image_link and self.tagged_body:
            body_soup = BeautifulSoup(self.tagged_body)
            img = body_soup.find('img')
            image_link = img.get('src') if img else None
            body_soup.decompose()
        if soup_init:
            soup.decompose()
        if image_link:
            return urlparse.urljoin(self.url, image_link)
        else:
            return None

    def __need_add_index__(self):
        if getattr(settings, 'CRAWLER_INDEX_ARTICLES', False):
            return self.is_article and self.updated_at
        else:
            return self.updated_at
    
    def __need_delete_index__(self):
        return False
    
    def __indexdoc__(self):
        updated_at = 0
        if self.updated_at:
            updated_at = unix_time(self.updated_at) * 1000
        if getattr(settings, 'CRAWLER_INDEX_DESCRIPTION', False):
            body = self.description or ''
        else:
            body = self.body
        d = {'url':self.url,
             'title':self.title,
             'body':body,
             'updated_at': updated_at}
        return d


class PageUpdate(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'ページの更新'

    page = ForeignKey(Page, verbose_name=u'ページ')
    content = TextField(u'コンテンツ')
    updated_at = DateTimeField(u'更新日時')

    def __unicode__(self):
        return self.page.title


class Feed(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'クロール対象フィード'

    title = CharField(u'タイトル', max_length=255, null=True)
    url = URLField(u'フィードURL', unique=True, max_length=255, db_index=True)
    link = URLField(u'サイトURL', null=True, editable=False)
    subtitle = CharField(u'サブタイトル', max_length=255, null=True, editable=False)
    description = TextField(u'説明', null=True, editable=False)
    language = CharField(u'言語', max_length=255, null=True, editable=False)
    last_fetched_at = DateTimeField(u'最終取得日時', null=True, editable=False)
    interval = IntegerField(u'平均更新間隔', default=30, editable=False)
    is_active = BooleanField(u'取得対象', default=True)
    pages = ManyToManyField(Page, verbose_name=u'検出ページ', editable=False)

    @property
    def data(self):
        return {
            'pk':self.pk,
            'title':self.title,
            'url':self.url,
            'link':self.link,
            'subtitle':self.subtitle,
            'description':self.description,
            'language':self.language,
            'last_fetched_at':unix_time(self.last_fetched_at),
            'interval':self.interval,
            'is_active':self.is_active
        }
        
    def needs_refresh(self):
        if self.last_fetched_at is None: return True
        if self.interval == 0: return True
        return (self.last_fetched_at + timedelta(minutes=self.interval) < 
            datetime.now())

    def __unicode__(self):
        return self.url


class FeedEntry(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'クロール済フィードエントリ'

    feed = ForeignKey(Feed, verbose_name=u'フィード', editable=False)
    url = URLField(u'URL', max_length=255, unique=True, db_index=True)
    title = CharField(u'タイトル', max_length=255)
    author = CharField(u'作成者', max_length=255, null=True)
    summary = TextField(u'概要', null=True)
    content = TextField(u'内容', null=True)
    updated_at = DateTimeField(u'更新日時', null=True)

    def __unicode__(self):
        return self.title

    @property
    def data(self):
        return {
            'pk':self.pk,
            'feed_id':self.feed.pk,
            'feed_url':self.feed.url,
            'url':self.url,
            'title':self.title,
            'author':self.author,
            'summary':self.summary,
            'content':self.content,
            'updated_at':unix_time(self.updated_at),
            'content_text':self.content_text
        }
        
    def __need_add_index__(self):
        return True
    
    def __need_delete_index__(self):
        return False
    
    def __indexdoc__(self):
        d = {'url':self.url,
             'title':self.title,
             'content':self.content,
             'updated_at':unix_time(self.updated_at) * 1000}
        return d

    @property
    def content_text(self):
        return self.title + '\n' + self.content


class NewsItem(Model):
    class Meta:
        verbose_name = verbose_name_plural = u'関連アイテム'

    is_valid = BooleanField(u'検証済', default=False, db_index=True)
    url = URLField(u'URL', max_length=255, db_index=True)
    title = CharField(u'タイトル', max_length=255)
    body = TextField(u'本文', null=True, blank=True)
    content_name = CharField(u'ひもづけ先', max_length=255, db_index=True, 
        null=True)
    created_at = DateTimeField(u'作成日時', auto_now_add=True)
    page = ForeignKey(Page, null=True, editable=False)
    entry = ForeignKey(FeedEntry, null=True, editable=False)
    content_type = ForeignKey(ContentType, editable=False)
    object_id = PositiveIntegerField(editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __init__(self, *args, **kwargs):
        super(NewsItem, self).__init__(*args, **kwargs)
        if 'content_object' in kwargs:
            self.content_name = getattr(kwargs['content_object'], 'name', None)

    def __unicode__(self):
        return self.title

    def get_news(self):
        return self.page if self.page else self.entry

    def set_news(self, news):
        if isinstance(news, Page):
            self.body = news.body
            self.page = news
        elif isinstance(news, FeedEntry):
            self.body = news.content
            self.entry = news
        else:
            raise ValueError('\'news\' must be a FeedEntry or a Page instance.')
        self.url = news.url
        self.title = news.title

    news = property(get_news, set_news)


register_index(Page)
register_index(FeedEntry)
