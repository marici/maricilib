# coding: utf-8
import logging
import urllib
import urlparse
import re
from datetime import datetime
from BeautifulSoup import BeautifulSoup
from django.conf import settings
from maricilib import load_class
from maricilib.websites.util import htmlentity2unicode
from maricilib.feed import finder
from maricilib.taskqueue.tasks import Task
from maricilib.django.apps.crawler import Crawl
from queue import get_taskqueue
from models import Page, PageUpdate, Feed, FeedEntry, Site, NewsItem
import chardet
import feedparser

DEFAULT_USER_AGENT = 'Marici Bot/1.0 +http://www.marici.co.jp'

class URLOpener(urllib.FancyURLopener):
    
    def __init__(self, *args):
        self.version = getattr(settings, 'USER_AGENT', DEFAULT_USER_AGENT)
        urllib.FancyURLopener.__init__(self, *args)

urllib._urlopener = URLOpener()
        
empty_line_pat = re.compile(r'^\s+$', re.M)
crlf_pat = re.compile(r'[\r\n]+')
tag_pat = re.compile(r'<[^>]*>')
title_pat = re.compile(r'<title>(.*)</title>', re.I | re.S)
br_pat = re.compile(r'<(/p|br */?)>', re.I)
a_pat = re.compile(r'<a [^>]*href="([^"]*)"[^>]*>', re.I)
js_css_pat = re.compile(r'(?is)<(script|style).*?>.*?(</\1>)', re.I)

class PageCheckTask(Task):
    queue_name = 'crawler.pagecheck'
    pass_update = False

    def get_task_info(self):
        data = {
            'site_url': self.site_url,
            'url': self.url,
            'depth': self.depth,
        }
        return (self.__class__.__name__,
            self.__class__.__module__,
            data)

    def decode_error(self):
        self.page.save()
        logging.warn('HTML decode error: %s', self.page.url)

    def do(self):
        if not hasattr(self, 'site'):
            self.site = Site.objects.get(url=self.site_url)
        site = self.site

        if not hasattr(self, 'page'):
            try:
                self.page = Page.objects.get(url=self.url)
            except:
                self.page = Page.objects.create(url=self.url, site=site)
        page = self.page
        self.url = page.url

        logging.info('fetching %s (depth: %s)', self.url, self.depth)
        page.checked_at = datetime.now()
        p = urllib.urlopen(page.url)
        raw_html = p.read()
        p.close()
        encoding = chardet.detect(raw_html)['encoding']
        if encoding is None:
            self.decode_error()
            return
        try:
            html = raw_html.decode(encoding, 'ignore')
        except:
            self.decode_error()
            return
        html = html.replace(r'[\r\n]+', '\n')

        added = False
        updated = False
        if page.content:
            updated_content = page.get_updated_content(html)
            interval = (page.updated_at - page.checked_at).seconds
            if updated_content:
                PageUpdate.objects.create(page=page, content=updated_content,
                    updated_at=page.checked_at)
                page.intervals = str(interval / 12)
                page.updated_content = updated_content
                self.update_page_info(html, raw_html)
                updated = True
            else:
                page.add_interval(interval)
        else:
            self.update_page_info(html, raw_html)
            page.crawled_at = page.checked_at
            added = True
            
        page.is_article = site.check_article(page.url, page.content)
        page.save()

        if ((site.depth == 0 or self.depth < site.depth) and
                not (page.is_article and site.nofollow_from_article)):
            follow_candidates = set()
            for href in a_pat.findall(page.content):
                if not href: continue
                href = href.replace('&amp;', '&')
                href = href.split('#')[0]
                if site.ignore_params:
                    href = href.split('?')[0]
                url = urlparse.urljoin(self.url, href)
                if url == self.url: continue
                if site.check_next_page(url):
                    self.append_page_info(url)
                elif site.follow(url):
                    follow_candidates.add(url)

            follow_pages = []
            for url in follow_candidates:
                try:
                    linked_page = Page.objects.get(url=url)
                    if self.pass_update:
                        continue
                except Page.DoesNotExist:
                    linked_page = Page.objects.create(url=url, site=site)
                else:
                    if site.update_depth < self.depth + 1: continue
                    if not linked_page.needs_refresh(): continue
                    linked_page.checked_at = None
                    linked_page.save()
                follow_pages.append(linked_page)

            for p in follow_pages:
                task = self.__class__(dict(site=site, page=p,
                    url=p.url, site_url=site.url, 
                    depth=self.depth+1))
                get_taskqueue().send_task(task, queue_name=task.queue_name)
                logging.info('following link from %s to %s.', self.url, p.url)

        if not(added or updated): return
        
        for task_cls in Crawl.imediate_tasks:
            task = task_cls({task_cls.target_name:page})
            task.do()

        if added and Crawl.new_page_task:
            task = Crawl.new_page_task({'news':page})
            get_taskqueue().send_task(task, queue_name=task.queue_name)
        if updated and Crawl.update_page_task:
            task = Crawl.update_page_task({'news':page})
            get_taskqueue().send_task(task, queue_name=task.queue_name)

        site_new_task_cls = (load_class(site.new_page_task) if
            site.new_page_task else None)
        if added and site_new_task_cls:
            task = site_new_task_cls({'news':page})
            get_taskqueue().send_task(task, queue_name=task.queue_name)

        site_update_task_cls = (load_class(site.update_page_task) if
            site.update_page_task else None)
        if updated and site_update_task_cls:
            task = site_update_task_cls({'news':page})
            get_taskqueue().send_task(task, queue_name=task.queue_name)
        
    def update_page_info(self, html, raw_html):
        tagged_body = self.site.extract_body(self.url, raw_html)
        # body = htmlentity2unicode(tagged_body)
        # この行でコケるため修正 
        body = tagged_body
        body = br_pat.sub('\n', body)
        body = tag_pat.sub('', body)
        body = empty_line_pat.sub('', body)
        body = crlf_pat.sub('\n', body)
        self.page.content = html
        self.page.body = body.strip()
        self.page.tagged_body = tagged_body
        self.page.updated_at = self.page.checked_at
        m = title_pat.search(html)
        if m:
            title = m.group(1).strip()
            self.page.title = htmlentity2unicode(crlf_pat.sub('', title))

        try:
            soup = BeautifulSoup(self.page.content)
            self.page.description = self.page.get_description(soup)
            self.page.article_image_url = self.page.get_article_image_url(soup)
        except Exception, e:
            logging.warn(e)
        finally:
            if soup:
                soup.decompose()

        feeds = finder.find_from_html(html)
        self.page.has_feed = bool(feeds)
        for feed in feeds:
            feed_url = feed.get('href')
            logging.debug('Feed was found: %s', feed_url)
            try:
                feed = Feed.objects.get(url=feed_url)
            except Feed.DoesNotExist:
                feed = Feed.objects.create(url=feed_url, is_active=False)
                task = FeedCheckTask(dict(url=feed_url))
                get_taskqueue().send_task(task, queue_name=task.queue_name)
            feed.pages.add(self.page)


    def append_page_info(self, url):
        p = urllib.urlopen(url)
        raw_html = p.read()
        p.close()
        encoding = chardet.detect(raw_html)['encoding']
        if encoding is None:
            self.decode_error()
            return
        try:
            html = raw_html.decode(encoding, 'ignore')
        except:
            self.decode_error()
            return
        html = html.replace(r'[\r\n]+', '\n')
        tagged_body, body = self.extract_body(self.site, url, raw_html)
        self.page.content += html
        self.page.body += body.strip()
        self.page.tagged_body += tagged_body
        self.page.save()

    @classmethod
    def extract_body(cls, site, url, raw_html):
        tagged_body = site.extract_body(url, raw_html)
        # body = htmlentity2unicode(tagged_body)
        # この行でコケるため修正
        body = tagged_body
        body = br_pat.sub('\n', body)
        body = js_css_pat.sub('', body)
        body = tag_pat.sub('', body)
        body = empty_line_pat.sub('', body)
        body = crlf_pat.sub('\n', body)
        return tagged_body, body


class FeedCheckTask(Task):
    queue_name = 'crawler.feedcheck'

    def do(self):
        if hasattr(self, 'feed'):
            feed = self.feed
            self.url = feed.url
        else:
            feed = Feed.objects.get(url=self.url)
            
        now = datetime.now()
        self.entries = []
        feed.last_fetched_at = now
        
        try:
            parsed = feedparser.parse(self.url, 
                agent=getattr(settings, 'USER_AGENT', DEFAULT_USER_AGENT))
        except Exception, e:
            logging.warn('Feed parse error: %s %s', self.url, e)
            feed.save()
            return
        if parsed.feed.get('link') is None:
            logging.warn('Invalid Feed: %s', feed)
            feed.save()
            return
        feed.link = parsed.feed.get('link')
        feed.title = parsed.feed.get('title')
        feed.substitle = parsed.feed.get('subtitle')
        feed.description = parsed.feed.get('description')
        feed.language = parsed.feed.get('language')
        
        try:
            site = Site.objects.get(url=feed.link)
        except:
            site = Site.objects.create(url=feed.link, name=feed.title,
                is_active=False)

        for entry in parsed.get('entries', []):
            summary=entry.get('summary')
            content = entry.get('content', [{}])[0].get('value')
            updated_parsed = entry.get('updated_parsed')
            if updated_parsed:
                updated_at = datetime(*updated_parsed[:6])
            else:
                updated_at = now
            
            try:
                FeedEntry.objects.get(url=entry.get('link'))
            except FeedEntry.DoesNotExist:
                if entry.get('link') is None:
                    logging.warn('Invalid Entry: %s', entry)
                    continue
                try:
                    fe = FeedEntry.objects.create(
                        feed=feed,
                        title=entry.get('title'),
                        author=entry.get('author'),
                        summary=entry.get('summary'),
                        url=entry.get('link'),
                        content=content, updated_at=updated_at
                    )
                except Exception, e:
                    logging.warn('FeedEntry \'%s\' could not be created: %s', self.url, e)
                else:
                    entry_task_cls = getattr(self, 'entry_task', None)
                    if entry_task_cls:
                        task = entry_task_cls(dict(feed=feed.data, entry=fe.data))
                        get_taskqueue().send_task(task, queue_name=task.queue_name)
                    self.entries.append(fe)
                    
            is_article = site.check_article(entry.get('link'))
            try:
                Page.objects.get(url=entry.get('link'))
            except Page.DoesNotExist:
                raw_html = content or summary
                body = htmlentity2unicode(raw_html)
                body = br_pat.sub('\n', body)
                body = tag_pat.sub('', body)
                body = empty_line_pat.sub('', body)
                body = crlf_pat.sub('\n', body)
                body = body.strip()
                page = Page.objects.create(site=site, url=entry.get('link'),
                    title=entry.get('title'), content=raw_html,
                    tagged_body=raw_html, body=body,
                    crawled_at=now, updated_at=now, checked_at=now,
                    is_article=is_article)
                if page.body and Crawl.imediate_tasks:
                    for task_cls in Crawl.imediate_tasks:
                        task = task_cls({task_cls.target_name:page})
                        task.do()
            
        feed.save()


class AsyncNewsTask(Task):
    
    def get_task_info(self):
        kwargs = {}
        kwargs.update(self.kwargs)
        if 'news' in self.kwargs and hasattr(self.news, 'data'):
            kwargs['news'] = self.news.data
        if 'site' in self.kwargs and hasattr(self.news, 'data'):
            kwargs['site'] = self.site.data
        if 'page' in self.kwargs and hasattr(self.news, 'data'):
            kwargs['page'] = self.page.data
        if 'feed' in self.kwargs and hasattr(self.news, 'data'):
            kwargs['feed'] = self.feed.data
        if 'entry' in self.kwargs and hasattr(self.news, 'data'):
            kwargs['entry'] = self.entry.data
        return (self.__class__.__name__,
            self.__class__.__module__,
            kwargs)
        
        
class FilterTask(AsyncNewsTask):

    def do(self):
        content_text = getattr(self.news, 'content_text', 
            self.news.get('content_text'))
        for matched_object in self.matcher.search(content_text):
            self.action(matched_object)

    def action(self, matched_object):
        raise NotImplementedError()


from maricilib.django.apps.search import searcher

class ItemLinkTask(Task):
    """
    model_class - names属性を持つひもづけ対象クラス
    model_name - names属性を持つひもづけ対象クラス名
    key_name - ひもづけオブジェクトのキーフィールド名
    target_name - 即実行するタスククラス名
    task_class - 即実行するタスククラス
                 target_name: ページ
                 model_instance: ひもづけ対象
    task_name - 即実行するタスククラス名
    """
    
    queue_name = 'crawler.itemlink'
    
    def do(self):
        if hasattr(self, 'task_class'):
            task_cls = self.task_class
        else:
            task_cls = (load_class(self.task_name) 
                if getattr(self, 'task_name', None) else None)
            
        if hasattr(self, 'model_class'):
            model_cls = self.model_class
        else:
            model_cls = load_class(self.model_name)
        
        params = {getattr(self, 'key_name', 'name'): self.names[0]}
        obj = model_cls.objects.get(**params)
        
        count = 0
        hits = 1
        page = 1
        while count < hits:
            result = searcher.search(Page.objects, self.names, 
                ('title', 'body'), use_or=True, page=page, per_page=100)
            hits = result['hits']
            count += len(result['object_list'])
            page += 1
            for page in result['object_list']:
                if task_cls:
                    if hasattr(task_cls, 'initialize'):
                        task_cls.initialize()
                    task = task_cls({task_cls.target_name:page,
                        'model_instance':obj})
                    task.do()
                else:
                    item = NewsItem(content_object=obj)
                    item.news = page
                    item.save()
