# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core import serializers
from django.http import HttpResponse
from maricilib.django.core.paginator import Paginator
from maricilib.django.apps.search import searcher
from models import Page, NewsItem

def search_pages(request, query, page=1):
    query = request.GET.get('query', query)
    page = request.GET.get('page', page)
    size = 5 
    pages = searcher.search(Page.objects, [query], ('title', 'body'))
    page_obj = Paginator(pages['object_list'], size).page(page)
    d = {'page_obj':page_obj, 'title':u'%s の検索結果' % query}
    return render_to_response('crawler/pages.html', d,
            RequestContext(request))

def show_pages(request):
    pages, page_obj = get_pages(request)
    d = {'page_obj':page_obj}
    return render_to_response('crawler/pages.html', d,
            RequestContext(request))

def show_items(request, name):
    items, page_obj = get_items(request, name)
    d = {'name':name, 'page_obj':page_obj}
    return render_to_response('crawler/items.html', d,
            RequestContext(request))

def json_pages(request):
    pages, page_obj = get_pages(request)
    paginator = page_obj.paginator
    serializer = serializers.get_serializer('json')()
    json = serializer.serialize(page_obj.object_list, 
            fields=('url','title', 'body', 'updated_at'))
    json = '{"count": %s, "size": %s, "page": %s, "objects": %s}' % \
            (pages.count(), paginator.per_page, page_obj.number, json)
    return HttpResponse(json, mimetype='application/json')

def json_items(request, name):
    items, page_obj = get_items(request, name)
    paginator = page_obj.paginator
    serializer = serializers.get_serializer('json')()
    json = serializer.serialize(page_obj.object_list)
    json = '{"count": %s, "size": %s, "page": %s, "objects": %s}' % \
            (items.count(), paginator.per_page, page_obj.number, json)
    return HttpResponse(json, mimetype='application/json')

def get_pages(request, size=10):
    page = int(request.GET.get('page', 1))
    size = int(request.GET.get('size', size))
    site_url = request.GET.get('site')
    pages = Page.objects.all()
    if site_url:
        pages = pages.filter(site__url=site_url)
    pages = pages.order_by('-updated_at')
    return pages, Paginator(pages, size).page(page)

def get_items(request, name, size=10):
    page = int(request.GET.get('page', 1))
    size = int(request.GET.get('size', size))
    items = NewsItem.objects.filter(content_name=name, is_valid=True)
    items = items.order_by('-created_at')
    return items, Paginator(items, size).page(page)

