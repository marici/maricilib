# -*- coding: utf-8 -*-
import time
from datetime import datetime, timedelta
from threading import Lock
from django.conf import settings
from maricilib.taskqueue import TaskQueue

taskqueue = None


class CrawlTaskQueue(TaskQueue):

    def __init__(self, *args, **kwargs):
        super(CrawlTaskQueue, self).__init__(*args, **kwargs)
        self.lock = Lock()
        self.history = {}

    def send_task(self, task, queue_name=None):
        if hasattr(task, 'site_url'):
            need_sleep = False
            self.lock.acquire()
            last_sent_at = self.history.get(task.site_url)
            if last_sent_at:
                next = last_sent_at + timedelta(seconds=3)
                now = datetime.now()
                if now < next:
                    need_sleep = True
                    next_time = time.mktime(next.timetuple())
                    now_time = time.mktime(now.timetuple())
                    self.history[task.site_url] = next
            if need_sleep:
                self.lock.release()
                time.sleep(next_time - now_time)
            else:
                self.history[task.site_url] = datetime.now()
                self.lock.release()
        super(CrawlTaskQueue, self).send_task(task, queue_name)


def get_taskqueue():
    global taskqueue
    if taskqueue is None:
        kwargs = {}
        def _setting(key, setting_name, default=None):
            if hasattr(settings, setting_name):
                kwargs[key] = getattr(settings, setting_name, None)
            elif default is not None:
                kwargs[key] = default
        _setting("backend", "CRAWL_QUEUE_BACKEND")
        _setting("user", "CRAWL_QUEUE_USER")
        _setting("password", "CRAWL_QUEUE_PASSWORD")
        _setting("host", "CRAWL_QUEUE_HOST")
        _setting("port", "CRAWL_QUEUE_PORT")
        _setting("timeout", "CRAWL_QUEUE_TIMEOUT")
        _setting("basedir", "CRAWL_QUEUE_BASEDIR")
        taskqueue = CrawlTaskQueue(**kwargs)
    return taskqueue
