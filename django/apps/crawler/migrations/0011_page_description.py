# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0010_auto_20151103_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='description',
            field=models.TextField(null=True, verbose_name='\u30c7\u30b9\u30af\u30ea\u30d7\u30b7\u30e7\u30f3'),
        ),
    ]
