# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0006_auto_20150810_1415'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='article_image_url',
            field=models.CharField(max_length=255, null=True, verbose_name='\u8a18\u4e8b\u753b\u50cfURL', blank=True),
        ),
    ]
