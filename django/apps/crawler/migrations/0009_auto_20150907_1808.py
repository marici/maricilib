# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0008_page_display_priority'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='article_keywords',
            field=models.TextField(null=True, verbose_name='\u8a18\u4e8b\u30ad\u30fc\u30ef\u30fc\u30c9', blank=True),
        ),
        migrations.AddField(
            model_name='site',
            name='nofollow_from_article',
            field=models.BooleanField(default=True, verbose_name='\u8a18\u4e8b\u304b\u3089\u306e\u30ea\u30f3\u30af\u7121\u8996'),
        ),
    ]
