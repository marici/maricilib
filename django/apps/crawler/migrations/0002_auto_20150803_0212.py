# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='new_page_task',
            field=models.CharField(max_length=255, null=True, verbose_name='\u30da\u30fc\u30b8\u8ffd\u52a0\u6642\u5b9f\u884c\u30bf\u30b9\u30af', blank=True),
        ),
        migrations.AddField(
            model_name='site',
            name='update_page_task',
            field=models.CharField(max_length=255, null=True, verbose_name='\u30da\u30fc\u30b8\u66f4\u65b0\u6642\u5b9f\u884c\u30bf\u30b9\u30af', blank=True),
        ),
    ]
