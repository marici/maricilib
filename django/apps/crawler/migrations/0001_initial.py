# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u30bf\u30a4\u30c8\u30eb')),
                ('url', models.URLField(unique=True, max_length=255, verbose_name='\u30d5\u30a3\u30fc\u30c9URL', db_index=True)),
                ('link', models.URLField(verbose_name='\u30b5\u30a4\u30c8URL', null=True, editable=False)),
                ('subtitle', models.CharField(verbose_name='\u30b5\u30d6\u30bf\u30a4\u30c8\u30eb', max_length=255, null=True, editable=False)),
                ('description', models.TextField(verbose_name='\u8aac\u660e', null=True, editable=False)),
                ('language', models.CharField(verbose_name='\u8a00\u8a9e', max_length=255, null=True, editable=False)),
                ('last_fetched_at', models.DateTimeField(verbose_name='\u6700\u7d42\u53d6\u5f97\u65e5\u6642', null=True, editable=False)),
                ('interval', models.IntegerField(default=30, verbose_name='\u5e73\u5747\u66f4\u65b0\u9593\u9694', editable=False)),
                ('is_active', models.BooleanField(default=True, verbose_name='\u53d6\u5f97\u5bfe\u8c61')),
            ],
            options={
                'verbose_name': '\u30af\u30ed\u30fc\u30eb\u5bfe\u8c61\u30d5\u30a3\u30fc\u30c9',
                'verbose_name_plural': '\u30af\u30ed\u30fc\u30eb\u5bfe\u8c61\u30d5\u30a3\u30fc\u30c9',
            },
        ),
        migrations.CreateModel(
            name='FeedEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(unique=True, max_length=255, verbose_name='URL', db_index=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u30bf\u30a4\u30c8\u30eb')),
                ('author', models.CharField(max_length=255, null=True, verbose_name='\u4f5c\u6210\u8005')),
                ('summary', models.TextField(null=True, verbose_name='\u6982\u8981')),
                ('content', models.TextField(null=True, verbose_name='\u5185\u5bb9')),
                ('updated_at', models.DateTimeField(null=True, verbose_name='\u66f4\u65b0\u65e5\u6642')),
                ('feed', models.ForeignKey(editable=False, to='crawler.Feed', verbose_name='\u30d5\u30a3\u30fc\u30c9')),
            ],
            options={
                'verbose_name': '\u30af\u30ed\u30fc\u30eb\u6e08\u30d5\u30a3\u30fc\u30c9\u30a8\u30f3\u30c8\u30ea',
                'verbose_name_plural': '\u30af\u30ed\u30fc\u30eb\u6e08\u30d5\u30a3\u30fc\u30c9\u30a8\u30f3\u30c8\u30ea',
            },
        ),
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_valid', models.BooleanField(default=False, db_index=True, verbose_name='\u691c\u8a3c\u6e08')),
                ('url', models.URLField(max_length=255, verbose_name='URL', db_index=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u30bf\u30a4\u30c8\u30eb')),
                ('body', models.TextField(null=True, verbose_name='\u672c\u6587', blank=True)),
                ('content_name', models.CharField(max_length=255, null=True, verbose_name='\u3072\u3082\u3065\u3051\u5148', db_index=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u4f5c\u6210\u65e5\u6642')),
                ('object_id', models.PositiveIntegerField(editable=False)),
                ('content_type', models.ForeignKey(editable=False, to='contenttypes.ContentType')),
                ('entry', models.ForeignKey(editable=False, to='crawler.FeedEntry', null=True)),
            ],
            options={
                'verbose_name': '\u95a2\u9023\u30a2\u30a4\u30c6\u30e0',
                'verbose_name_plural': '\u95a2\u9023\u30a2\u30a4\u30c6\u30e0',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(unique=True, max_length=255, verbose_name='URL', db_index=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u30bf\u30a4\u30c8\u30eb')),
                ('content', models.TextField(null=True, verbose_name='\u30b3\u30f3\u30c6\u30f3\u30c4')),
                ('tagged_body', models.TextField(null=True, verbose_name='\u30bf\u30b0\u4ed8\u304d\u672c\u6587')),
                ('body', models.TextField(null=True, verbose_name='\u672c\u6587')),
                ('updated_content', models.TextField(null=True, verbose_name='\u6700\u5f8c\u306e\u66f4\u65b0\u5185\u5bb9')),
                ('frame_urls', models.TextField(verbose_name='\u30d5\u30ec\u30fc\u30e0URL\u306e\u30ea\u30b9\u30c8', null=True, editable=False)),
                ('has_feed', models.BooleanField(default=False, verbose_name='\u30d5\u30a3\u30fc\u30c9\u4fdd\u6301', editable=False)),
                ('crawled_at', models.DateTimeField(verbose_name='\u53d6\u5f97\u65e5\u6642', null=True, editable=False, db_index=True)),
                ('updated_at', models.DateTimeField(verbose_name='\u66f4\u65b0\u65e5\u6642', null=True, editable=False, db_index=True)),
                ('checked_at', models.DateTimeField(verbose_name='\u30c1\u30a7\u30c3\u30af\u65e5\u6642', null=True, editable=False, db_index=True)),
                ('intervals', models.CommaSeparatedIntegerField(default=b'', verbose_name='\u66f4\u65b0\u9593\u9694\u5c65\u6b74\uff08\u5206\uff09', max_length=255, editable=False)),
                ('meta', models.TextField(null=True, verbose_name='\u30e1\u30bf\u30c7\u30fc\u30bf')),
            ],
            options={
                'verbose_name': '\u30af\u30ed\u30fc\u30eb\u6e08\u30da\u30fc\u30b8',
                'verbose_name_plural': '\u30af\u30ed\u30fc\u30eb\u6e08\u30da\u30fc\u30b8',
            },
        ),
        migrations.CreateModel(
            name='PageUpdate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(verbose_name='\u30b3\u30f3\u30c6\u30f3\u30c4')),
                ('updated_at', models.DateTimeField(verbose_name='\u66f4\u65b0\u65e5\u6642')),
                ('page', models.ForeignKey(verbose_name='\u30da\u30fc\u30b8', to='crawler.Page')),
            ],
            options={
                'verbose_name': '\u30da\u30fc\u30b8\u306e\u66f4\u65b0',
                'verbose_name_plural': '\u30da\u30fc\u30b8\u306e\u66f4\u65b0',
            },
        ),
        migrations.CreateModel(
            name='Preference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('unique', models.BooleanField(default=True, unique=True, editable=False)),
                ('new_page_task', models.CharField(max_length=255, null=True, verbose_name='\u30da\u30fc\u30b8\u8ffd\u52a0\u6642\u5b9f\u884c\u30bf\u30b9\u30af', blank=True)),
                ('update_page_task', models.CharField(max_length=255, null=True, verbose_name='\u30da\u30fc\u30b8\u66f4\u65b0\u6642\u5b9f\u884c\u30bf\u30b9\u30af', blank=True)),
                ('entry_task', models.CharField(max_length=255, null=True, verbose_name='\u30d5\u30a3\u30fc\u30c9\u30a8\u30f3\u30c8\u30ea\u8ffd\u52a0\u6642\u5b9f\u884c\u30bf\u30b9\u30af', blank=True)),
            ],
            options={
                'verbose_name': '\u5168\u4f53\u8a2d\u5b9a',
                'verbose_name_plural': '\u5168\u4f53\u8a2d\u5b9a',
            },
        ),
        migrations.CreateModel(
            name='SearchExecution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('query', models.CharField(max_length=255, verbose_name='\u691c\u7d22\u30af\u30a8\u30ea')),
                ('executed_at', models.DateTimeField(verbose_name='\u5b9f\u884c\u65e5\u6642', null=True, editable=False, blank=True)),
                ('content', models.TextField(verbose_name='\u30b3\u30f3\u30c6\u30f3\u30c4', null=True, editable=False)),
            ],
            options={
                'verbose_name': '\u691c\u7d22\u30ef\u30fc\u30c9',
                'verbose_name_plural': '\u691c\u7d22\u30ef\u30fc\u30c9',
            },
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u30b5\u30a4\u30c8\u540d')),
                ('url', models.URLField(unique=True, max_length=255, verbose_name='\u8d77\u70b9URL', db_index=True)),
                ('depth', models.IntegerField(default=2, verbose_name='\u30ea\u30f3\u30af\u8ffd\u8de1\u306e\u6df1\u3055')),
                ('update_depth', models.IntegerField(default=1, verbose_name='\u66f4\u65b0\u30c1\u30a7\u30c3\u30af\u306e\u6df1\u3055')),
                ('ignore_params', models.BooleanField(default=False, verbose_name='URL\u30d1\u30e9\u30e1\u30fc\u30bf\u3092\u7121\u8996\u3059\u308b')),
                ('follow_pattern', models.TextField(null=True, verbose_name='\u30ea\u30f3\u30af\u8ffd\u8de1\u30d1\u30bf\u30fc\u30f3', blank=True)),
                ('nofollow_pattern', models.TextField(null=True, verbose_name='\u30ea\u30f3\u30af\u7121\u8996\u30d1\u30bf\u30fc\u30f3', blank=True)),
                ('body_start', models.TextField(null=True, verbose_name='\u672c\u6587\u958b\u59cb\u884c\u306e\u6b63\u898f\u8868\u73fe', blank=True)),
                ('body_end', models.TextField(null=True, verbose_name='\u672c\u6587\u7d42\u4e86\u884c\u306e\u6b63\u898f\u8868\u73fe', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='\u53d6\u5f97\u5bfe\u8c61')),
            ],
            options={
                'verbose_name': '\u30af\u30ed\u30fc\u30eb\u5bfe\u8c61\u30b5\u30a4\u30c8',
                'verbose_name_plural': '\u30af\u30ed\u30fc\u30eb\u5bfe\u8c61\u30b5\u30a4\u30c8',
            },
        ),
        migrations.AddField(
            model_name='page',
            name='site',
            field=models.ForeignKey(editable=False, to='crawler.Site', null=True),
        ),
        migrations.AddField(
            model_name='newsitem',
            name='page',
            field=models.ForeignKey(editable=False, to='crawler.Page', null=True),
        ),
        migrations.AddField(
            model_name='feed',
            name='pages',
            field=models.ManyToManyField(verbose_name='\u691c\u51fa\u30da\u30fc\u30b8', null=True, editable=False, to='crawler.Page'),
        ),
    ]
