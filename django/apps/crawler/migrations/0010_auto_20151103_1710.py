# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0009_auto_20150907_1808'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='site_name',
            field=models.CharField(verbose_name='\u30b5\u30a4\u30c8\u540d', max_length=255, null=True, editable=False),
        ),
        migrations.AddField(
            model_name='page',
            name='site_url',
            field=models.URLField(verbose_name='\u30b5\u30a4\u30c8URL', max_length=255, null=True, editable=False),
        ),
        migrations.AlterField(
            model_name='feed',
            name='pages',
            field=models.ManyToManyField(verbose_name='\u691c\u51fa\u30da\u30fc\u30b8', editable=False, to='crawler.Page'),
        ),
    ]
