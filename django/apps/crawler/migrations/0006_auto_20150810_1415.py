# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0005_page_is_article'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='is_article',
            field=models.BooleanField(default=False, db_index=True, verbose_name='\u8a18\u4e8b\u30d5\u30e9\u30b0'),
        ),
    ]
