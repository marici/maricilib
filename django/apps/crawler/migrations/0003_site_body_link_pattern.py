# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0002_auto_20150803_0212'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='body_link_pattern',
            field=models.TextField(null=True, verbose_name='\u672c\u6587\u30ea\u30f3\u30af\u30d1\u30bf\u30fc\u30f3', blank=True),
        ),
    ]
