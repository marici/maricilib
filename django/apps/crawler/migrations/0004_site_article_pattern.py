# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0003_site_body_link_pattern'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='article_pattern',
            field=models.TextField(null=True, verbose_name='\u8a18\u4e8bURL\u30d1\u30bf\u30fc\u30f3', blank=True),
        ),
    ]
