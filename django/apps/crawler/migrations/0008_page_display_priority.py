# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawler', '0007_page_article_image_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='display_priority',
            field=models.IntegerField(default=0, verbose_name='\u8868\u793a\u512a\u5148\u5ea6', db_index=True),
        ),
    ]
