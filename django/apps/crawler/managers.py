# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.db import models

class SearchSourceManager(models.Manager):
    @property
    def boss(self):
        if not hasattr(self, '_boss'):
            setattr(self, '_boss', self.get(name='Yahoo! BOSS'))
        return self._boss
