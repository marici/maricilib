# -*- coding:utf-8 -*-
import unicodedata
from pytrie import StringTrie

class Matcher(object):

    @staticmethod
    def normalize(text):
        if not text: return ''
        return unicodedata.normalize('NFKC', text)
    
    def __init__(self, objects=None, key_name=None, value_name=None):
        self.trie = StringTrie()
        if objects and key_name:
            self.fill(objects, key_name, value_name)
    
    def fill(self, objects, key_name, value_name=None):
        for obj in objects:
            value = getattr(obj, value_name) if value_name else obj
            keys = getattr(obj, key_name)
            if type(keys) in (str, unicode):
                keys = [keys]
            elif not hasattr(keys, '__iter__'):
                continue
            for key in keys:
                key = self.normalize()
                if key not in self.trie:
                    self.trie[key] = set()
                self.trie[key].add(value)
    
    def search(self, text):
        matched_keys = set()
        matched_objects = []
        text = self.normalize(text)
        for i in xrange(len(text)):
            key, values = self.trie.longest_prefix_item(text[i:], (None, None))
            if values:
                if key in matched_keys: continue
                matched_keys.add(key)
                matched_objects.extend(list(values))
        return matched_objects
    