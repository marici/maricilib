# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.conf.urls import patterns, include, url

urlpatterns = patterns('maricilib.django.apps.crawler.views',
    url(r'^$', 'show_pages', name=''),
    url(r'^items/([^/]+)/$', 'show_items', name=''),
    url(r'^api/pages', 'json_pages', name=''),
    url(r'^api/items/([^/]+)$', 'json_items', name=''),
    url(r'^search/(\w+)?/?$', 'search_pages', name='crawler-search'),
)
