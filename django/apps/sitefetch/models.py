# coding: utf-8
from datetime import datetime, timedelta
from django.db import models
import managers

class Page(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'ページ'

    url = models.URLField(u'URL', db_index=True, unique=True, max_length=255)
    real_url = models.URLField(u'実URL')
    title = models.CharField(u'タイトル', max_length=255, db_index=True)
    frame_urls = models.TextField(u'フレームURLのリスト', null=True)
    content = models.TextField(u'コンテンツ', null=True)
    updated_content = models.TextField(u'更新内容', null=True)
    has_feed = models.BooleanField(u'フィード保持', default=False)
    updated_at = models.DateTimeField(u'更新日時', null=True)
    checked_at = models.DateTimeField(u'チェック日時', null=True)
    interval = models.IntegerField(u'平均更新間隔', default=10)
    meta = models.TextField(u'メタデータ', null=True)

    def __unicode__(self):
        return self.title

    def needs_refresh(self):
        if not self.updated_at: return True
        return self.updated_at + timedelta(minutes=self.interval) \
            < datetime.now()

    def update(self, content, updated_at):
        if self.updated_at is None or self.updated_at < updated_at:
            self.updated_content = content
            self.update_at = updated_at
        

class PageUpdate(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'ページの更新'

    page = models.ForeignKey(Page, verbose_name=u'ページ', editable=False)
    content = models.TextField(u'コンテンツ')
    updated_at = models.DateTimeField(u'更新日時')

    def __unicode__(self):
        return self.page.title


class Feed(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'フィード'

    pages = models.ManyToManyField(Page, verbose_name=u'検出ページ', 
        null=True, editable=False)
    url = models.URLField(u'URL', unique=True, max_length=255, db_index=True)
    link = models.URLField(u'リンクURL', null=True)
    title = models.CharField(u'タイトル', max_length=255, null=True)
    subtitle = models.CharField(u'サブタイトル', max_length=255, null=True)
    description = models.TextField(u'説明', null=True)
    language = models.CharField(u'言語', max_length=255, null=True)
    last_fetched_at = models.DateTimeField(u'最終取得日時', null=True)
    interval = models.IntegerField(u'平均更新間隔', default=30)

    def needs_refresh(self):
        if self.last_fetched_at is None: return True
        if self.interval == 0: return True
        return self.last_fetched_at + timedelta(minutes=self.interval) \
            < datetime.utcnow()

    def __unicode__(self):
        return self.url


class FeedEntry(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'フィードエントリ'

    objects = managers.FeedEntryManager()

    feed = models.ForeignKey(Feed, verbose_name=u'フィード', editable=False)
    url = models.URLField(u'URL', max_length=255, unique=True, db_index=True)
    title = models.CharField(u'タイトル', max_length=255)
    author = models.CharField(u'作成者', max_length=255, null=True)
    summary = models.TextField(u'概要', null=True)
    content = models.TextField(u'内容', null=True)
    updated_at = models.DateTimeField(u'更新日時', null=True)

    def __unicode__(self):
        return self.title

    def __need_add_index__(self):
        return True
    
    def __need_delete_index__(self):
        return False
    
    def __indexdoc__(self):
        d = {"url_s":self.url,
             "title_tj":self.title,
             "summary_tj":self.summary,
             "content_tj":self.content}
        return d

