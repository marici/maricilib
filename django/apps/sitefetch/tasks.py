# coding: utf-8
import logging
import re, unicodedata, difflib, urllib, urlparse
from datetime import datetime, timedelta
from maricilib.feed import finder
from maricilib.taskqueue.tasks import Task
from maricilib.django.apps.taskqueue.queue import get_taskqueue
from models import Page, PageUpdate, Feed, FeedEntry

import chardet
import feedparser

def unified_diff_ctrl(buf):
    marks = ('--- ', '+++ ', '@@ ')
    for m in marks:
        if buf.startswith(m): return True
    return False

_tags_pat = re.compile(r'<(p|li)[^>]*>', re.I)
def contains_significant_tag(buf):
    m = _tags_pat.search(buf)
    return m is not None

_ja_char_names = ('CJK', 'HIRAGANA', 'KATAKANA')
_p_names = ('COMMA', 'FULL STOP')
def contains_japanese(buf, threshold=20):
    count = 0
    for c in buf:
        try:
            name = unicodedata.name(c)
        except ValueError: continue
        else:
            for n in _ja_char_names:
                if name.startswith(n) or name.endswith(_p_names):
                    count += 1
                else:
                    count = 0
    return threshold < count

def get_updated_content(page, html):
    if page.content == html: return None
    difflist = []
    update_lines = []
    sig = ja = False
    for buf in difflib.unified_diff(page.content.splitlines(),
            html.splitlines()):
        if unified_diff_ctrl(buf):
            if update_lines:
                if sig and ja:
                    difflist.append('\n'.join(update_lines))
                update_lines = []
            sig = ja = False
            continue
        if not buf.startswith('-'):
            update_lines.append(buf[1:])
        if contains_significant_tag(buf): sig = True
        if contains_japanese(buf): ja = True
    return '\n\n'.join(difflist)

_title_pat = re.compile(r'<title[^>]*>([^<]*)</title>', re.I)
class PageCheckTask(Task):
    queue_name = 'feedfetch.pagecheck'

    def do(self):
        if hasattr(self, 'page'):
            self.url = self.page.url
        else:
            try:
                self.page = Page.objects.get(url=self.url)
            except Page.DoesNotExist:
                self.page = Page(url=self.url)
            else:
                if not getattr(self, 'update', True): return
        page = self.page

        now = datetime.now()
        page.checked_at = now
        page.updated_at = now

        urlpage = urllib.urlopen(page.url)
        page.real_url = urlpage.geturl()
        html = urlpage.read()
        urlpage.close()
        encoding = chardet.detect(html)['encoding']
        if encoding is None:
            logging.warn('HTML decode error: %s', page.url)
            page.save()
            return
        html = html.decode(encoding, 'ignore')

        m = _title_pat.search(html)
        if m: page.title = m.group(1)

        if page.content:
            updated_content = get_updated_content(page, html)
            if updated_content:
                page.updated_content = updated_content
                PageUpdate.objects.create(page=page, content=updated_content,
                    updated_at=now)
                if page.updated_at is not None and \
                        now < page.updated_at + timedelta(minutes=page.interval):
                    page.interval = (page.interval + \
                        ((now - page.updated_at).seconds / 60)) / 2
                page.updated_at = now
            else:
                page.interval = min(60 * 24, page.interval * 1.2)
        page.content = html

        if getattr(self, 'find_feed', True):
            feeds = finder.find_from_html(html)
            if feeds:
                page.has_feed = True
            page.save()
            for feed in feeds:
                feed_url = feed.get('href')
                if not feed_url.startswith('http'):
                    feed_url = urlparse.urljoin(page.url, feed_url)
                logging.debug("Feed was found: %s", feed_url)
                try:
                    feed = Feed.objects.get(url=feed_url)
                except Feed.DoesNotExist:
                    feed = Feed.objects.create(url=feed_url)
                    task = FeedCheckTask(dict(url=feed_url))
                    get_taskqueue().send_task(task, queue_name=task.queue_name)
                feed.pages.add(page)
        else:
            page.save()


class FeedCheckTask(Task):
    queue_name = 'feedfetch.feedcheck'

    def do(self):
        now = datetime.utcnow()
        self.entries = []
        self.new_entries = []
        if hasattr(self, 'feed'):
            feedobj = self.feed
            self.url = feedobj.url
        else:
            feedobj = Feed.objects.get(url=self.url)
        feedobj.last_fetched_at = now
        try:
            feed = feedparser.parse(self.url, 
                agent='Universal Feed Parser/4.1 +http://www.marici.co.jp')
        except Exception, e:
            logging.warn('Feed parse error: %s %s', self.url, e)
            feedobj.save()
            return
        if feed.feed.get('link') is None:
            logging.warn("Invalid Feed: %s", feed)
            feedobj.save()
            return
        feedobj.link = feed.feed.get('link')
        feedobj.title = feed.feed.get('title')
        feedobj.substitle = feed.feed.get('subtitle')
        feedobj.description = feed.feed.get('description')
        feedobj.language = feed.feed.get('language')
        for entry in feed.get('entries', []):
            try:
                entry = FeedEntry.objects.get(url=entry.get('link'))
            except FeedEntry.DoesNotExist:
                if entry.get('link') is None:
                    logging.warn('Invalid Entry: %s', entry)
                    continue
                logging.debug(entry)
                try:
                    entry = FeedEntry.objects.create(
                        feed=feedobj,
                        title=entry.get('title'),
                        author=entry.get('author'),
                        summary=entry.get('summary'),
                        content=entry.get('content', [{}])[0].get('value'),
                        updated_at=datetime(*entry.updated_parsed[:6]),
                        url=entry.get('link')
                    )
                    self.new_entries.append(entry)
                except Exception, e:
                    logging.warn("FeedEntry '%s' could not be created: %s", self.url, e)
                    continue
            self.entries.append(entry)
        newest = None
        intervals = []
        prev = None
        sorted_entries = [ entry for entry in self.entries if entry.updated_at ]
        sorted_entries = sorted(self.entries, lambda x, y: x.updated_at < y.updated_at)
        for entry in sorted_entries:
            if newest is None or newest.updated_at < entry.updated_at:
                newest = entry
            if prev is not None and prev.updated_at and entry.updated_at:
                newer = max(prev.updated_at, entry.updated_at)
                older = min(prev.updated_at, entry.updated_at)
                intervals.append((newer - older).seconds / 60)
            prev = entry
        if newest:
            for page in feedobj.pages.all():
                page.update(newest.content, newest.updated_at)
                page.save()
            if newest.updated_at < now:
                intervals.append((now - newest.updated_at).seconds / 60)
        if intervals:
            avg_interval = reduce(lambda x, y: x + y, intervals) / len(intervals)
            if avg_interval:
                if feedobj.interval:
                    feedobj.interval = (feedobj.interval + avg_interval) / 2
                else:
                    feedobj.interval = avg_interval
        feedobj.save()

