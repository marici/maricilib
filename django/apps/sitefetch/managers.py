# coding: utf-8
from django.db import models
from maricilib.django.apps.search import searcher

class FeedEntryManager(models.Manager):

    def search(self, queries, fields=None, page=1, per_page=10):
        if fields is None:
            fields = [ 'title_tj' for i in range(len(queries)) ]
        if searcher is None:
            return {"length":0, "object_list":[]}
        else:
            return searcher.search(self, queries, fields, page, per_page,
                '-updated_at')

