# coding: utf-8
from django.contrib import admin
import models

class PageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'checked_at')
admin.site.register(models.Page, PageAdmin)


class PageUpdateAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.PageUpdate, PageUpdateAdmin)


class FeedAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Feed, FeedAdmin)


class FeedEntryAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.FeedEntry, FeedEntryAdmin)
