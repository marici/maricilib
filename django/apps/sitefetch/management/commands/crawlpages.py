# coding: utf-8
import logging
logging.basicConfig(level=logging.INFO)

from optparse import make_option
from datetime import datetime
from django.core import serializers
from django.utils import simplejson
from maricilib import load_class
from maricilib.django.core.management.base import MultiThreadCommand
from maricilib.django.apps.taskqueue.queue import get_taskqueue
from maricilib.django.apps.sitefetch.models import Page
from maricilib.websites import newscrawl

def _get_class(path):
    clspath = path.split('.')
    module = '.'.join(clspath[:-1])
    clsname = clspath[-1]
    return load_class(clsname, module)

def is_new_page(url):
    try:
        Page.objects.get(url=url)
        return False
    except Page.DoesNotExist, e:
        return True

class Command(MultiThreadCommand):

    option_list = MultiThreadCommand.option_list + (
        make_option('--page-task', dest='page_task', 
            help='task to process pages.'),
    )

    def mainloop(self, *args, **opts):
        self.pages = []
        self.page_taskcls = _get_class(opts.get('page_task')) \
            if opts.get('page_task')  else None
        filename = args[0]
        for line in file(filename):
            site_name, url, pattern = line.strip().split(',')
            self.enqueue((url, pattern))
            
    def finalize(self, *args, **opts):
        for page in self.pages:
            task = self.page_taskcls(dict(page_id=page.pk))
            get_taskqueue().send_task(task)
        
    def do(self, pageinfo):
        seed_url, pattern = pageinfo
        logging.info('crawling %s', seed_url)
        for url, real_url, title, body, published_at in \
                newscrawl.crawl(seed_url, pattern, is_new_page):
            try:
                pages = Page.objects.filter(title=title)
                if pages.count() == 0:
                    page = Page.objects.create(url=url, title=title,
                        real_url=real_url, content=body, updated_at=published_at,
                        checked_at=datetime.now())
                    self.pages.append(page)
                    logging.info('fetched %s', title)
            except Exception, e:
                logging.warn(e)

