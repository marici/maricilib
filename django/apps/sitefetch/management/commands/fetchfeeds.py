# coding: utf-8
import logging
logging.basicConfig(level=logging.INFO)

from optparse import make_option
from django.core import serializers
from django.utils import simplejson
from maricilib import load_class
from maricilib.django.core.management.base import MultiThreadCommand
from maricilib.django.apps.search import indexer
from maricilib.django.apps.taskqueue.queue import get_taskqueue
from maricilib.django.apps.sitefetch.models import Feed, Page
from maricilib.django.apps.sitefetch.tasks import FeedCheckTask, PageCheckTask

def _get_class(path):
    clspath = path.split('.')
    module = '.'.join(clspath[:-1])
    clsname = clspath[-1]
    return load_class(clsname, module)

class Command(MultiThreadCommand):

    option_list = MultiThreadCommand.option_list + (
        make_option('--entry-task', dest='entry_task', 
            help='task to process feedentries.'),
    )

    def mainloop(self, *args, **opts):
        self.pages = []
        self.entry_taskcls = _get_class(opts.get('entry_task')) \
            if opts.get('entry_task')  else None
        filename = args[0]
        for line in file(filename):
            url = line.strip()
            if not url: continue
            try:
                feed = Feed.objects.get(url=url)
            except:
                feed = Feed.objects.create(url=url)
            task = FeedCheckTask(dict(feed=feed))
            self.enqueue(task)
        
    def do(self, task):
        task.do()
        json_serializer = serializers.get_serializer("json")()
        feed_json = json_serializer.serialize([task.feed], ensure_ascii=False)
        feed_obj = simplejson.loads(feed_json)[0]
        if isinstance(task, FeedCheckTask):
            logging.info('Fetched feed: %s', task.feed.url)
            for entry in task.new_entries:
                logging.info('New entry: %s', entry.url)
                if indexer is not None:
                    indexer.add(entry)
                if self.entry_taskcls is not None:
                    entry_json = json_serializer.serialize([entry], 
                        ensure_ascii=False)
                    entry_obj = simplejson.loads(entry_json)[0]
                    newtask = self.entry_taskcls(dict(
                        feed=feed_obj,
                        entry=entry_obj))
                    get_taskqueue().send_task(newtask)

