# -*- coding: utf-8 -*-
import logging
from optparse import make_option
from django.core.management.base import BaseCommand
from maricilib import load_class
from maricilib.django.apps.search import indexer

def get_class(class_name):
    names = class_name.split('.')
    return load_class(names[-1], '.'.join(names[:-1]))

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--models', dest='models', action='store',
            default='', help='target models.'),
        make_option('--debug', dest='debug', action='store_true',
            help='raise exception and exit.'),
    )

    def handle(self, *args, **opts):
        self.debug = opts.get('debug', False)

        model_class_names = opts.get('models', '').split(',')
        model_classes = []
        for class_name in model_class_names:
            if not class_name: continue
            model_cls = get_class(class_name)
            for obj in model_cls.objects.all():
                indexer.add(obj, force=True)
            logging.info('%s objects were indexed.', class_name)

