# -*- coding: utf-8 -*-
import logging
import urllib
from dateutil.parser import parse
from collections import defaultdict
from django.utils import simplejson
from maricilib.taskqueue.tasks import Task
from maricilib.nlp import morpheme
from twitter.models import Tweet, Query, Word

class SearchTask(Task):

    url = 'http://search.twitter.com/search.json'

    def do(self):
        params = getattr(self, 'params', {})

        try:
            query = Query.objects.get(text=self.query)
        except Query.DoesNotExist:
            query = Query.objects.create(text=self.query)
        self.query_obj = query
        if query.max_id: params['since_id'] = query.max_id

        tokens = morpheme.analyze(self.query)
        params['q'] = ' '.join([ t.encoded_surface for t in tokens ])
        params_str = '?' + urllib.urlencode(params)

        while params_str is not None:
            print params_str
            url = self.url + params_str
            p = urllib.urlopen(url)
            json = p.read()
            p.close()
            objects = simplejson.loads(json)
            for result in objects.get('results'):
                self.insert(result)
            params_str = objects.get('next_page')

        if objects:
            query.max_id = objects.get('max_id')
            query.save()

    def insert(self, result):
        if self.query not in result.get('text', ''): return

        try:
            tweet = Tweet.objects.get(tweet_id=result.get('id'),
                query=self.query_obj)
        except Tweet.DoesNotExist:
            tweet = Tweet.objects.create(
                tweet_id=result.get('id'),
                text=result.get('text'),
                from_user_id=result.get('from_user_id'),
                from_user=result.get('from_user'),
                to_user_id=result.get('to_user_id'),
                to_user=result.get('to_user'),
                iso_language_code=result.get('iso_language_code'),
                source=result.get('source'),
                profile_image_url=result.get('profile_image_url'),
                created_at=parse(result.get('created_at')),
                query=self.query_obj
            )
            logging.debug('create: %s(%s)', tweet.text, tweet.tweet_id)


class HotWordTask(Task):

    def do(self):
        try:
            query = Query.objects.get(text=self.query)
        except Query.DoesNotExist:
            return
        df = defaultdict(int)
        tweets = Tweet.objects.filter(query=query, created_at__gte=self.start_at)
        if self.end_at: tweets = tweets.filter(created_at__lt=self.end_at)
        poslist = list(morpheme.TokenList.poslist1)
        poslist.append(u'未知語')
        for tweet in tweets:
            text = tweet.text.replace('@', u' ')
            text = text.replace(' ', u' 。 ')
            tokens = morpheme.analyze_by_mecab(text, '--unk-feature 未知語')
            for tokens in tokens.all_subtokens(tuple(poslist)):
                df[unicode(tokens)] += 1
        word_freqs = sorted(df.iteritems(), lambda x, y: cmp(y[1], x[1]))
        for word, freq in word_freqs[:20]:
            # 前回のデータを取得
            prev_word_freq = 0
            prev_word_delta = 0
            prev_words = Word.objects.filter(text=word, 
                start_at__lt=self.start_at).order_by('-start_at')
            if prev_words.count():
                prev_word_freq = prev_words[0].frequency
                prev_word_delta = prev_words[0].delta
            delta = freq - prev_word_freq
            if 0 < delta: delta += max(prev_word_delta, 0)
            Word.objects.create(text=word, frequency=freq, delta=delta,
                start_at=self.start_at, end_at=self.end_at)
            
