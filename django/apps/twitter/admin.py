# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.contrib import admin
from twitter import models

class QueryAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Query, QueryAdmin)

class TweetAdmin(admin.ModelAdmin):
    list_display = ('tweet_id', 'text', 'created_at')
admin.site.register(models.Tweet, TweetAdmin)

