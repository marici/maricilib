# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.core.management.base import BaseCommand
from django.conf import settings
from twitter.oauthtwitter import OAuthApi

class Command(BaseCommand):

    def handle(self, *args, **opts):
        consumer_key = getattr(settings, 'TWITTER_CONSUMER_KEY')
        consumer_secret = getattr(settings, 'TWITTER_CONSUMER_SECRET')
        api = OAuthApi(consumer_key, consumer_secret)
        request_token = api.getRequestToken()
        url = api.getAuthorizationURL(request_token)
        print u'以下のURLにアクセスし、アクセスを許可した後表示される暗証番号を入力してください。'
        print url
        api = OAuthApi(consumer_key, consumer_secret, request_token)
        oauth_verifier = raw_input(u'暗証番号: '.encode('utf-8'))
        token = api.getAccessToken(request_token, oauth_verifier)
        print 'oauth_token:', token['oauth_token']
        print 'oauth_token_secret', token['oauth_token_secret']
