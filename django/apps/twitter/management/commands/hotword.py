# -*- coding: utf-8 -*-
import datetime
from django.core.management.base import BaseCommand
from twitter.tasks import HotWordTask

class Command(BaseCommand):

    def handle(self, *args, **opts):
        query = args[0].decode('utf-8')
        start_at = self.parse_datetime(args[1])
        end_at = self.parse_datetime(args[2]) if 2 < len(args) else None
        print query, start_at, end_at
        task = HotWordTask(dict(query=query, start_at=start_at, end_at=end_at))
        task.do()
        
    def parse_datetime(self, s):
        data = []
        splitted = s.split(' ')
        for s in splitted[0].split('-'):
            data.append(int(s))
        if 1 < len(splitted):
            for s in splitted[1].split(':'):
                data.append(int(s))
        return datetime.datetime(*data) - datetime.timedelta(hours=9)
