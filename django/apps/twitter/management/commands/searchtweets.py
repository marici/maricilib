# -*- coding:utf-8 -*-
'''
@author: michisu@marici.co.jp
'''
from django.core.management.base import BaseCommand
from twitter.tasks import SearchTask
from twitter.models import Query

class Command(BaseCommand):

    def handle(self, *args, **opts):
        for query in Query.objects.all():
            task = SearchTask(dict(query=query.text))
            task.do()
