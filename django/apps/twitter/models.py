# -*- coding: utf-8 -*-
from django.db import models

class Query(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'検索ワード'

    text = models.CharField(u'検索ワード', max_length=255)
    max_id = models.IntegerField(u'取得済最大ID', default=0)

    def __unicode__(self):
        return self.text


class Tweet(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'ツイート'

    tweet_id = models.IntegerField(u'Tweet ID')
    text = models.TextField(u'テキスト')
    from_user_id = models.IntegerField(u'ユーザID')
    from_user = models.CharField(u'ユーザ名', max_length=255)
    to_user_id = models.IntegerField(u'送信先ユーザID', null=True)
    to_user = models.CharField(u'送信先ユーザ名', max_length=255, null=True)
    iso_language_code = models.CharField(u'言語コード', max_length=10, null=True)
    source = models.TextField(u'ソース', null=True)
    profile_image_url = models.URLField(u'プロフィール画像', max_length=255)
    created_at = models.DateTimeField(u'作成日時', db_index=True)
    query = models.ForeignKey(Query)

    def __unicode__(self):
        return self.text


class Word(models.Model):

    class Meta:
        verbose_name = verbose_name_plural = u'ワード'

    text = models.CharField(u'ワード', max_length=255, db_index=True)
    frequency = models.IntegerField(u'頻度')
    start_at = models.DateTimeField(u'集計開始')
    end_at = models.DateTimeField(u'集計終了')
    delta = models.IntegerField(u'前回との頻度差', default=0)

