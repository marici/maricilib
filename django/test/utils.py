# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse


class ViewTestCase(TestCase):

    def _test(self, method, urlname, url_args=None, url_kwargs=None, params={},
            status=200, contains=[], redirect_to=None, redirect_urlname=None,
            redirect_url_args=None, redirect_url_kwargs=None,
            response_validator=None, content_validator=None,
            context_validator=None):
        method = method.lower()
        url = reverse(urlname, args=url_args, kwargs=url_kwargs)
        if method == 'get':
            response = self.client.get(url, params)
        elif method == 'post':
            response = self.client.post(url, params)
        else:
            self.fail("method '{0}' is not allowed".format(method))
        self.assertEqual(response.status_code, status)
        if contains:
            for contain in contains:
                self.assertContains(response, contain)
        if redirect_to:
            self.assertRedirects(response, redirect_to, status)
        if redirect_urlname:
            redirect_to = reverse(
                redirect_urlname, args=redirect_url_args,
                kwargs=redirect_url_kwargs)
            self.assertRedirects(response, redirect_to, status)
        if response_validator:
            response_validator(response)
        if content_validator:
            content_validator(response.content)
        if context_validator:
            context_validator(response.context)
