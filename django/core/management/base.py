# coding: utf-8
"""
The MIT License

Copyright (c) 2009 Marici, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import logging, signal, time
from optparse import make_option
from Queue import Queue
from threading import Thread, Event
from django.core.management.base import BaseCommand

class MultiThreadCommand(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--threads', action='store', type='int', 
            dest='num_threads', default=1,
            help='Specifies the number of threads.'),
        make_option('--debug', dest='debug', action='store_true',
            help='raise exception and exit.'),
    )

    def initialize(self, *args, **opts):
        pass

    def mainloop(self, *args, **opts):
        pass

    def finalize(self, *args, **opts):
        pass

    def do(self, arg):
        pass

    def enqueue(self, arg):
        if 1 < self.num_threads:
            self.queue.put(arg)
        else:
            self.do(arg)

    def handle_signal(self):
        print('terminating command...')
        self.event.set()

    def handle(self, *args, **opts):
        self.event = Event()
        signal.signal(signal.SIGINT, lambda a, b: self.handle_signal())
        self.initialize(*args, **opts)
        self.num_threads = opts.get('num_threads', 1)
        self.initialize_threads()
        try:
            self.mainloop(*args, **opts)
        except Exception, e:
            if opts.get('debug', False): raise
            logging.warn(e)
        except:
            if opts.get('debug', False): raise
            pass
        #self.queue.join()
        self.finalize(*args, **opts)

    def initialize_threads(self):
        self.queue = Queue(self.num_threads)
        if self.num_threads == 1: return
        for i in range(self.num_threads):
            t = Thread(target=self.worker)
            t.setDaemon(True)
            t.start()

    def worker(self):
        while True:
            if self.event.isSet(): break
            try:
                arg = self.queue.get(True, 0.5)
            except:
                continue
            try:
                self.do(arg)
            except Exception, e:
                logging.warn(e)
            finally:
                self.queue.task_done()
        
