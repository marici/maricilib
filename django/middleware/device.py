# -*- coding: utf-8 -*-
import re
from django.conf import settings

charset_pattern = re.compile(r'charset=[^;]*', re.I)

class DeviceType(object):

    smartphone = 1
    featurephone = 2
    etc = 3


class Device(object):

    def __init__(self, type):
        self.is_smartphone = (type == DeviceType.smartphone)
        self.is_featurephone = (type == DeviceType.featurephone)
        self.is_etc = (type == DeviceType.etc)


class DeviceMiddleware(object):
    '''
    この Middleware を有効にすると、request オブジェクトが device 属性を持つよ
    うになります。
    request.device.is_smartphone が True の場合はスマートフォン、
    request.device.is_featurephone と True の場合はフィーチャーフォンと判別され
    ています。
    セッション変数 'pc_view' に True を格納すると、強制的に PC と判別されます。

    また、フィーチャーフォンと判定された場合、リクエストとレスポンスの文字コー
    ドを自動で cp932 に設定し、Content-Type の charset を Shift_JIS に置き換え
    ます。
    この変換動作は settings.py に FEATUREPHONE_CONVERT_CHARSET = False を設定す
    ることによって無効にできます。

    XXX: このクラスは HttpResponse インスタンスの _charset 属性にアクセスしてい
    るため、Django のバージョンによっては動作しない可能性があります。
    Django 1.5 では動作確認済です。
    '''

    sp_ua_parts = (
        'iPad',
        'iPhone',
        'iPod',
        'Android',
    )

    fp_ua_parts = (
        'DoCoMo',
        'UP.Browser',
        'KDDI-',
        'J-PHONE',
        'Vodafone',
        'SoftBank',
        'emobile',
        'WILLCOM',
        'DDIPOCKET',
    )

    def is_debug_mode(self, request, setting_name, param_name):
        if getattr(settings, setting_name, False):
            return True
        if settings.DEBUG:
            if (param_name in request.GET and
                    request.GET[param_name] == 'True'):
                return True
        return False

    def match_ua(self, request, parts):
        ua = request.META.get('HTTP_USER_AGENT', '')
        for part in parts:
            if part in ua:
                return True
        return False

    def is_smartphone(self, request):
        if self.is_debug_mode(request, 'SMARTPHONE_DEBUG', 'DG_SP'):
            return True
        return self.match_ua(request, self.sp_ua_parts)

    def is_featurephone(self, request):
        if self.is_debug_mode(request, 'FEATUREPHONE_DEBUG', 'DG_FP'):
            return True
        return self.match_ua(request, self.fp_ua_parts)

    def process_request(self, request):
        request.device = Device(DeviceType.etc)
        if not request.session.get('pc_view'):
            if self.is_smartphone(request):
                request.device = Device(DeviceType.smartphone)
            elif self.is_featurephone(request):
                request.device = Device(DeviceType.featurephone)
                if getattr(settings, 'FEATUREPHONE_CONVERT_CHARSET', True):
                    request.encoding = 'cp932'

    def process_response(self, request, response):
        if request.device.is_featurephone:
            if getattr(settings, 'FEATUREPHONE_CONVERT_CHARSET', True):
                # XXX: accessing private attribute
                response._charset = 'cp932'
                response['Content-Type'] = charset_pattern.sub(
                    'Shift_JIS', response['Content-Type'])
        return response
